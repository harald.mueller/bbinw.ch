<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>
<body>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!-- Start Content -->
<br><br>
<h2>AGBs - Allgemeine Gesch&auml;ftsbedingungen</h2>

<b>Vorbemerkung<br><br></b>Die
Maklerfirma BBINW widmet sich der Erf&uuml;llung von Maklerauftr&auml;gen mit
gr&ouml;sstm&ouml;glicher Sorgfalt und objektiver Wahrung der Interessen der
Auftraggeber im Rahmen der allgemein anerkannten kaufm&auml;nnischen
Grunds&auml;tze und Gebr&auml;uche unter Einhaltung der Standesregeln des
Berufstandes.<br><br>Bei allen Leistungen und f&uuml;r alle Vertr&auml;ge, die
BBINW f&uuml;r Kunden erbringt oder mit Ihnen eingeht, gelten die unten
aufgef&uuml;hrten Allgemeinen Gesch&auml;ftsbedingungen, die der Kunde mit
Vertragsabschluss anerkennt.<br><br><b>&sect; 1 Art der T&auml;tigkeit<br><br></b>Die
T&auml;tigkeit umfasst den Nachweis oder die Vermittlung von Grundst&uuml;cken,
H&auml;usern, Gesch&auml;ftsverk&auml;ufen, Verpachtungen, Wohnungs- und
Gewerberaumvermietungen, Inneneinrichtung, Imageberatung.<br><br><b>&sect; 2 Maklervertrag<br><br></b>Die
Aufnahme von Kaufverhandlungen zu dem jeweilig angebotenen Objekt
begr&uuml;ndet das Zustandekommen eines Maklervertrages und die Anerkennung
des im Expos&eacute; genannten Courtageanspruches.<br><br><b>&sect; 3 Vertraulichkeit der Angebote</b><br>Der
Empf&auml;nger behandelt alle Angebote und Mitteilungen als vertraulich.
Gelangt durch eine von ihm zu verantwortende Indiskretion ein Dritter
zu einem Vertragsabschluss mit einem von dem Maklerunternehmen
nachgewiesenen Anbieter, so ist der Empf&auml;nger zur Zahlung einer
Courtage in H&ouml;he von 3 % zzgl. gesetzlicher Mehrwertsteuer vom
Wirtschaftswert des Vertrages verpflichtet.<br><br><b>&sect; 4 H&ouml;he der Courtage (Provision)</b><br>F&uuml;r
den Nachweis oder die Vermittlung zahlt der Empf&auml;nger bei Erwerb eines
Objektes durch ihn oder einen Verwandten oder ein ihm wirtschaftlich
oder personell verbundenes Unternehmen eine Courtage von 3 % bei Objekten in der Schweiz, oder 5 % bei Objekten in der EU, zzgl.
gesetzlicher Mehrwertsteuer, vom Wirtschaftsgut des Vertrages und
Einschluss aller damit zusammenh&auml;ngenden Nebenabreden und
Ersatzgesch&auml;ften. Ersatzgesch&auml;fte zwischen den zusammengef&uuml;hrten
Vertragspartnern - K&auml;ufer und Verk&auml;ufer - sind ebenso wie
Folgegesch&auml;fte f&uuml;r einen Zeitraum von zwei Jahren ab letztem
Vertragsabschluss bzw. Vertragsverhandlungen, auch wenn diese nicht zum
Abschluss eines notariellen Vertrages gef&uuml;hrt haben, in voller H&ouml;he
courtagepflichtig. Die Zahlungspflicht besteht auch, wenn das
Maklerunternehmen am Ersatz- oder Folgegesch&auml;ft nicht urs&auml;chlich mit
dem Nachweis oder der Vermittlung beteiligt war. (Art. 413 ff. OR)<br><br>F&uuml;r den
Nachweis oder die Vermittlung von gewerblichen Miet- oder
Pachtvertr&auml;gen zahlt der Mieter/ P&auml;chter bei Vertr&auml;gen mit einer
Laufzeit von bis zu 5 Jahren eine Courtage von 2 Monatskaltmieten, bzw.
monatlichen Pachtzahlungen zzgl. gesetzlicher Mehrwertsteuer. Bei
Vertr&auml;gen mit einer Laufzeit von mehr als 5 Jahren hat der Mieter/
P&auml;chter eine Courtage in H&ouml;he von 5 Monatskaltmieten bzw. monatlichen
Pachtzahlungen zzgl. gesetzlicher Mehrwertsteuer zu zahlen. Die
Vereinbarung eines Optionsrechtes gilt bei Miet- und Pachtvertr&auml;gen als
Vertragsabschluss und ist ebenfalls courtagepflichtig.<br><br>Ist die
von dem Maklerunternehmen nachgewiesene Gelegenheit zum Abschluss des
Vertrages dem Empf&auml;nger bereits bekannt, erkl&auml;rt er das dem
Maklerunternehmen innerhalb von 5 Tagen und f&uuml;hrt den Nachweis, woher
seine Kenntnis stammt. Sp&auml;tere Widerspr&uuml;che braucht das
Maklerunternehmen nicht gegen sich gelten zu lassen.<br>Hinsichtlich
des Objektes ist das Maklerunternehmen auf die Ausk&uuml;nfte der Verk&auml;ufer,
Vermittler, Verp&auml;chter, Bauherren, Bautr&auml;ger und Beh&ouml;rden angewiesen.
F&uuml;r die Richtigkeit und Vollst&auml;ndigkeit der Angaben kann keine Haftung
&uuml;bernommen werden. Dar&uuml;ber hinaus kann das Maklerunternehmen f&uuml;r die
Objekte keine Gew&auml;hr &uuml;bernehmen und f&uuml;r die Bonit&auml;t der Vertragspartner
nicht haften.<br><br>Das Maklerunternehmen nimmt keine Verm&ouml;genswerte
entgegen, die der Erf&uuml;llung vertraglicher Vereinbarungen zwischen
Ver&auml;usserer und Interessenten dienen.<br><br><b>&sect; 5 Beratung<br><br></b>Der Stundensatz f&uuml;r eine Beratung betr&auml;gt CHF 160.-- und die <br>Kilometerpauschale  CHF 1.-- pro km<br><br><b>&sect; 6 Mehrwertsteuer<br><br></b>Die Erhebung der Mehrwertsteuer erfolgt nach dem jeweils g&uuml;ltigen Mehrwertsteuersatz.<br><br><b>&sect; 7 Nebenabreden<br><br></b>Nebenabreden zu den Angeboten der Firma BBINW bed&uuml;rfen zur ihrer Rechtswirksamkeit der Schriftform.<br><br>Alle weiteren Vereinbarungen bed&uuml;rfen zu ihrer G&uuml;ltigkeit der Schriftform.<br><br><b>&sect; 8 Vertraulichkeit der Angebote<br><br></b>S&auml;mtliche
Angebote und Mitteilungen sind ausschliesslich an den jeweiligen
Adressaten selbst gerichtet. Sie sind vertraulich zu behandeln und
d&uuml;rfen Dritten weder als Original noch inhaltlich zug&auml;nglich gemacht
werden. Kommt infolge unbefugter Weitergabe ein Vertrag zustande, so
ist der Weitergebende verpflichtet, Schadenersatz in H&ouml;he der Courtage
gem&auml;ss &sect; 4 AGB an die Firma BBINW zu zahlen.<br><br><b>&sect; 9 Doppelt&auml;tigkeit, Verweispflicht<br><br></b>Die
Firma BBINW ist berechtigt, auch f&uuml;r den anderen Vertragspartner t&auml;tig
zu werden und hierf&uuml;r Geb&uuml;hren zu berechnen. Eine durch die Firma BBINW
mitgeteilte Gelegenheit zum Abschluss eines Rechtsgesch&auml;ftes wird als
bisher unbekannt erachtet, wenn nicht innerhalb von 5 Tagen nach
Kenntnisnahme schriftlicher Widerspruch erfolgt und gleichzeitig
nachgewiesen wird, woher die Kenntnis stammt. Bei erteiltem
Makler-Alleinauftrag sind direkte oder auch durch andere Makler
benannte Interessenten unverz&uuml;glich an den allein beauftragten Makler
zu verweisen.<br><br><b>&sect; 10 Mitteilungspflicht<br><br></b>Sobald ein
Vertragsabschluss &uuml;ber ein durch die Firma BBINW als Auftragnehmer
angebotenes Objekt zustande gekommen ist, hat der Auftraggeber den
Auftragnehmer hiervon unverz&uuml;glich in Kenntnis zu setzen. Es besteht
Anspruch auf Anwesenheit bei Vertragsabschluss.<br><br><b>&sect; 11 Verzug<br><br></b>Sollte
der Auftraggeber mit der Zahlung der Maklercourtage in Verzug geraten,
so werden ihm ab dem Verzugszeitpunkt Verzugszinsen p.a. in H&ouml;he von 5%
&uuml;ber dem Basiszinssatz berechnet.<br><br><b>&sect; 12 Haftung<br><br></b>Die
Angaben und Unterlagen zum Objekt basieren auf Informationen Dritter,
die der Firma BBINW erteilt wurden. Die Firma BBINW ist bem&uuml;ht, &uuml;ber
Vertragspartner oder Objekte m&ouml;glichst vollst&auml;ndige und wahrheitsgem&auml;sse
Angaben zu erhalten. F&uuml;r deren Richtig- und Vollst&auml;ndigkeit kann jedoch
keine Haftung &uuml;bernommen werden.<br><br>Die Haftung f&uuml;r Sch&auml;den ist beschr&auml;nkt auf Vorsatz und grobe Fahrl&auml;ssigkeit.<br><br>Zwischenverkauf, -vermietung, -verpachtung und Irrtum bleiben vorbehalten.<br><br><b>&sect; 13 Wirksamkeit<br><br></b>Sollten
Teile dieser Gesch&auml;ftsbedingungen oder einzelne vertragliche Abreden
unwirksam sein, so wird dadurch die Wirksamkeit der anderen
Bestimmungen nicht ber&uuml;hrt. An Stelle der unwirksamen treten sinngem&auml;ss
einschl&auml;gige gesetzliche Bestimmungen entsprechend des wirtschaftlichen
Zweckes.<br><br><b>&sect; 14 Erf&uuml;llungsort und Gerichtsstand<br><br></b>Als Erf&uuml;llungsort und Gerichtsstand ist, soweit gesetzlich zul&auml;ssig, <br>CH 4144 Arlesheim vereinbart.<br><br><b>BBINW - Arlesheim<br></b>Neumattstrasse 8<br>4144 Arlesheim<br>061 599 27 46 phone<br>061 599 47 45 fax<br>076 413 1936 mobile<br>076 418 1938 mobile<br><br><b>BBINW - Zeihen</b> <br>M&uuml;hliweiher 9<br>5079 Zeihen<br>062 876 27 46 phone<br>062 876 27 45 fax<br>076 413 1936 mobile<br>076 418 1938 mobile<br><br><b>E-Mail:</b> immobilien@bauland-nw.ch<br><br>BBINW Arlesheim, den 01.04.2008

<!-- End Content -->


<br>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->

</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
