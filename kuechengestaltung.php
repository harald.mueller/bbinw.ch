<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<br><br>
<h2 align="left">Suchen Sie nach Ihrer individuellen K&uuml;che?<br></h2>
<p>Wir beraten Sie gerne bei der Gestaltung Ihrer individuellen K&uuml;che.<br><br>
Bitte <a href="bbinw-kontakt.php">kontaktieren</a> Sie uns, wenn wir Ihr Interesse geweckt haben.

<br><br><a class="link" href="downloads/kuecheneinteilungen.pdf" target="_blank">Download: &Uuml;bersicht K&uuml;chenplanung (PDF)</a>
<br><br><a class="link" href="downloads/checkliste_kueche_20080709.pdf" target="_blank">Checkliste K&uuml;chenplanung (PDF)</a>

</p>
<br>

</div>
<br>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
