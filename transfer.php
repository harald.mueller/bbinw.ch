<?php 
session_start();
require_once 'functions.php';
function getRelIconDirectory() {	return "icon/";	}
function getRelDataDirectory() {	return "transfer/";			}
function getAbsDataDirectory() {
	return getcwd()."/".getRelDataDirectory();	
}
$passwortOK = false;
$loeschenChecked="";
if (isset($_REQUEST["btnPasswort"])) {
	if (isset($_REQUEST["inpPasswort"])) {
		$inpPasswort = $_REQUEST["inpPasswort"];
		if ($inpPasswort == "transfer") {
			$passwortOK = true;
			$loeschenChecked="ja";
		}
	}
}
if (isset($_REQUEST['btnFileupload'])) {
	$fileName = basename( $_FILES['uploaded']['name']);
	if (strlen($fileName) > 1) {
		uploadFile($fileName);
	}
}
if (isset($_REQUEST['toDelete'])) {
	if ($_REQUEST['toDelete'] != "") {
		deleteFile($_REQUEST['toDelete']);
		$_REQUEST['toDelete'] = "";
	}
}
include 'inc.head.html';
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<body>
<?php include 'inc.menuStickyTop.html';?>
	<div class="container">
	    
	        <h1>Transfer</h1>
	        <h2>&nbsp;Dokumente zum Herunterladen</h2>
	        
	    <div class="textbereich">
	
<?php if (isset($_REQUEST["btnCMS"])) {	?>
	<form id="isCMS" id="isCMS" action="" method="post">
		<div class="pull-right">
			<input type="password" name="inpPasswort" id="inpPasswort" placeholder="Passwort" style="width: 80px"/>
			<br>			
			<input type="submit"  name="btnPasswort" id="btnPasswort" value="OK" style="width: 89px"/>

		</div>	
	</form>
<?php 
	} else { 
		if (!$passwortOK) {
?>
			<form name="frmCMS" id="frmCMS" method="post" action="">
				<div class="pull-right pull-top">
					<input type="submit" name="btnCMS" id="btnCMS" value="Login" style="background:transparent; border:0; font-size:12pt; width: 89px"/>
					<br>
				</div>	
			</form>
<?php
		}
	} 
?>

	        
	        <?php 
	        if ($passwortOK) {
	        ?>
				<div class="collapse">
					<form name="frm_Eingabe" method="post" enctype="multipart/form-data">
						<input type="hidden" name="MAX_FILE_SIZE" value="900000000" />
						<table class="uploadBar" width="500">
							<tr>
								<td>
									<input type="file"  name="uploaded" id="uploaded" />
								</td>
								<td>
									<input type="submit" name="btnFileupload" id="btnFileupload" onclick="jsFktBtnUpload()" value="OK"/>
								</td>
							</tr>
						</table>
						<br>
					</form> 
				</div>
	        <?php 
	        } 
	        ?>
	        <form  method="post">
	        <?= dokusAnzeigen($passwortOK); ?>
	        </form>
	        
	    </div>

			
	</div>
	<div class="clearfix"></div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
