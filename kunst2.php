<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2>Chinesische Malerei</h2>
<p>Die Chinesische Malerei ist eine Auspr&auml;gung der chinesischen Kunst und damit der chinesischen Kultur. Wie die chinesische Geschichtsschreibung kann sie auf einen langen Zeitraum zur&uuml;ckblicken.<br>Die wichtigsten Motive in der chinesischen Malerei sind Figuren, Landschaften, Blumen und V&ouml;gel. Unter den Blumen und Pflanzen werden besonders gerne Bambus, Chrysanthemen und Orchideen abgebildet. Bei der Darstellung geht es nun nicht in erster Linie darum, das Motiv m&ouml;glichst naturgetreu und realistisch darzustellen. Viel wichtiger ist, eine Stimmung mit dem Bild zu transportieren. Auch die naturgetreue Perspektive hat in der chinesischen Malerei wenig Bedeutung. Es gibt stattdessen Darstellungen, die z.B. eine Landschaft aus ganz unterschiedlichen Perspektiven darstellen.</p><br>Bitte <a href="bbinw-kontakt.php">kontaktieren</a> Sie uns, wenn wir Ihr Interesse geweckt haben.<br><br></p>
<i>
<img width="308px" src="./images/china1.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china2.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china3.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china4.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china5.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china6.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china7.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china8.jpg" ><p>Preis: CHF 350.--</p><br>
<img width="308px" src="./images/china9.jpg" ><p>Preis: CHF 350.--</p><br>
</i>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
