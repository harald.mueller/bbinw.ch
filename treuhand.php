<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>
<style>
td {
	padding: 10px;
}
</style>
<div class="clearfix"></div>

<br><br>
<h1>Das Treuhand-Mandat</h1>
<br><br>

<p>
<table border="0" class="repairBackgroundColor">
  <tr>
    <th width="200">Parteien</th>
    <td>Auftraggeber / Beauftrager</td>
  </tr>
  <tr>
    <th>Entstehung</th> 
    <td>formlos durch Annahme der Besorgung OR 395</td>
  </tr>
  <tr>
    <th>Umfang</th>
    <td>bestimmt sich nach der Natur des zu besorgenden Gesch&auml;ftes OR 396</td>
  </tr>
  <tr>
    <th>Vollmachten</th>
    <td>z.T. sind besondere Erm&auml;chtigungen notwendig OR 396/3</td>
  </tr>
  <tr>
    <th>Verpflichtungen</th>
    <td>der Beauftragte ist zur vorschriftsgem&auml;ssen Ausf&uuml;hrung verpflichtet OR 397</td>
  </tr>
  <tr>
    <th>Haftung</th>
    <td>der Beauftragte haftet f&uuml;r getreue und sorgf&auml;ltige Ausf&uuml;hrung</td>
  </tr>
  <tr>
    <th>Durchf&uuml;hrung</th>
    <td>pers&ouml;nlich, ausgenommen wenn der Beauftragte erm&auml;chtigt ist,
    <br>bestimmte Aufgaben einem Dritten zu &uuml;bertragen OR 398/3 oder OR 399</td>
  </tr>
  <tr>
    <th>Rechenschaft</th>
    <td>jederzeit auf Verlangen des Auftraggebers &uuml;ber OR 400</td>
  </tr>
  <tr>
    <th>Rechte</th>
    <td>gehen auf den Auftraggeber &uuml;ber OR 401</td>
  </tr>
  <tr>
    <th>Pflichten</th>
    <td>der Auftraggeber verpflichtet sich um Auslagenersatz samt Zinsen und Befreiung von Verpflichtungen OR 402</td>
  </tr>
  <tr>
    <th>Haftung mehrerer</th>
    <td>alle Auftraggeber und Beauftragte haften unter sich solidarisch OR 403</td>
  </tr>
  <tr>
    <th>Beendigung</th>
    <td>beidseitig jederzeit m&ouml;glich durch Widerruf bzw. K&uuml;ndigung OR 404</td>
  </tr>
  <tr>
    <th>Erl&ouml;schen</th>
    <td>durch Tod, Handlungsunf&auml;higkeit oder Konkurs OR 405</td>
  </tr>
  <tr>
    <th>Wirkung des Erl&ouml;schens</th>
    <td>Verpflichtungen vor dem Erl&ouml;schen bleiben Verbindlich OR 406</td>
  </tr>
</table>
<br>
<br>
<br>
<br>
<h2>Unsere Referenzen</h2>
<table border="0" class="repairBackgroundColor">
  <tr>
    <th colspan="2">Erfolgreich erledigte Treuhandmandate/Renditeberechnungen f&uuml;r folgende Liegenschaften:</th>
  </tr>
  <tr>
    <th width="200">&nbsp;</th>
    <td>&nbsp;</td>
  </tr>
  <tr>
  	<td style="text-align: right;">1.)</td>
    <td>7,5 Zimmer EFH in Gipf-Oberfrick</td>
  </tr>
  <tr>
    <td style="text-align: right;">2.)</td>
    <td>Bungalow im Jura</td>
  </tr>
  <tr>
  	<td style="text-align: right;">3.)</td>
    <td>6,5 Zimmer-Eck-EFH im Fricktal</td>
  </tr>
  <tr>
    <td style="text-align: right;">4.)</td>
    <td>Renditeberechnung f&uuml;r den Umbau des
      Mehrfamilienhauses mit ehemaligem Handschuh-
      und Strumpfwarengesch&auml;ft in Basel am Marktplatz
    </td>
  </tr>
  <tr>
    <th>&nbsp;</th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><a href="downloads/Referenzliste_Bautreuhand_2017_01_02.pdf"><img src="icon/pdf.png">&nbsp;Referenzliste_Bautreuhand</a></td>
    <th>Gerne &uuml;bernehmen wir weitere Mandate</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

</div>

<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>

Referenzliste_Bautreuhand_2017_01_02.pdf
