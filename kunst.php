<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2>Wohnraum - Kunstraum</h2>
<p>Wohnr&auml;ume und deren Gestaltung sind ein Ausdruck der Pers&ouml;nlichkeit. Nirgendwo sonst verbringt man gew&ouml;hnlich mehr Zeit mit der Betrachtung von Kunst als in den eigenen vier W&auml;nden. Dem Betrachter ein entspannendes und gleichzeitig interessantes Sehvergn&uuml;gen zu verschaffen sollte hier angestrebt werden, eine Harmonie zwischen Architektur und Bilderwelt.</p>

<br><br><a class="link" href="kunst1.php"><b>K&uuml;nstler aus der Region</b><br><i>(&rArr; Weitere Informationen finden Sie hier...)</i></a>

<br><br><a class="link" href="kunst2.php"><b>Chinesische Malerei</b><br><i>(&rArr; Weitere Informationen finden Sie hier...)</i></a>

<br>
<br>Bitte <a href="bbinw-kontakt.php">kontaktieren</a> Sie uns, wenn wir Ihr Interesse geweckt haben.<br><br></p>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
