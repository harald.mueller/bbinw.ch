<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>


<br><br>
<h1>Finanzierung</h1>
<h2>Finanzdienstleistungen</h2>
<p><ul><li>Wir leiten f&#252;r Sie die passende Hypothekarfinanzierung ein.</li>

<li>Vermitteln Ihnen den geeigneten Versicherungsanbieter mit der passenden Versicherung.</li>

<li>Vermitteln Ihnen die passende Kapitalanlage.</li>

<li>Suchen f&#252;r Sie den passenden Investoren.</li>
</ul>
</p>

<br><br>

<p align="center">

<img src="./images/visitenkarte.gif" width="321" height="203">
</p>
<p align="center">&nbsp;</p>
<div class="center">	
<p><a href="https://swissrealestate-services.com/" target="_blank"><strong>SWISS REAL ESTATE SERVICES</strong></a></p>
  </div>	
<p align="center">&nbsp;</p>

<a href="bbinw-kontakt.php" style="text-decoration:none;"><h3 style="color:#505050;" onmouseover="this.style.color='#1D3F93'"; onmouseout="this.style.color='#505050'";>Bitte kontaktieren Sie uns!</h3></a>
<!--End Content -->

</div>
<br>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
