<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->

<br><br>
<h2 align="left"><br>Hausbau in Trockenbauweise</h2>
<!-- <table border="0" width="450"> -->
<table border="0" width="308">
<tr>
<td>
<div align="left">
<i>Neuheiten bei LUXHAUS</i><br><br>
</div>
</td>
</tr>
<tr>
<td>
Am 15./16.1.10 fand die Jahresendtagung bei LUXHAUS in Deutschland statt. Hier wurden einige, ganz spannende Neuheiten vorgestellt und pr&auml;sentiert, welche wir Ihnen nicht vorenthalten, bzw. zusammenfassend kurz aufz&auml;hlen m&ouml;chten:
<br>
<br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/luxneu1.jpg" alt="LUXHAUS">
<img width="200" src="./images/luxneu2.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/luxneu3.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>
<tr>
<td>
<ul>
<li>Ein neues, attraktives Bungalow- Programm "eben" mit gr&ouml;sseren Fenstern, attraktiveren Grundrissen bez&uuml;glich Bad, Anordnung WP, Eing&auml;ngen, LuxDesign, Dachformen, Fassadenelementen wurde kundenorientiert entwickelt</li>

<li>Neue Tecalor Luft-Wasser W&auml;rmepumpe (TTL) mit integrierter K&uuml;hlfunktion (L&uuml;ftung) f&uuml;r Innen- und Aussenaufstellung</li>

<li>Neue LUXHAUS Raumthermostaten (mit aufgedrucktem LUXHAUS Signet)</li>

<li>Neue L&ouml;sungen f&uuml;r ebenes betreten des Garten und der Umgebung (stufenfrei)</li>

<li>Neue Gegensprech- /Sicht Anlagen</li>

<li>Neue Verbrauchs&uuml;bersichtfolien der Energieerzeugungen (professionelle Verkaufshilfen)</li>

<li>Neu Heizkreispumpen der Energieklassen A</li>

<li>Neue Einbauspots Gesamtl&ouml;sungen mit dichten Einbaukasten ab 64 Euro / Stk.</li>

<li>Neue OSB B&ouml;den anstelle Spanplatten mit h&ouml;heren Festigkeiten und professionellem Look</li>
</ul>
<br>
<br>
</td>
</tr>
<tr>
<td>
<div align="left">
<i>Hausbau in Trockenbauweise, mit vorgefertigten Fertigelementen</i><br><br>
</div>
</td>
</tr>
<tr>
<td>
Traditionelles, bayerisches Familienunternehmen seit 1925. Inhabergef&uuml;hrt. Kein Fremdkapital im Unter- nehmen. 30 Jahre Garantie. Ausgesuchte, gepr&uuml;fte Materialien. Modernstes Fertighauswerk in Deutschland. Nur deutsche Arbeitskr&auml;fte werden zum Bau des <b>LUXHAUSES</b> besch&auml;ftigt. Bereits in der 3. Generation am Markt. H&ouml;chste Garantieleistungen. Hochwertige Deutsche, &Ouml;sterreichische und Schweizer Zulieferfirmen. Unabh&auml;ngig zertifizierte Qualit&auml;t. Feuersicherheit im h&ouml;chsten Bereich (F90B). Wandkraft garantiert (500 kg). Kompetente Begleitung von vertraglicher Vereinbarung bis hin zum Einzug. <b>LUX</b> baut &uuml;ber das Unternehmen Olux auch f&uuml;r die &ouml;ffentliche Hand (Kinderg&auml;rten, Schulen etc.).
<br>
<br>
</td>
</tr>
<tr>
<td>
Nachweislich geringster Energieverbrauch, 3-Liter-Haus, Einhaltungen FKW 40/60, Duo-Balken-System, sehr kurze Bauzeit, hohe Wartungsfreundlichkeit, Flat-Rate Heizung. Keine Vorauszahlung. Garantierter Fertig- stellungstermin. Schlanke Bauweise - mehr Wohnfl&auml;che bei gleichem Bauk&ouml;rper, lange Lebensdauer.
<br>
<br>
</td>
</tr>
<tr>
<td>
<u><b>live it<span style="color:red">.</span>  &nbsp; H&auml;user</b></u><br><br>
</td>
</tr>
<tr>
<td>
<b>live it<span style="color:red">.</span></b> H&auml;user von <b>LUXHAUS</b> sind erh&auml;ltlich in diversen Grundfarben, mit und ohne Erker, zweifarbig, einfarbig oder mit Akzentputzfl&auml;chen, Pult-, Zelt- oder Satteldach, in 3 verschiedenen Gr&ouml;ssen, schl&uuml;sselfertig, mit oder ohne Eigenleistung.
<br>
<br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/lux1.jpg" alt="LUXHAUS">
<img width="200" src="./images/lux2.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/lux3.jpg" alt="LUXHAUS">
<img width="200" src="./images/lux4.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/lux5.jpg" alt="LUXHAUS">
<img width="200" src="./images/lux6.jpg" alt="LUXHAUS">
<br>
<br>
</td>
</tr>
<tr>
<td>
<i>Innovation</i>
<br>
<br>
</td>
</tr>
<tr>
<td>
Climatic-Wand, hervorragender Hitze- und K&auml;lteschutz.
W&auml;rmepumpen von einem der namhaftesten Hersteller in Europa. Solartechnik. Modernste Produktionsanlagen, heissverleimte Wand. Freie Planungen. Junges, dynamisches Architektenteam mit frischen Ideen. Silentrohre, KFW 60 oder 40. Modernste Haustechnik (Bus-Systeme, Be- und Endl&uuml;ftung, etc.)
<br>
<br>
</td>
</tr>
<tr>
<td>
<i>Bequemlichkeit</i>
<br>
<br>
</td>
</tr>
<tr>
<td>
Alles aus einer Hand. Eigene Kellerbaufirma, eigene Architekten, eigene Finanzierer, Bauherrenbetreuung. Grundst&uuml;ckservice, wenige Nahtstellen, Hausbesuch beim Kunden, Koordination von weiteren Arbeiten wie z. B. Gartengestaltung. K&uuml;che, Innenarchitektur. Gesamtkoordination in der Bauphase. Alle Beh&ouml;rdeng&auml;nge. Baustellentermine.
<br>
<br>
</td>
</tr>
<tr>
<td>
<i>Zeitvorteil</i>
<br>
<br>
</td>
</tr>
<tr>
<td>
Bei einem Hausbau in Trockenbauweise mit Fertigbauelementen betr&auml;gt die Erstellungszeit nur 1 bis 2 Tage ohne Innenausbau und ohne Keller.
<br>
<br>
</td>
</tr>
<tr>
<td>
<u><b>Individualh&auml;user</b></u><br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/luxb1.jpg" alt="LUXHAUS">
<img width="200" src="./images/luxb2.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/luxb3.jpg" alt="LUXHAUS">
<img width="200" src="./images/luxb4.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/luxb5.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>

<tr>
<td>
<u><b>Musterhaus in Georgsgem&uuml;nd</b></u><br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/lux_muster3.jpg" alt="LUXHAUS">
<img width="200" src="./images/luxb6.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>
<tr>
<td>
<u><b>Haus eines Mitarbeiters</b></u><br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/lux_muster2.jpg" alt="LUXHAUS">
<br><br>
</td>
</tr>

<br><br><br>

<tr>
<td>
M&ouml;chten Sie Ihren Traum vom selbstgebauten Eigenheim Realit&auml;t werden lassen? <b>BBINW</b> &uuml;nterst&uuml;tzt Sie fachkompetent und engagiert bei der Umsetzung Ihrer Ideen.<br><br>
</td>
</tr>
<tr>
<td>
<u><b>K&uuml;chen von Zeyko</b></u><br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/Zeyko1.jpg" alt="ZEYKO">
<img width="200" src="./images/Zeyko2.jpg" alt="ZEYKO">
<br><br>
</td>
</tr>
<tr>
<td>
<img width="200" src="./images/Zeyko3.jpg" alt="ZEYKO">
<img width="200" src="./images/Zeyko4.jpg" alt="ZEYKO">
<br><br>
</td>
</tr>
<tr>
<td>
<a class="link" href="./downloads/flyer_zeyko.pdf"><b><i>(&rArr; Weitere Informationen &uuml;ber Zeyko-K&uuml;chen finden Sie hier...)</i></b></a><br><br>
</td>
</tr>


</table>

<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
