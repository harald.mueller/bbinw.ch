<?php
function uploadFile($fileName) {
	$file = basename( $_FILES['uploaded']['name']);
	//$file = str_replace($file, " ", "");
	$endung2 = strtolower(substr($file, -2));
	$endung3 = strtolower(substr($file, -3));
	$endung4 = strtolower(substr($file, -4));
	if(
			$endung4 == "docx" ||
			$endung4 == "xlsx" ||
			$endung4 == "pptx" ||
			$endung3 == "doc" ||
			$endung3 == "ppt" ||
			$endung3 == "txt" ||
			$endung3 == "xls" ||
			$endung3 == "zip" ||
			$endung3 == "pdf" ||
			$endung3 == "psd"
	) {
	} else {
		echo "File: ".$file."<br>
                	  	Keine g&uuml;ltige Dateiendung oder File zu gross!<br>
                	  	Maximale Folegr&ouml;sse: 900 MB<br>
                		G&uuml;ltig sind: docx,xlsx,pptx,doc,ppt,psd,txt,xls,zip,pdf<br><br>
                      	<a href='downloads.php'>back</a>";
		exit;
	}

	$target_path = getAbsDataDirectory().$file;
	if(move_uploaded_file($_FILES['uploaded']['tmp_name'], $target_path)) {
		chmod($target_path, 0777);
	} else{
		echo "There was an error uploading the file, please try again! <a href='downloads.php'>back</a>";
	}
}

function getThisFile($myFile){
	return substr($myFile, 1+strrpos($myFile, "/"));
}

function deleteFile($theFile) {
	@unlink(getAbsDataDirectory().$theFile);
}
function dokusAnzeigen($passwortOK) {
	$absWorkingDir 	= getAbsDataDirectory();
	$relWorkingDir 	= getRelDataDirectory();
	$iconsDir 		= getRelIconDirectory();

	chdir($absWorkingDir);
	$directory = ".";
	if(is_dir($directory) == true){
		$handle = opendir($directory);
		$zaehler=0;
//		while (false !== ($file = readdir($handle))){
		while($entryName = readdir($handle)) {
			$dirArray[] = $entryName;
		} 
		closedir($path);
		$indexCount = count($dirArray);
		sort($dirArray);
		
		for($index=0; $index < $indexCount; $index++) {
			$file = $dirArray[$index];
//		while (false !== ($file = readdir($handle))){
			$endung2 = strtolower(substr($file, -2));
			$endung3 = strtolower(substr($file, -3));
			$endung4 = strtolower(substr($file, -4));
			if ($endung4 == "docx") 	{$endung3 = "doc";}
			if ($endung4 == "xlsx") 	{$endung3 = "xls";}
			if ($endung4 == "pptx") 	{$endung3 = "ppt";}

			if ($endung3 == "doc" ||
				$endung3 == "ppt" ||
				$endung3 == "txt" ||
				$endung3 == "xls" ||
				$endung3 == "zip" ||
				$endung3 == "pdf") {
						$gezeigtesDok = $file;
						$zaehler++;
						
						$radioButton = '';						
						if ($passwortOK) {
							$radioButton = '<input type="radio" name="toDelete" value="'.$gezeigtesDok.'">';
						}
						$style = "cssEven";
						if ($zaehler % 2 == 0) {
							$style = "cssOdd";
						}
						
						echo ("<div class='dokZeile "
								.$style
								."'>&nbsp;"
								.$radioButton
								." <a href='"
								.$relWorkingDir.$gezeigtesDok
								."' target='_blank' style='font-size: 14pt;'>"
								."<img src='".$iconsDir."miniicon_".$endung3.".gif' border='0' height='20' width='20' >&nbsp;"
								.anzeigeFileName($gezeigtesDok)
								."</a></div>"
								);
					}
		}//end while

		if ($passwortOK) {
			echo('<div style="width:100%;">
					<input type="submit" name="btnDelete" id="btnDelete" value="Selektierte l&ouml;schen" style="width: 170px; font-size: "/>
					<input type="submit" name="btnCancel" id="btnCancel" value="Abbrechen" style="width: 120px;"/>
				</div>
				');
		}
		closedir($handle);
	} //end if
}//end function dokusAnzeigen


function anzeigeFileName($fileName){
	$ret = $fileName;
	$schwellenLaenge = 50;
	if (strlen($fileName) > $schwellenLaenge){
		$ret = substr( $fileName, 0,$schwellenLaenge-4) . "..." . substr($fileName, strlen($fileName)-4, 4);
	}
	return $ret;
}


?>