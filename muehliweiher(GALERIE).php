<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->

<br><br>
<h2 align="left">Doppel-Einfamilienhaus<br>
M&uuml;hliweiher 9, CH-5079 Zeihen</h2>
<br>

<p>
Der Innenausbau dieses Hauses ist so gew&auml;hlt, dass die R&auml;ume schnell gereinigt werden k&ouml;nnen.
Die B&ouml;den sind mit Feinsteinplatten und Linoleum belegt.
Die Sockelleisten wurden passend zu den B&ouml;den gew&auml;hlt, ebenso die Lichtschalter.
Die Fenstergriffe passen zu den T&uuml;rgriffen.
</p>
<br>
<p>
Die K&uuml;che ist eine Massanfertigung mit Granitabdeckung und einem eingebauten Granit-Tisch.
Es gibt vier Glaskeramik-Kochfelder mit zuschaltbaren Feldern, sowie zwei zus&auml;tzliche Induktionskochfelder,
eine Mikrowelle mit Ofen, sowie einen Backofen mit diversen Funktionen.
Die K&uuml;che verf&uuml;gt &uuml;ber einen K&uuml;hlschrank mit K&uuml;hl- und Gefrierfach, sowie einen Tiefgefrierschrank mit magnetischer Aussenfront.
</p>
<br>
<p>
Die W&auml;nde des Badezimmers wurden mit weisser und matter V + B Keramik belegt, die B&ouml;den bestehen aus robustem, pflegeleichten Feinstein.
Das Badezimmer verf&uuml;gt des weiteren &uuml;ber Spiegelschr&auml;nke mit passenden M&ouml;beln.
</p>
<br>
<p>
Das Haus hat eine moderne Aussend&auml;mmung,<br>
sowie eine Erdsonden-W&auml;rmepumpe.
</p>
<br>
<div class="compressContainer">
<p>
<b>Details: 6,5 Zimmer Doppel-EFH</b>
<ul>
<li>Nettowohnfl&auml;che 160 qm</li>
<li>1 Badezimmer mit Duschbadewanne (7,9 qm)</li>
<li>1 G&auml;ste-WC</li>
<li>1 B&uuml;ro (12,7 qm)</li>
<li>1 Wohn- und Esszimer (40 qm)</li>
<li>1 Elternschlafzimmer (22,69 qm) mit einem separaten, begehbaren<br>
Schrankzimmer (4,9 qm) und einem direktem Zugang<br>
zum Badezimmer</li>
<li>1 Schlafzimmer (13,17 qm)</li>
<li>1 Schlafzimmer (15,35 qm)</li>
<li>1 Arbeitszimmer (14,65 qm) im Empfangsbereich</li>
</ul>
</p>
<br>
<b><img src="./images/muehli_pic1.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Ansicht von der Strasse</b>
<p>Das Haus hat eine Garage, sowie 5 Parkpl&auml;tze vor dem Haus.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic4.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Gartenanlage - Blick Richtung Hang</b>
<p>Ziel der Gartengestaltung war ein pflegeleichter Garten mit einem w&ouml;chentlichen Arbeitsaufwand von max. einer halben Stunde.
Der Hang wurde mit einer Folie unterlegt und mit Granitsteinen bedeckt.
Vorg&auml;ngig musste mit Hilfe eines Baggers die "Bitterorange" und der "Larix" gepflanzt werden.
Das Bord wird mit einer Betonmauer gest&uuml;tzt. Unterhalb der Mauer befindet sich ein Heidegarten mit Biotope.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic16.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Balkon und Terasse</b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic2.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Wohnzimmer, Blick Richtung Essplatz</b>
<p>Es wurde besonderer Wert auf eine stilvolle Inneneinrichtung gelegt. Die Farbauswahl ist zeitlos, klassisch und diskret. Die Polstergruppe, das Sideboard mit der Granitabdeckung, sowie das Glasgestell f&uuml;r die Musikanlage, wurden zu den bestehenden M&ouml;beln erg&auml;nzt. Der Marmor-Clubtisch ist eine Spezialanfertigung aus Lasa-Marmor. Die B&ouml;den wurde mit pflegeleichten Feinsteinplatten in der Farbe tope (grau/beige) ausgestattet.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic3.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Wohnzimmer</b>
<p>Gem&auml;ss des Inneneinrichtungskonzeptes schaffen Designer-Polsterm&ouml;bel im Wohnzimmer ein angenehmes Ambiente.<br>
Fenster- und T&uuml;rgriffe verf&uuml;gen &uuml;ber das gleiche Design.<br>
Die W&auml;nde wurden mit einer diskreten, abwaschbaren Tapete tapeziert.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic11.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">K&uuml;che</b>
<p>Auch in der K&uuml;chenplanung wurde das Inneneinrichtungskonzept konsequent umgesetzt, um Ergonomie und Ambiente zu vereinigen.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic12.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">B&uuml;ro</b>
<p>Die Inneneinrichtung des B&uuml;roraumes besteht aus einem Victoria-Pult und schwedischen DUX-M&ouml;beln.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic6.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Treppenaufgang zur Galerie</b>
<p>Die W&auml;nde wurden mit einem attraktiven Abriebputz versehen.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic5.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Kleine Galerie</b>
<p>Die Galerie verbindet die Zug&auml;nge zu Schlafzimmern und Bad mit dem Treppenhaus.<br clear="all"></p>
<br><br>


<b><img src="./images/muehli_pic10.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Schlafzimmer</b>
<p>Schlafzimmer mit grossem TV und silbernen, abwaschbaren Tapeten. Die B&ouml;den bestehen aus Feinsteinplatten, welche bis ins Badezimmer verlegt wurden.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic8.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Schlafzimmer mit Teilansicht Schrankraum</b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic7.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Schiebet&uuml;re Schrankraum</b>
<p>In das Schlafzimmer wurde ein begehbarer Schrankraum integriert.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic9.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Offener Schrankraum</b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic15.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Badezimmer</b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic13.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Juniorenzimmer</b>
<p>Mit Z&uuml;co-B&uuml;rostuhl.<br clear="all"></p>
<br><br>

<b><img src="./images/muehli_pic14.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Atelier</b>
<p>Mit B&uuml;gelstation, diversen N&auml;hmaschinen und Strickmaschinen.<br clear="all"></p>
<br><br>

<!--Bauplaene-->

<b><img src="./images/muehli_plan7.jpg" width="450" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_plan1.jpg" width="450" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_plan2.jpg" width="450" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_plan6.jpg" width="450" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_plan4.jpg" width="450" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/muehli_plan5.jpg" width="450" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>
<a href="referenzen.php"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
</div>

<!--End Content -->
</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
