<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h1>Veranstaltungen</h1>
<br>
<br>
<br>

<div class="Veranstalt">


<?php if(1==1){ ?>


<h4>Geplante Veranstaltungen und Tag der offenen T&uuml;r in den n&auml;chsten Wochen:</h4>

<b>Wir sind am Planen von Vortr&auml;gen. </b>
<p>&nbsp;</p>
<p>&nbsp;</p>

<b>OKTOBER 2017</b>
<p>An einem Samstagnachmittag, im Oktober 2017 (siehe Inserat im Wochenblatt) 

Maisonettewohnungs-Besichtigung in Oberdornach/SO, Landskronstrasse, 
206 m2 Wohnfl&auml;che, Baujahr 2004, 5,5 Zimmer, beide Stockwerke rollstuhlg&auml;ngig, pflegeleicht, hell und freundlich, 2 Tiefgaragenparkpl&auml;tze, offene K&uuml;che, Duschsauna.<br>
<a href="downloads/Veranstaltung-Oktober-2017.pdf" target="_blank">FLYER</a><br><br>

<b>NOVEMBER 2017</b>

</p>
<p>An einem Samstagnachmittag, im November 2017 (siehe Inserat in den Wochenbl&auml;ttern)
  stellen wir Ihnen in der Keramik Laufen AG, Wahlenstrasse 46, 4242 Laufen die Maisonette-Eigentumswohnungen ab CHF 665.000.—inkl. Parkplatz  in Breitenbach und das Investorenobjekt vor. Das heisst, es bietet sich die Möglichkeit das ganze MFH mit 4 Maisonette-Wohnungen zu kaufen oder nur einzelne 3,5 oder 4,5 Zimmer-Eigentumswohnungen.<br>
  <a href="downloads/Veranstaltung-November-2017.pdf" target="_blank">FLYER</a>
</p>
</div>
<div class="Veranstalt">
<h4>Letzte Veranstaltungen</h4>
<b>Tag der offen T&uuml;r, Samstag, 16.September 2017 </b>

<p>Anl&auml;sslich des 11-j&auml;hrigen Bestehens der Firma BBINW, hatte die Firma in Arlesheim und Zeihen (Fricktal) <br> 
die Kunden zur Firmenbesichtigung und Firmenvorstellung eingeladen.<br>
  <a href="downloads/Tag-der-offenen-Tuer.pdf" target="_blank">FLYER</a></p>
<p>&nbsp;</p>
<p><strong>Ausstellung am BAETTWILER Antiquit&auml;ten, Floh- und und Buurem&auml;rt, 1. September</strong> <strong>2017</strong><br>
  <a href="downloads/2017_09_01_Ausstellung-Flomi-Baettwil.pdf" target="_blank">FLYER</a> </p>
  
  <?php } ?>
  
  
</div>
<br>
<br>
<br>

<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>


<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
