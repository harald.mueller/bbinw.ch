<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2>Restaurant 'Zum R&ouml;ssli' in Olsberg</h2>

<p>
Der Landgasthof R&ouml;ssli wurde vermutlich im Sp&auml;tmittelalter (15. Bis 16. Jhr.) gebaut und im 19. Jahrhundert erneuert. Der Verhandlungspreis von CHF 1.25 Mio. beinhaltet die saubere Inventarstruktur, eine Homepage, eine hochwertig eingerichtete Wirtewohnung mit sch&ouml;nen Eichenparkettb&ouml;den und einer grossen Terrasse. WIR-Anteil von 20 bis 30 Prozent w&auml;re m&ouml;glich. M&ouml;gliche Wohn- Nutzfl&auml;che ca. 600 m2. Vollgeschosse, Geb&auml;ude- und Firsth&ouml;he, Grenzabst&auml;nde wird im Gegenseiten Einvernehmen mit dem Gemeinderat bestimmt. Mit dem angrenzenden Kloster k&ouml;nnen Synergien eingegangen werden (kloster-olsberg.ch). Der Name des Hauses Zum R&ouml;ssli bezieht sich auf die urspr&uuml;ngliche Funktion als Relais hin (Ort zum Auswechseln der Pferde), in diesem Fall am Knotenpunkt der Stellstrassen von und nach Arisdorf, Rheinfelden und Magden. Mit dem Haus war eine Wirtschaft f&uuml;r Fuhrleute verbunden. Eigengew&auml;chse und eigene Milch- und Fleischprodukte wurden offeriert. Aus diesem Grunde gliederte sich dem Haus ein landwirtschaftlicher Betrieb an. Erbaut vermutlich im Sp&auml;tmittelalter (15. bis 16. Jhr.) nach dem Dreissigj&auml;hrigen Krieg und im 19. Jahrhundert erneuert. Die zugeh&ouml;rigen landwirtschaftlichen Nebengeb&auml;ude, eine Stallscheune und ein Schopf wurden in den 70-er Jahren abgerissen. Ein westseitig vorgelagertes St&ouml;ckli wurde 1950 abgerissen - damit b&uuml;sste die Bebauung auf der Ostseite des Dorfplatzes ihren kleinteiligen Charakter ein. Das Gasthaus R&ouml;ssli, das in seiner heutigen Gestalt auf das 17. Jahrhundert zur&uuml;ckgehen d&uuml;rfte, tritt als ortsbildpr&auml;gender Mauerbau in Erscheinung, welches vor ca. 21 Jahren total saniert wurde. Der Scheunenteil wurde bei der Gesamtsanierung zu einem S&auml;li ausgebaut. Das durchzogene steile Knickdach ist mit Biberschwanz-Ziegeln eingedeckt. Mit Ausnahme der verputzten Rieggiebeln sind die Fassaden aus verputztem Bruchsteinmauerwerk erstellt.
</p>
<br><br>
<div class="compressContainer">
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_1_800x600-75.jpg" ><br><br>
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_2_800x600-75.jpg" ><br><br>
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_3_800x600-75.jpg" ><br><br>
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_4_800x600-75.jpg" ><br><br>
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_G1_800x600-75.jpg" ><br><br>
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_G2_800x600-75.jpg" ><br><br>
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_G3_800x600-75.jpg" ><br><br>
<img width="450" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_G4_800x600-75.jpg" ><br><br>
</div>
<a href="referenzen.php"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
