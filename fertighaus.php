<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2 align="left">Suchen Sie nach einem individuellen Fertighaus?<br></h2>

<img src="images/fertig_bild.jpg" width="350" align="left" vspace="0" hspace="20" >
<br>
<br>

<p>Suchen Sie nach einer massgeschneiderten
<br> 
L&ouml;sung f&uuml;r Ihr Eigenheim?</p>

<br>
<img src="images/fertig3.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">
<br clear="left"> 
<br>
<p>Von der Grundst&uuml;ckssuche, &uuml;ber die Planung<br>
bis zur Finanzierung unterst&uuml;tzen wir Sie bei der<br>
Realisierung Ihres Traumhauses</p>
<br>
<h2>Einige Beispiele:</h2>
<p>
<b>
<a href="musterhaus_bassersdorf.php">Muster-Fertighaus in Bassersdorf<br><br>
<img width="350px" src="./images/fertig1.jpg" ></a><br><br>
<a href="musterhaus_frick.php">Muster-Fertighaus in Frick<br><br>
<img width="350px" src="./images/fertig1b.jpg" ></a><br><br>
</b>
</p>
<h2>Bauen Sie Ihr pers&ouml;nliches Fertighaus</h2>
<p>
Planbar nach Ihren W&uuml;nschen!Ganzheitlich wohnen und leben in guter Nachbarschaft.<br>
Ein individualisiertes Fertighaus zeigt eine unverwechselbare Handschrift.<br><br>
<i>Ein Haus mit vier Gesichtern!</i><br><br>
Sie werden ihn lieben: den weitl&auml;ufigen Wohn- und Essraum, von dem aus Sie in w&auml;rmeren Jahreszeiten direkt auf Ihre sonnige Terrasse gelangen. Besonders praktisch auch der separate Hauswirtschaftsraum, wo man die W&auml;sche ruhig mal W&auml;sche sein lassen kann. T&uuml;r zu und gut.
Jede Menge Entfaltungsm&ouml;glichkeiten f&uuml;r die ganze Familie er&ouml;ffnet das Obergeschoss mit drei grossen Zimmern und echtem Komfortbad.<br><br>
<i>Welcher Dachtyp sind Sie?</i><br><br>
Jetzt m&uuml;ssen Sie sich nur noch f&uuml;r ein Dach entscheiden, dass zu Ihnen passt. Klassisches Satteldach, elegantes Walmdach, innovatives Pultdach oder funktionelles Flachdach? Und abh&auml;ngig davon die optimale Position und Gr&ouml;&szlig;e der Fenster. Sie haben die Wahl.Als Traditionsunternehmen bieten wir handwerklich ausgereifte L&ouml;sungen und h&ouml;chstm&ouml;gliche Sicherheit durch jahrzehntelange Erfahrung. Mit Ansprechpartnern und umfassendem Service vor Ort. Regional ganz nah f&uuml;r Sie da.
<br>
<br>
<i>Wir offerieren Wohnkonzepte, die &uuml;berzeugen:</i>
<ul>
<li>mit anspruchsvoller, individueller Architektur</li>
<li>in solider, zukunftsweisender Bauweise und Qualit&auml;t</li>
<li>energiesparend, wohngesund und umweltgerechtWir bauen konsequent und serienm&auml;ssig nach dem MINERGIE Standard, Definition energieeffizienten und wohngesunden Bauens. Zertifiziert und f&ouml;rderf&auml;hig. Massgeblich bestimmend f&uuml;r die Wirtschaftlichkeit und den zuk&uuml;nftigen Immobilienwert Ihres Hauses.</li>
</ul>
<i>Ein modernes Fertighaus ist energieeffizient und wohngesund durch:</i>
<ul>
<li>solaroptimierte Planung</li>
<li>MINERGIE-W&auml;nde</li>
<li>extrastarke Speziald&auml;mmung auch im Dach</li>
<li>w&auml;rmebr&uuml;ckenfreie Konstruktion</li>
<li>3-Scheiben-Thermoverglasung</li>
<li>Thermokeller</li>
</ul>
</p>

<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
