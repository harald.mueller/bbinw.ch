<?php include 'inc.head.html';?>
<head><style>div.gallery-container .thumbs img.example-image {width:100%;}</style></head>
	<body>
		<!-- <style>div.gallery-container .thumbs img.example-image {width:100%;}</style> -->
		<?php include 'inc.menuStickyTop.html';?>
			<div class="container">

				<?php include 'inc.secNavBar.html';?>

				<div class="container">
					<br>
					<h1 class="moveDown40">Mietobjekte</h1><h2 class="goneForGood">BBINW baut mit Innovation f&uuml;r die Nordwestschweiz</h2>
				</div>

				<nav>
					<div class="far-left">
						<section>
							<br>
							<h2>BBIE 30001</h2>
							<p>4414, F&uuml;llinsdorf, Schweiz</p>
							<p>Zu vermieten 3,5 oder 4,5 Zimmer EFH mit Hobbyraum sep. B&uuml;ro und Garage Rheinstrasse in F&uuml;llinsdorf</p>
							<div class="gallery-container">
								<div class="thumbs">
									<br>
									<a class="example-image-link" href="bbinw-bilder/test/BIE_30001_1_800x600-75_vermietet.jpg" data-lightbox="example-set" data-title="Zu vermieten 3,5 oder 4,5 Zimmer EFH mit Hobbyraum sep. B&uuml;ro und Garage Rheinstrasse in F&uuml;llinsdorf."><img class="example-image" src="bbinw-bilder/test/thumbs/BIE_30001_1_800x600-75_vermietet.jpg" alt=""></a><br><br>
								</div>
							</div>
							<a class="follow-to-the-next-page" href="downloads/BIE_30001.pdf" target="_blank" title="">Schaufensterwerbung</a><br>
							<a class="follow-to-the-next-page" href="expose_test.php5?objektnummer=BIE%2030001" title="">Kurzdokumentation</a><br>
							<a class="follow-to-the-next-page" href="flyer_test.php5?objektnummer=BIE%2030001" title="">Dokumentation</a><br>
							<a class="follow-to-the-next-page" href="sendmailinteresse.php?objektnummer=BIE%2030001"><b>Bin intessiert</b></a><br>
							<a class="follow-to-the-next-page" href="sendmail.php?objektnummer=BIE%2030001"><b>Link senden</b></a></p>
						</section>
					</div>
				</nav>

				<div class="content">

					<div class="content-left">
						<section>
							<br>
							<h2>BIE 700012</h2>
							<p>4414, Arlesheim, Schweiz</p>
							<p>Angrenzende B&uuml;ror&auml;ume per sofort zu vermieten Neumattstrasse 8, 4144 Arlesheim</p><br>
							<div class="gallery-container">
								<div class="thumbs">
									<br>
									<a class="example-image-link" href="bbinw-bilder/test/BIE_700012_1_800x600-75.jpg" data-lightbox="example-set" data-title="Angrenzende B&uuml;ror&auml;ume per sofort zu vermieten Neumattstrasse 8, 4144 Arlesheim"><img class="example-image" src="bbinw-bilder/test/thumbs/BIE_700012_1_800x600-75.jpg" alt=""></a><br><br>
								</div>
							</div>
							<a class="follow-to-the-next-page" href="objekt_test.php5?objektnummer=BIE%20700012" title="">Schaufensterwerbung</a><br>
							<a class="follow-to-the-next-page" href="expose_test.php5?objektnummer=BIE%20700012" title="">Kurzdokumentation</a><br>
							<a class="follow-to-the-next-page" href="flyer_test.php5?objektnummer=BIE%20700012" title="">Dokumentation</a><br>
							<a class="follow-to-the-next-page" href="sendmailinteresse.php?objektnummer=BIE%20700012"><b>Bin intessiert</b></a><br>
							<a class="follow-to-the-next-page" href="sendmail.php?objektnummer=BIE%20700012"><b>Link senden</b></a></p>
						</section>
					</div>

					<div class="content-right">
						<article>
							<br>
							<h2>Andere Mietobjekt gesucht?</h2>
							<p>BBINW findet es f&uuml;r Sie!</p>
							<p>Gerne suchen wir Ihre Wunschobjekt. Bitte geben Sie uns per Mail auf immobilien@bauland-nw.ch<br><br><br>
							<div class="gallery-container">
								<div class="thumbs">
									<a class="example-image-link" href="bbinw-bilder/test/vermietung.jpg" data-lightbox="example-set" data-title="Gerne suchen wir Ihre Wunschobjekt. Bitte geben Sie uns per Mail auf immobilien@bauland-nw.ch"><img class="example-image" src="bbinw-bilder/test/thumbs/vermietung.jpg" alt=""></a><br><br>
								</div>
							</div>
							<p>Ihre Suchw&uuml;nsche bekannt. Preis ab, Preis bis, Lage, Anzahl Zimmer, Gr&ouml;sse der Wohnung, Ausbaustandart, Anzahl
							 G&auml;ste-WC und Badezimmer, Best&auml;tigung f&uuml;r den Suchauftrag gegen entsprechende Aufwandsentsch&auml;digung gem. SVIT.</p><br>
							<a class="follow-to-the-next-page" href="downloads/Leistungsbeschrieb_Verwaltung_Bewirtschaftung.pdf" target="_blank" title="">Leistungsbeschreibung: Erstvermietung (PDF)</a></p>
							<a class="follow-to-the-next-page" href="downloads/erstvermietung_20080709.pdf" target="_blank" title="">Leistungsbeschreibung: Verwaltung und Bewirtschaftung von Mietliegenschaften (PDF)</a><br>
						</article>
					</div>

				</div>

			</div>

					<div class="clearfix"></div>
				<script src="js/lightbox-plus-jquery.min.js"></script>
			<?php include 'inc.footer.html';?>
		<?php include 'inc.menuitemsM.html';?>
	</body>
</html>
