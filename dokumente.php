<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2>Download Bereich</h2>
<p>Hier k&ouml;nnen Sie bequem unsere Formulare und<br>Dokumente im pdf Format downloaden</p><br><br>
<p>
<span class="normal">
<a class="link" href="./downloads/gutschein.pdf" target="_blank">&rArr;&nbsp;Gutschein</a>
<br><br><a class="link" href="./downloads/maklervertrag20090711.pdf" target="_blank">&rArr;&nbsp;Maklervertrag</a>
<br><br><a class="link" href="./downloads/maklervertrag-gastro-20080301.pdf" target="_blank">&rArr;&nbsp;Maklervertrag f&uuml;r Gastro- und landwirt. Betriebe</a>
<br><br><a class="link" href="./downloads/vorvertrag20080826.pdf" target="_blank">&rArr;&nbsp;Vorvertrag</a>
<br><br><a class="link" href="./downloads/wunschformular20101108.pdf" target="_blank">&rArr;&nbsp;Wunschformular</a>
<br><br><a class="link" href="./downloads/wunschformulaeng20070402.pdf" target="_blank">&rArr;&nbsp;Application</a>
<br><br><a class="link" href="./downloads/checkliste_kueche_20080709.pdf" target="_blank">&rArr;&nbsp;Checkliste K&uuml;chenplanung</a>
<br><br><a class="link" href="./downloads/Leistungsbeschrieb_Verwaltung_Bewirtschaftung.pdf" target="_blank">&rArr;&nbsp;Leistungsbeschreibung: Verwaltung und Bewirtschaftung von Mietliegenschaften</a>
<br><br><a class="link" href="./downloads/AB_Hausverwaltungsvertrag.pdf" target="_blank">&rArr;&nbsp;Allgemeine Bedingungen zum Hausverwaltungsvertrag</a>
<br><br><a class="link" href="./downloads/Hausverwaltungsvertrag.pdf" target="_blank">&rArr;&nbsp;Hausverwaltungsvertrag</a>
<br><br><a class="link" href="./downloads/erstvermietung_20080709.pdf" target="_blank">&rArr;&nbsp;Leistungsbeschreibung: Erstvermietung</a>
<br><br><a class="link" href="./downloads/allgemeinegeschaeftsbedingungen20080401.pdf" target="_blank">&rArr;&nbsp;Allgemeine Gesch&#228;ftsbedingungen (AGB) BBINW</a>
</span>
</p>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<br>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
