<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2>Impressum</h2>
<span class="normal"><p><i>Name und Anschrift des Betreibers:</i><b><br><br>BBINW - Arlesheim</b><br>Neumattstrasse 8<br>4144 Arlesheim<br><br>061 599 27 46 phone<br>061 599 47 45 fax<br>076 413 1936 mobile<br>076 418 1938 mobile<br>
<br><b>Gesch&auml;ftsf&uuml;hrerin</b><br>Frau Susanne Bieli<br><br><i>E-Mail Adresse des Betreibers:</i><br><b>E-Mail:</b> <a class="link" href="mailto:immobilien@bauland-nw.ch">immobilien@bauland-nw.ch</a><br><br><b>Skype-Name: </b>bauland-nw<br><br>
<a class="link" href="mailto:%28keine%20Angaben%29">webmaster@bauland-nw.ch</a><br>
<br>
<i>Urheberrechtshinweis:</i><br>
<br>
Alle Inhalte dieses Internetangebotes, insbesondere Texte,
Fotografien und Grafiken, sind urheberrechtlich gesch&uuml;tzt
(&copy; Copyright BBINW Arlesheim 2009). Das Urheberrecht liegt,
soweit nicht ausdr&uuml;cklich anders gekennzeichnet, bei
BBINW Arlesheim. Bitte fragen Sie BBINW Arlesheim, falls Sie
die Inhalte dieses Internetangebotes verwenden m&ouml;chten.
<br><br>
Wer gegen das Urheberrecht verst&ouml;&szlig;t (z.B. die Inhalte
unerlaubt auf die eigene Homepage kopiert), macht sich gem&auml;ss
Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt
und muss Schadensersatz leisten. Kopien von Inhalten k&ouml;nnen
im Internet ohne gro&szlig;en Aufwand verfolgt werden.
<br><br>
BBINW Arlesheim 01. Dezember 2009<br>
<br>
</p></span>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
