<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2 >Einfamilienh&auml;user in Arlesheim</h2>
<p>
 Der Ausbau der Einfamilienh&auml;user entspricht den heutigen Qualit&auml;tsanforderungen, wobei besonderer Wert auf eine solide und nachhaltige Bauweise gelegt wird. Der Innenausbau der H&auml;user ist im Rahmen eines definierten Budgets grunds&auml;tzlich individuell gestaltbar, dazu geh&ouml;rt die Auswahl der Oberfl&auml;chenbekleidung (Boden, Wand, Decke) sowie Hersteller, Anzahl und Gr&ouml;sse von K&uuml;chen- und Sanit&auml;rapparaten. Dei Reihen-EFH entstehen am Fusse des Goetheanums, nahe am B&auml;chlein. Die H&auml;user bieten mit der N&auml;he zur Natur und der g&uuml;nstigen Erreichbarkeit vom Dorfzentrum und der Stadt Basel den perfekten Wohnraum f&uuml;r eine Kleinfamilie oder Paare. Gasheizung, W&auml;rmeverteilung mit Bodenheizung, Einzelraumregulierung. Einbauschr&auml;nke, Plattenbodenbel&auml;ge f&uuml;r K&uuml;che, Bad und WC, Zimmer im OG sowie Gang mit Klebeparkett. Wandbel&auml;ge mit Abrieb oder Glattstrich, Decke Weissputz. Umgebung mit Rasensaat, Sitzplatz mit Holzrost. Weitere Optionen sind die individuelle Planung.
</p>
<br><br><br>
<i>
<img width="462" src="./images/arlesheim1.jpg" ><br><br>
<img width="462" src="./images/arlesheim2.jpg" ><br><br>
<img width="462" src="./images/arlesheim3.jpg" ><br><br>
<img width="462" src="./images/arlesheim4.jpg" ><br><br>
<img width="462" src="./images/arlesheim5.jpg" ><br><br>
<img width="462" src="./images/arlesheim6.jpg" ><br><br>
<img width="462" src="./images/arlesheim7.jpg" ><br><br>
</i>
<a href="referenzen.php"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
