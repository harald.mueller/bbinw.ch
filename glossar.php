<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>

<h1>Immobilienglossar</h1>

<b>Annuit&auml;t</b><br>
Regelm&auml;ssige Zahlung in gleichbleibender H&ouml;he, die sich aus Hypothekarzins- und Amortisationsanteil zusammensetzt.
<br><br>
<b>Ausbesserung bei Mietobjekten, kleiner Unterhalt</b>
Unterhaltsarbeit zur Beseitigung eines Mangels, der vom Mieter nach Ortsgebrauch auf eigene Kosten beseitigt werden muss (OR Art. 259), z. B. der Ersatz einer defekten  Kochherdplatte oder eines Rolladengurtes.
<br><br>
<b>Attikawohnung</b><br>
Wohnung in zur&uuml;ckversetztem oberstem Geschoss
<br><br>
<b>Bruttogeschossfl&auml;che (BGF)</b><br>
Summe aller ober- und unterirdischen Geschossfl&auml;chen einschliesslich der Mauer- und Wandquerschnitte, abz&uuml;glich aller nicht dem Wohnen und nicht dem Arbeiten dienenden und hierf&uuml;r nicht verwendbaren Fl&auml;chen.
<br><br>
<b>Bruttowohnfl&auml;che - SIA-Norm 381/3</b><br>
Fl&auml;che s&auml;mtlicher innerhalb einer Wohnung liegender Wohnr&auml;ume, Nebenr&auml;ume, G&auml;nge und Treppen; nicht aber ausserhalb liegende Fl&auml;chen, wie Treppenh&auml;user, Terrassen und offene Balkone sowie nicht bewohnbare Keller- und Dachgeschossr&auml;ume.
<br><br>
<b>Geschossfl&auml;che (GF)</b><br>
Die GF inkl. der Aussenwandkonstruktionen stellt zum Beispiel die Bezugsgr&ouml;sse f&uuml;r den Z&uuml;richer Wohnbaukostenindex dar.
<br><br>
<b>Konsolidierung</b><br>
Umwandlung eines Baukredits in eine Hypothek
<br><br>
<b>Kubikmeterpreis: Grundlagen</b><br>
Als Berechnungsgrundlage gilt seit dem Jahre 2003 die SIA-Norm 416 (Vorg&auml;nger SIA -Norm 116 aus dem Jahre 1952). In der SIA-Norm 416 werden keine umst&auml;ndlichen Zuschl&auml;ge f&uuml;r zum Beispiel D&auml;cher und Dachaufbauten kalkuliert. Das effektive Geb&auml;udevolumen wird berechnet und mit dem Kennwert des Kubikmeterpreises, der die reinen Baukosten ohne Umgebung und Nebenkosten wiedergibt. Die Kubikmeterpreise werden f&uuml;r die Berechnung von Grobkostensch&auml;tzungen mit einer Genauigkeit von 20 bis 25 % f&uuml;r die Machbarkeitsstudie eingesetzt. In der eigentlichen Vorprojektphase wird mit einer Genauigkeit von 15 bis 20 % berechnet. Diese Berechnung ist oft die Kostengrundlage bei der Baueingabe. Der Kubikmeterpreis, der vom Z&uuml;richer Index der Wohnbaupreise jeweils f&uuml;r ein Musterhaus berechnet wird, betrug im Jahre 2007 CHF 618.--/Kubikmeter. 
<br><br>
<b>Lex Friedrich, Furgler, von Moos</b><br>
Bundesgesetz &uuml;ber den Erwerb von Grundst&uuml;cken durch Personen im Ausland. Das Gesetz regelt, inwieweit Personen mit Wohnsitz im Ausland Grundst&uuml;cke in der Schweiz erwerben d&uuml;rfen.
<br><br>
<b>Liegenschaftssteuer, Grundsteuer</b><br>
Wird in 14 Kantonen zus&auml;tzlich zur Verm&ouml;gens- bzw. Kapitalsteuer auf dem Grundeigentum erhoben.
<br><br>
<b>Luft-Wasser-W&auml;rmepumpe</b><br>
Bei dieser W&auml;rmepumpe wird die Umgebungsluft als Energiequelle genutzt. Das kann beispielsweise die Luft in den Kellerr&auml;umen sein, mit der die W&auml;rmepumpe warmes Wasser produziert. Einen vorteilhaften Einsatz findet die Luft-Wasser-W&auml;rmepumpe bei einem Haus mit niedrigem Heizw&auml;rmebedarf.
<br><br>
<b>Maisonette-Wohnung, Duplex-Wohnung</b><br>
Wohnung auf zwei Stockwerken
<br><br>
<b>Nebenkosten (NK)</b><br>
Sind f&uuml;r vertraglich vereinbarte Leistungen des Vermieters zu entrichten, die nicht bereits im Mietzins inbegriffen sind, z. B. die Kosten f&uuml;r Heizung, Warmwasser, Treppenhausreinigung, Hausbetreuung, Allgemeinstrom, Wasser/Abwasser, Serviceabonnement f&uuml;r Waschautomaten und Tumbler, Kehrichtabfuhr, Gartenunterhalt, Geb&auml;udeversicherung und Verwaltung. NK werden vom Mieter f&uuml;r eine Abrechnungsperiode durch Pauschalbetr&auml;ge abgegolten oder durch Akontozahlungen vorgeschossen. Die NK m&uuml;ssen den tats&auml;chlichen Aufwendungen entsprechen (OR Art. 257b). Bei der Pauschalierung muss der Vermieter auf Durchschnittswerte dreier Jahre abstellen (Art. 4 VMWG).
<br><br>
<b>Nettowohnfl&auml;che (NWF)  - SIA-Norm 118</b><br>
Bei Wohnbauten entspricht die Nettowohnfl&auml;che der Fl&auml;cher aller begeh- und belegbaren Fl&auml;chen innerhalb einer Wohnung einschliesslich der Fl&auml;che von mobilien Bauteilen, Einbauten (Schr&auml;nke, Chemine&eacute;) und internen Treppen (Weisungen Schweiz. Vereinigung kant. Grundst&uuml;ckbewertungsexperten).
<br><br>
<b>Pacht</b><br>
Zeitweise Ueberlassung einer nutzbaren Sache (z. B. landw. Grundst&uuml;ck, Hotel, Betrieb), oder eines nutzbaren Rechts (z. B. Fischereirecht). Die gesetzlichen Bestimmungen &uuml;ber die Pacht sind im OR Art. 275 ff und in der Verordnung &uuml;ber die  Mieter und Pacht von Wohn- und Gesch&auml;ftsr&auml;umen (VMWG) geregelt.
<br><br>
<b>Pellets</b><br>
Ein durchschnittliches EFH mit einem W&auml;rembedarf von 10 kW ben&ouml;tigt einen Jahresbedarf von rund vier Tonnen Pellets. Der Ersatz einer &Ouml;lfeuerung ist in der Regel problemlos m&ouml;glich.
<br><br>
<b>SIA-Norm 102 f&uuml;r Leistungen und Honorare der Architekten</b><br>
In dieser Norm ist geregelt, dass die Baueingabe auf der Basis einer Kostensch&auml;tzung einer kubischen Berchnung gem. SIA-Norm 416 erfolgen kann. Die Kostengenauigkeit daf&uuml;r betr&auml;gt +/- 20 %.
<br><br>
<b>SIA-Norm 416 f&uuml;r Fl&auml;chen und Volumen von Geb&auml;uden</b><br>
Hier werden die Fl&auml;chenberechnungen in 3 Hauptgruppen unterteilt: Grundst&uuml;cksfl&auml;che, Geschossfl&auml;che und Aussengeschossfl&auml;che.
<br><br>
<b>Sole-Wasser-W&auml;rmepumpe</b><br>
Diese nutzt die Erdw&auml;rme. Es werden Elektrokollektoren oder Erdsonden aus unverrottbaren Kunststoffrohren, als geschlossene Kreisl&auml;ufe verwendet. Diese Kollektoren werden waagrecht im Boden Ihres Grundst&uuml;ckes verlegt, in Form von Schleifen. Dabei ist eine Tiefe von 1,0 - 1,2 m  ausreichend, um der W&auml;rempume die notwendige Energiemenge zu liefern, die sie zum Beheizen Ihres Hauses ben&ouml;tigen. Eine W&auml;rmepumpen-Anlage ist extrem wartungsarm.
<br><br>
<b>Stockwerkeigentum (StWE)</b><br>
Miteigentumsanteil an einem Grundst&uuml;ck, der dem Miteigent&uuml;mer das Sonderrecht gibt, bestilmmte Teile eines Geb&auml;udes ausschliesslich zu benutzen und innen auszubauen (ZGB Art. 712a).
<br><br>
<b>Verkehrswertsch&auml;tzungen</b><br>
Der <i>Real-</i> oder auch <i>Sachwert</i> wird aufgrund von Elementrichtwerten wie z. B. pro m2 Deckenplatten, Fassaden, Fenster, Dach etc. bewertet. Die Berechnung basiert im Wesen und Inhalt auf der Anlagekostenberechnung gem&auml;ss Baukostenplan (BKP). Der Realwert kann von den Anlagekosten abweichen (z. B. wegen Entwertung, unrationeller Bauweise, schlechter Bauqualit&auml;t, unterschiedlicher Wertung des Landes etc.). Der Landwert ist gem&auml;ss Grundrestwertmethode (kap. Mietwert/- R&uuml;ckw&auml;rtsrechnung) und Vergleichswerte einzusetzen.
<br>
Die <i>Ertragswertberechnung</i> basiert auf der sogenannten R&uuml;ckw&auml;rtsrechnung, d. h. der j&auml;hrliche Mietwert (gem&auml;ss Mieterspiegel/Vertrag und Markt) wird mit einem angemessenen, kostendeckenden Zinssatz kapitalisiert.
<br>
Der <i>Verkehrswert (ermittelt aus Real- und/oder Ertragswert)</i> verk&ouml;rpert den Liegenschaften-Marktwert, wie er bei einem Kauf oder Verkauf unter normalen Verh&auml;ltnissen (ohne R&uuml;cksicht auf ungew&ouml;hnliche oder pers&ouml;nliche Verh&auml;ltnisse) erzielt werden kann. 
<br>
Der <i>Verkehrswert</i> entspricht dem Marktwert wie er unter normalen Bedingungen bei freiem Spiel von Angebot und Nachfrage zu erzielen ist.     
<br><br>
<b>W&auml;rmepumpe</b><br>
W&auml;rmepumpen nutzen Umweltenergien aus dem Erdreich, dem Wasser oder der Luft. Von 100 % Energiebedarf f&uuml;r die Beheizung eines Geb&auml;udes liefert z. B. das Erdreich 75 %. Die restlichen 25 % stammen vom Stromeinsatz f&uuml;r den Antrieb der W&auml;rmepumpe. Vorteile von W&auml;rmepumpen: Es werden nur ca. 25 % Fremdenergie ben&ouml;tigt. Wenig Betriebsraum, keinen Schornstein. Im Vergleich zu einer Gas- oder &Ouml;lheizung senken die W&auml;rmepumpen die CO<sub>2</sub> Emissionen um 30 %. W&auml;rmepumpen werden in der Heizungs- und L&uuml;ftungstechnik eingesetzt und sie unterscheiden sich nach den Energiequellen, die sie nutzen und nach den Medien, an die sie die Energie wieder abgeben. Zur groben Unterscheidung sprechen wir deshalb von den Gruppen: Luft-Wasser-, Wasser-Wasser-, Sole-Wasser- und Luft-Luft-W&auml;rmepumpen.
Alle W&auml;rmepumpen arbeiten nach dem gleichen Prinzip.
Man nutzt die energieg&uuml;nstigen Bedingungen der K&auml;ltetechnik, nur mit umgekehrten Vorzeichen! Bei einer W&auml;rmepumpe steht nicht die K&uuml;hlung im Vordergrund, sondern das Abfallprodukt der K&uuml;hlung - die W&auml;rme!
Beispiel K&uuml;hlschrank. An der R&uuml;ckseite des K&uuml;hlschrankes steigt die warme Luft auf.
Wenn das Ziel aber nicht die K&uuml;hlung w&auml;re, sonder der Heizeffet , dann w&auml;re Ihr K&uuml;hlschrank eine W&auml;rmepumpe. Und zwar eine Luft-Luft-W&auml;rmepumpe. Denn der Luft im Innenraum des K&uuml;hlschrankes wird Energie entzogen (K&uuml;hleffekt) und diese Energie wird in W&auml;rme umgesetzt und an die Umgebungsluft (Heizeffekt) wieder abgegeben. Kommt eine W&auml;rmepumpe in einem Haus zum Einsatz, so spendet die Sonne drei Viertel der erforderlichen Energie (gespeicherte Sonnenw&auml;rme in Erde, Wasser und Luft). Mit dieser W&auml;rem und einem Viertel Antriebsenergie (Strom), beheizt die W&auml;rmepumpe Ihr Haus oder Ihre Wohnung Tag und Nacht, Sommer und Winter. Eine W&auml;rmepumpen-Anlage ist extrem wartungsarm. 
<br><br>
<b>Wohnfl&auml;che (WF)</b><br>
Diese wird durch die Fl&auml;che innerhalb der Aussenmauern, inkl. Bad/WC sowie der Innenw&auml;nde berechnet. Diese gilt als Grundlage f&uuml;r die Mietpreisberechnung und basiert auf einer Definition der Vereinigung der Z&uuml;richer Immobilienunternehmer (VZI). In den Inseraten werden die WF aufgef&uuml;hrt.
<br><br>


<!-- End Content -->

</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
