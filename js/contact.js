$(document).ready(function() {
	// auf click-event reagieren
	$('.btnSubmit').click(function() {

		// prüfen, ob Inhalt vorhanden
		if($('#firstname').val() == "") {
			alert("Bitte geben Sie Ihre Vorname ein!");
			return false;
		}
		else if($('#lastname').val() == "") {
			alert("Bitte geben Sie Ihre Nachname ein!");
			return false;
		}
		else if($('#email').val() == "") {
			alert("Bitte geben Sie Ihre E-Mail Adresse ein!");
			return false;
		}
		else if($('#subject').val() == "") {
			alert("Bitte geben Sie ein Betreff ein!");
			return false;
		}
		else if($('#comment').val() == "") {
			alert("Bitte geben Sie noch Ihre Nachricht ein!");
			return false;
		}
		else {
			// alert("Vorname: " + $("#firstname").val() + ", Nachname: " + $("#lastname").val() + ", Email: " + $("#email").val() + ", Telefonnummer: " + $("#phone").val() + ", Betreff: " + $("#subject").val() + ", Nachricht: " + $("#comment").val());
			// Ausgabe als JSON:
			// alert("{'Vorname': '" + $("#firstname").val() + "', 'Nachname': '" + $("#lastname").val() + "', 'E-Mail': '" + $("#email").val() + "', 'Telefonnummer': '" + $("#phone").val() + "', 'Betreff': '" + $("#subject").val() + "', 'Kommentar': '" + $("#comment").val() + "'} ");
			// alert('Sie sind gerade dabei, Ihre Nachricht zu versenden. Falls Sie es wünschen, können Sie sich eine Kopie der versendeten Daten, mit einem Klick auf "Download JSON", nach dem Versenden herunterladen. Wir melden uns bei Ihnen so bald wie möglich.');
			// alert('Mit einem Klick auf "Download JSON" können Sie sich eine Kopie der versendeten Daten herunterladen. Wir melden uns bei Ihnen so rasch wie möglich.');
			// alert("Vorname: " + $("#firstname").val() + ", Nachname: " + $("#lastname").val() + ", Email: " + $("#email").val() + ", Telefonnummer: " + $("#phone").val() + ", Betreff: " + $("#subject").val() + ", Nachricht: " + $("#comment").val());
			$('.cmxform').submit();
			return false;
		}
	});
});
// E-Mail Adresse vom Spam Bots verstecken
	// var mail="mailto:";
	// var recipient="test";
	// var at = String.fromCharCode(64);
	// var dotcom="example.com";
// window.open(mail+recipient+at+dotcom);
