function initialize(coords) {
	var latlng = new google.maps.LatLng(coords.latitude, coords.longitude);
	var myOptions = {
		zoom: 8,
		// center: latlng, // I'm the belly of the world! ;)
		center: {lat: 47.300, lng: 8.700},
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById("pos"), myOptions);

  var marker = new google.maps.Marker({
			position: latlng,
			map: map,
			icon: 'img/library-1-32.png',
			title: 'Ihre aktuelle Position'
	});

	// var latLng = {lat: 47.376887, lng: 8.541694};

	// var marker = new google.maps.Marker({
  //   position: latLng,
  //   map: map,
  //   title: 'Standort Zürich'
  // });

	// var latLng = {lat: 47.559599, lng: 7.588576};

	// var marker = new google.maps.Marker({
  //   position: latLng,
  //   map: map,
  //   title: 'Standort Basel'
  // });

	// var latLng = {lat: 46.850783, lng: 9.531986};

	// var marker = new google.maps.Marker({
  //   position: latLng,
  //   map: map,
  //   title: 'Standort Chur'
  // });

   var contentStringZ = '<div id="pos">'+
       '<div id="siteNotice">'+
       '</div>'+
       '<h1 id="firstHeading" class="firstHeading">Arlesheim (BL)</h1>'+
       '<div id="bodyContent">'+
       // '<p><img src="img/logo.png"> '+
       '<p><b>BBINW</b> ' +
      // '<p><a href="http://www.somelink.com/home.html" target="_blank"><b>Some Text</b></a> '+
       '<p>Neumattstrasse 8 '+
       '<p>4144 Arlesheim (BL) '+
       '<p>Tel. +41 61 599 2746 '+
      // '<p><b>Nächste Event:</b> MLZ  '+
      // '<p><b>Zeitpunkt:</b> 01. April 2000, 08:15 Uhr, Zimmer: 666  '+
      // '<p>Detailierte Informationen entnehmen Sie bitte unserem '+
      // '<p><a href="http://bbinw.bplaced.net/some.html" target="_blank">Eventplan</a> '+
      // '<br> '+
       '<p><a href="http://fahrplan.sbb.ch/bin/query.exe/dn" target="_blank">SBB-Fahrplan</a>&nbsp;|&nbsp;<a href="https://www.google.ch/maps/dir///@47.3500943,8.7246323,13z" target="_blank">Rutenplan</a> '+
       '</div>'+
       '</div>';

  var infowindow1 = new google.maps.InfoWindow({
    content: contentStringZ,
    maxWidth: 180
  });

  var marker1 = new google.maps.Marker({
    position: {lat: 47.495724, lng: 7.619405},
    map: map,
    icon: 'img/logo-1-32.png',
    title: 'Arlesheim (BL)'
  });

  
  marker1.addListener('click', function() {
    infowindow1.open(map, marker1);
  });

  var contentStringB = '<div id="pos">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">Zeihen (AR)</h1>'+
      '<div id="bodyContent">'+
      '<p><b>BBINW</b> ' +
      '<p>Mühliweiher 9 '+
      '<p>5079 Zeihen (AR) '+
      '<p>Tel. +41 61 599 2746 '+
      '<br> '+
      '<p><a href="http://fahrplan.sbb.ch/bin/query.exe/dn" target="_blank">SBB-Fahrplan</a>&nbsp;|&nbsp;<a href="https://www.google.ch/maps/dir///@47.3500943,8.7246323,13z" target="_blank">Rutenplan</a> '+
      '</div>'+
      '</div>';

  var infowindow2 = new google.maps.InfoWindow({
    content: contentStringB,
    maxWidth: 180
  });

  var marker2 = new google.maps.Marker({
    position: {lat: 47.473399, lng: 8.084027},
    map: map,
    icon: 'img/logo-1-32.png',
    title: 'Zeihen (AR)'
  });

  marker2.addListener('click', function() {
    infowindow2.open(map, marker2);
  });
  
}

	navigator.geolocation.getCurrentPosition(function(position){
		initialize(position.coords);
		}, function(){
			document.getElementById('pos').innerHTML = 'Ihre Position konnte leider nicht ermittelt werden';
		}
	);
