function initialize() {

if (GBrowserIsCompatible()) {
var map = new GMap2(document.getElementById("pos"));

// Ausgangsdaten konfigurieren: Laengen und Breitenangabe des Karten-Mittelpunktes; Zoomfaktor, Kartentyp (G_NORMAL_MAP, G_HYBRID_MAP, G_SATELLITE_MAP)
map.setCenter(new GLatLng(51.223335,6.76981), 13, G_HYBRID_MAP);

function createMarker(point,html) {
var marker = new GMarker(point);

// Tooltip beim Klick auf den Marker anzeigen oder nicht.
GEvent.addListener(marker, 'click', function() {
marker.openInfoWindowHtml(html);
});
return marker;
}

// Standort Anfang
var point = new GLatLng(51.21412,6.77418);
var marker = createMarker(point,'Hier wohne ich')
map.addOverlay(marker);
// Standort Ende

// Standort Anfang
var point = new GLatLng(51.217894,6.762105);
var marker = createMarker(point,'Fernsehturm')
map.addOverlay(marker);
// Standort Ende

// Standort Anfang
var point = new GLatLng(51.227394,6.770754);
var marker = createMarker(point,'Rheintreppen')
map.addOverlay(marker);
// Standort Ende

// Standort Anfang
var point = new GLatLng(51.215056,6.752064);
var marker = createMarker(point,'Medienhafen')
map.addOverlay(marker);
// Standort Ende

// Standort Anfang
var point = new GLatLng(51.226211,6.774702);
var marker = createMarker(point,'Altstadt')
map.addOverlay(marker);
// Standort Ende

// Navigationselemente einblenden
map.addControl(new GLargeMapControl());

// Kartentypen einblenden
map.addControl(new GMapTypeControl());

// Übersichtskarte einblenden
map.addControl(new GOverviewMapControl());

// Massstab einblenden
map.addControl(new GScaleControl());

}
}
