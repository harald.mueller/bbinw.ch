$(document).ready(function() {
	// auf click-event reagieren
	$('.btnSubmit').click(function() {

		// pruefen, ob name (k)einen Inhalt hat
		if($('#name').val() == "") {
			alert("Bitte geben Sie Ihre Name ein!");
			return false;
		}
		else if($('#email').val() == "") {
			alert("Bitte geben Sie Ihre E-Mail Adresse ein!");
			return false;
		}
		else if($('#comment').val() == "") {
			alert("Bitte geben Sie noch Ihr Feedback ein!");
			return false;
		}
		else {
			// alert("Name: " + $("#name").val() + ", Email: " + $("#email").val() + ", Feedback: " + $("#comment").val());
			// window.open('mailto:empfaenger@firmendomaene.ch?subject=Feedback&amp;body=Feedback%20');
			// Ausgabe als JSON:
			// Beispiel Ausgabe als JSON: alert("{'Name': '" + $("#name").val() + "', 'Email': '" + $("#email").val() + "', 'Feedback': '" + $("#comment").val() + "'} ");
			// alert("{'Name': '" + $("#name").val() + "', 'Email': '" + $("#email").val() + "', 'Feedback': '" + $("#comment").val() + "'} ");
			$('.cmxform').submit();
			return false;
		}
	});
});
// E-Mail Adresse vom Spam Bots verstecken
	// var mail="mailto:";
	// var recipient="test";
	// var at = String.fromCharCode(64);
	// var dotcom="example.com";
// window.open(mail+recipient+at+dotcom);
