<?php include 'inc.head.html';?>
	<body>
		<?php include 'inc.menuStickyTop.html';?>
			<div class="container">

				<?php include 'inc.secNavBar.html';?>

				<div class="container">
					<br>
					<h1 class="moveDown40">Lust auf mehr Licht?</h1><h2 class="goneForGood">BBINW baut mit Innovation f&uuml;r die Nordwestschweiz</h2>
					<h3 id="hide_undertitle">Wir beraten Sie gerne</h3>
				</div>

			</div>

				<div class="clearfix"></div>


				<div class="container">
					<div class="gallery-container"><br>
						<div class="thumbs">
							<a class="example-image-link" href="bbinw-bilder/bbinw-beleuchtung/web04.jpg" title="Fira 5: Aussenbeleuchtung" data-lightbox="example-set" data-title="Fira 5: Vereinbaren Sie noch heute einen Besprechungstermin und realisieren Sie Ihr individuelles Beleuchtungskonzept."><img class="example-image" src="bbinw-bilder/bbinw-beleuchtung/thumbs/web04.jpg" alt=""></a>
							<a class="example-image-link" href="bbinw-bilder/bbinw-beleuchtung/web03.jpg" title="Fira 4: Licht schafft Ambiente" data-lightbox="example-set" data-title="Fira 4: Licht schafft Ambiente. Wir freuen uns, Ihnen die sch&ouml;nste Beleuchtung anbieten zu k&ouml;nnen."><img class="example-image" src="bbinw-bilder/bbinw-beleuchtung/thumbs/web03.jpg" alt=""></a>
							<a class="example-image-link" href="bbinw-bilder/bbinw-beleuchtung/web02.jpg" title="Fira 2: Licht-FIRALUX Design" data-lightbox="example-set" data-title="Fira 2: Licht-FIRALUX Design ist ein starker Partner f&uuml;r clevere Beleuchtungsl&ouml;sungen."><img class="example-image" src="bbinw-bilder/bbinw-beleuchtung/thumbs/web02.jpg" alt=""></a>
							<a class="example-image-link" href="bbinw-bilder/bbinw-beleuchtung/web01.jpg" title="Fira 1: Licht-Kunst mit FIRALUX Design" data-lightbox="example-set" data-title="Fira 1: Licht-Kunst mit FIRALUX Design - schafft eine positive Atmosphere."><img class="example-image" src="bbinw-bilder/bbinw-beleuchtung/thumbs/web01.jpg" alt=""></a>
						</div>
					</div>
					<p>Klicken Sie auf die Bilder um sie zu vergrössern und eine Beschreibung anzuzeigen!</p>
					<br><br>
					<!-- <hr><br> -->
					<a id="follow-to-the-next-page" href="./downloads/informationen_beleuchtung.pdf" target="_blank" title="Informationen Beleuchtung">&rArr;&nbsp;Flyer Beleuchtung (pdf)</a>
					<!-- <a id="follow-to-the-next-page" href="javascript:window.history.back();" title="Zur&uuml;ck">Zur&uuml;ck</a> -->

				</div>


					<div class="clearfix"></div>
				<script src="js/lightbox-plus-jquery.min.js"></script>
			<?php include 'inc.footer.html';?>
		<?php include 'inc.menuitemsM.html';?>
	</body>
</html>
