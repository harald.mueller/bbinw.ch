<?php include 'inc.head.html';?>
	<body>
		<?php include 'inc.menuStickyTop.html';?>
			<div class="container">

			<?php include 'inc.secNavBar.html';?>

				<div class="container">
					<br>
					<h1>Impressum</h1><h2 class="goneForGood">BBINW baut mit Innovation f&uuml;r die Nordwestschweiz</h2>
					<h3 id="hide_undertitle">BBINW Nordwestschweiz</h3>
					<br>
				</div>

				<nav>
					<div class="far-left">
						<section>
							<h2></h2>
							<br>
							<p></p>
						</section>
					</div>
				</nav>

				<div class="content">

					<div class="content-left"><!-- class="circular" -->                                            <!-- class="circular-image" -->
						<div><img src="img/susanne_bieli1.jpg" title="" alt="Susanne Bieli - Gesch&auml;ftsf&uuml;hrerin" ></div>
						<br><br>
							<section>
								<h2>Name und Anschrift <br>des Betreibers:</h2>
								<br>
								<p><b>BBINW</b><br>Neumattstrasse 8<br>4144 Arlesheim<br><br><a href="tel:+41615992746">061 599 27 46 phone</a>
								<br>061 599 47 45 fax<br><a href="tel:+41764131936">076 413 1936 mobile</a>
								<br><a href="tel:+41764181938">076 418 1938 mobile</a><br>
								<br><b>Gesch&auml;ftsf&uuml;hrerin</b><br>Frau Susanne Bieli</p><br>
								<p><strong>Unternehmens-Identifikationsnummer:</strong><br>
UID Nr.: CHE-115.316.381<br>
<br>
								  <br><i><strong>E-Mail Adresse des Betreibers:</strong></i><br>
								  <br><b>E-Mail:</b> <a class="link" href="mailto:immobilien@bauland-nw.ch">immobilien@bauland-nw.ch</a><br>
								  <br><a href="skype:bauland-nw?call" title=""><b>Skype-Name: </b>bauland-nw</a><br><br>
								  <a class="link" href="mailto:webmaster@bauland-nw.ch">webmaster@bauland-nw.ch</a><br>
							  </p>
							  <p>&nbsp;</p>
								<p><br>
							  </p>
								<p><strong>Urheberrechtshinweis:</strong><br><br>
								Alle Inhalte dieses Internetangebotes, insbesondere Texte,
								Fotografien und Grafiken, sind urheberrechtlich gesch&uuml;tzt
								(&copy; Copyright BBINW Arlesheim 2009-2017). Das Urheberrecht liegt,
								soweit nicht ausdr&uuml;cklich anders gekennzeichnet, bei
								BBINW Arlesheim. Bitte fragen Sie BBINW Arlesheim, falls Sie
								die Inhalte dieses Internetangebotes verwenden m&ouml;chten.
								<br><br>
								Wer gegen das Urheberrecht verst&ouml;&szlig;t (z.B. die Inhalte
								unerlaubt auf die eigene Homepage kopiert), macht sich gem&auml;ss
								Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt
								und muss Schadensersatz leisten. Kopien von Inhalten k&ouml;nnen
								im Internet ohne gro&szlig;en Aufwand verfolgt werden.
								<br><br>
								<p><strong>Datenschutzerkl&auml;rung:</strong><br><br>
								Cookies
								<br><br>
								Beim Besuch von Webseiten des Anbieters erhebt, speichert und
								verwendet der Anbieter sowohl von registrierten als auch von nicht
								registrierten Nutzern allgemeine, nicht personenbezogene Daten.
								Dazu werden u.a. sogenannte „Cookies“ eingesetzt. Dabei handelt es sich
								um Textdateien, die auf dem Benutzer-System des Anbieters sowie allf&auml;lligen
								Dritten gespeichert werden und die eine Analyse der Benutzung der Produkte ermöglicht.
								Diese Technologie erlaubt es, einzelne Nutzer als Besucher zu erkennen und ihnen
								individualisierte Dienstleistungen und Produkte zu unterbreiten.
								Solche Trackingdaten werden getrennt von allf&auml;llig erhobenen Personendaten gespeichert.
								Die Erhebung erfolgt ausschliesslich in anonymer Form, womit zu keinem Zeitpunkt ein R&uuml;ckschluss
								auf den einzelnen Nutzer möglich ist. Es wird einzig eine Verbindung zur bekannten
								IP-Adresse ermöglicht ohne dahinter den jeweiligen Nutzer persönlich identifizieren zu können.
								Nutzer von Webseiten des Anbieters können das Speichern von Cookies verhindern, indem sie
								in ihren Browser- Einstellungen "keine Cookies akzeptieren" w&auml;hlen.
								Dies kann jedoch die Funktionalit&auml;t der Produkte behindern und eine optimierte Nutzung der Online-Dienste f&uuml;r den Nutzer erschweren.
								<br><br>
								Google Analytics
								<br><br>
								Dieses Angebot nutzt ebenfalls den Webanalysedienst Google Analytics, ein Programm der Google Inc.
								(„Google, USA“). Die durch das Tracking erfassten Informationen zu Ihrer Nutzung
								dieser Website werden auf einem Server von Google in den USA gespeichert.
								Durch eine sogenannte IP-Anonymisierung wird Ihre IP-Adresse von Google innerhalb
								 von Mitgliedstaaten der Europ&auml;ischen Union oder in anderen Vertragsstaaten des
								 Abkommens &uuml;ber den Europ&auml;ischen Wirtschaftsraum zuvor gek&uuml;rzt. Somit ist der
								 Standort Ihres Browsers lediglich regional zuortbar, nicht aber ihrer Person.
								 Google kann Besucherverhalten auswerten, um Reports &uuml;ber die Websiteaktivit&auml;ten
								 zusammenzustellen. Auch weitere mit der Websitenutzung und der Internetnutzung
								 verbundene Dienstleistungen können so gegen&uuml;ber dem Websitebetreiber erbracht werden.
								<br><br>
								BBINW Arlesheim 29. Mai 2018<br>
								<br>
								</p>
							</section>
							<!-- <div class="circular"><img src="img/nadine-gordimer.jpg" title="nadine gordimer" alt="nadine gordimer" class="circular-image"></div>
							<br>
								<p>Wie jemand aussieht ist einem guten Foto egal.</p> -->
					</div>

					<div class="content-right">
						<article>
							<h2></h2>
							<br>
							<p></p>
						</article>
					</div>

				</div>

			</div>

				<div class="clearfix"></div>

				<aside></aside>
			<?php include 'inc.footer.html';?>
		<?php include 'inc.menuitemsM.html';?>
	</body>
</html>
