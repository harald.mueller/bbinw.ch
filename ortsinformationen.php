<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>
<br>
<br>
<h1>Ortsinformationen</h1>

<p>Wir haben f&uuml;r Sie einige Informationen zu interessanten Orten aus unserem Immobilienangebote zusammengestellt</p><br>

<span class="normal">
<div style="float: left; margin: 10px;">
	<h3>Region Basel</h3>
	    <a class="link" href="http://www.basel.com" target="_blank">Informationen &uuml;ber Basel (BS)</a>
	<br><a class="link" href="pdf-files/informationen_basel_feldberg.pdf" target="_blank">Informationen &uuml;ber die Feldbergstrasse in Basel (BS)</a>
	<br><a class="link" href="pdf-files/OrtsinformationenBaselWebergasse.pdf" target="_blank">Informationen &uuml;ber die Webergasse in Basel (BS)</a>
	<br><a class="link" href="pdf-files/informationen_riehen.pdf" target="_blank">Informationen &uuml;ber Riehen (BS)</a>
	<br><a class="link" href="pdf-files/informationen_arisdorf.pdf" target="_blank">Informationen &uuml;ber Arisdorf (BL)</a>
	<br><a class="link" href="pdf-files/informationen_binningen.pdf" target="_blank">Informationen &uuml;ber Binningen (BL)</a>
	<br><a class="link" href="pdf-files/informationen_blauen.pdf" target="_blank">Informationen &uuml;ber Blauen (BL)</a>
	<br><a class="link" href="pdf-files/informationen_grellingen.pdf" target="_blank">Informationen &uuml;ber Grellingen (BL)</a>
	<br><a class="link" href="pdf-files/informationen_liesberg.pdf" target="_blank">Informationen &uuml;ber Liesberg (BL)</a>
	<br><a class="link" href="pdf-files/informationen_waldenburg.pdf" target="_blank">Informationen &uuml;ber Waldenburg (BL)</a>
	<br>
	<br>
</div>
<div style="float: left; margin: 10px;">
	<h3>Region Solothurn</h3>
	    <a class="link" href="pdf-files/informationen_breitenbach2.pdf" target="_blank">Informationen &uuml;ber Breitenbach (SO) von Schwarzbubenland Tourismus</a>
	<br><a class="link" href="pdf-files/informationen_buesserach2.pdf" target="_blank">Informationen &uuml;ber B&uuml;sserach (SO) von Schwarzbubenland Tourismus</a>
	<br><a class="link" href="pdf-files/informationen_buesserach.pdf" target="_blank">Informationen &uuml;ber B&uuml;sserach (SO)</a>
	<br><a class="link" href="pdf-files/informationen_gempen.pdf" target="_blank">Informationen &uuml;ber Gempen (SO)</a>
	<br><a class="link" href="pdf-files/informationen_nunningen.pdf" target="_blank">Informationen &uuml;ber Nunningen (SO)</a>
	<br>
	<br>
</div>
<div style="float: left; margin: 10px;">
	<h3>Region Aargau</h3>
	    <a class="link" href="pdf-files/informationen_effingen.pdf" target="_blank">Informationen &uuml;ber Effingen (AG)</a>
	<br><a class="link" href="pdf-files/informationen_frick.pdf" target="_blank">Informationen &uuml;ber Frick (AG)</a>
	<br><a class="link" href="pdf-files/informationen_hornussen.pdf" target="_blank">Informationen &uuml;ber Hornussen (AG)</a>
	<br><a class="link" href="pdf-files/informationen_moehlin.pdf" target="_blank">Informationen &uuml;ber M&ouml;hlin (AG)</a>
	<br><a class="link" href="pdf-files/informationen_olsberg.pdf" target="_blank">Informationen &uuml;ber Olsberg (AG)</a>
	<br><a class="link" href="pdf-files/informationen_schupfart.pdf" target="_blank">Informationen &uuml;ber Schupfart (AG)</a>
	<br><a class="link" href="pdf-files/informationen_ueken.pdf" target="_blank">Informationen &uuml;ber Ueken (AG)</a>
	<br><a class="link" href="pdf-files/informationen_woelflinswil.pdf" target="_blank">Informationen &uuml;ber W&ouml;lflinswil (AG)</a>
	<br><a class="link" href="pdf-files/informationen_zeiningen.pdf" target="_blank">Informationen &uuml;ber Zeiningen (AG)</a>
	<br>
	<br>
</div>
<div style="float: left; margin: 10px;">
	<h3>Region Luzern</h3>
	    <a class="link" href="pdf-files/informationen_rickenbach.pdf" target="_blank">Informationen &uuml;ber Rickenbach (LU)</a>
	<br>
   	<br>
</div>
<br>
<div class="clearfix"></div>
</span>
</p>
<div class="clearfix"></div>

</div>
<br>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
