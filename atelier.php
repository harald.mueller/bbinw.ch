<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2 >Atelier Bieli</h2>

<p>
Wir freuen uns, einige langj&auml;hrige Kunden von Damen-Mode Rosy T&auml;schler in Oberwil weiterhin bedienen zu d&uuml;rfen. 
</p><br>
<strong>Eigenes Atelier vorhanden </strong><br><br>
<img width="308px" src="images/kleider.jpg" ></a><br><br><br>
Folgende Arbeiten k&ouml;nnen wir Ihnen abnehmen:
<br><br>
-   Absteckarbeiten bei Ihnen zu Hause, kleine &Auml;nderungen. Reparaturen
<br><br>
-   Fachgerechte Kleiderreinigung
<br><br>
-   Anfertigung von Inneneinrichtungsgegenst&auml;nden (Beispiel spezielle
    Tagesdecken, Zierkissen etc.)
<br><br>
-   Entw&uuml;rfe von passendem Kleidungsst&uuml;ck ohne Anfertigung
    (Referenzen vorhanden)
<br><br>
-   Begleitung beim Kleidereinkauf mit entsprechender fachgerechter
    Beratung
<br><br>
<br>

<?php include 'inc.preisliste.html';?>
<br><br>

<a class="link" href="http://www.elbeo.de/" target="_blank"><b>Hochwertige Strumpfwaren von ELBEO f&uuml;r den 
Business-Bereich und die gehobenen Anspr&uuml;che
in einem guten Preis-Leistungsverh&auml;ltnis (Mindest-
Abnahme). 
 </b></a>
<br><br>
<a class="link" href="http://www.maxschindler.ch/" target="_blank"><b>Vertrieb von Designer- und englischen Stoffen.
F&uuml;r die Bekleidung.</b><br><b><i>Max Schindler, L&uuml;thi</i></b></a><br><br>
<img src="images/2014/stoffe.jpg" width="80%"><br><br>
<a class="link" href="http://www.fischbacher.ch/" target="_blank"><b>Stoffe f&uuml;r den Inneneinrichtungsbereich</b>
<br><b><i>von Christian Fischbacher</i></b></a><br><br>
<img src="images/2014/kissen.jpg" width="80%">
<br><br>
Leder, Kunststoffe, Stoffe etc. von Winter 
<br><br>
<img src="images/2014/leder.jpg" width="80%">
<br><br><br>


Bettinhalte von <a class="link" href="http://www.albis.ch/" target="_blank">Albis</a> und <a href="http://www.balette.ch/" class="link" target="_blank">Balette</a> <br><br>
<!-- <table class="hometabelle">
<tr><td>
Dauneninhalt    
</td><td>
Wildseide
</td>
</tr>
<tr>
<td><img src="images/2014/dauneninhalt.jpg" width="308"></td>
<td><img src="images/2014/wildseide.jpg"  width="308"></td>
</tr>
</table> -->

<figure>
	<!-- <div id="image1"></div> -->
	<a href="#"><img src="images/2014/dauneninhalt.jpg" alt="Dauneninhalt" ></a>
	<figcaption style="text-align:left;"><strong>Dauneninhalt</strong></figcaption>
</figure>
<br>
<figure>
	<!-- <div id="image1"></div> -->
	<a href="#"><img src="images/2014/wildseide.jpg" alt="Wildseide" ></a>
	<figcaption style="text-align:left;"><strong>Wildseide</strong></figcaption>
</figure>

<br><br>

<h5>Gerne beraten wir Sie in unserem B&uuml;ro in Arlesheim, zeigen Ihnen unsere Stoff- Leder- und Kunststoffkollektionen sowie m&ouml;gliche Bettinhalte etc.  </h5>


<br><br>
<b>Bitte <a href="bbinw-kontakt.php">kontaktieren</a> Sie uns!</b>
<br><br>
<img width="450" src="./images/visit_atelier.jpg">
<br><br><br>
<b>Wer alleine arbeitet addiert - wer zusammen arbeitet multipliziert</b>

<!-- End Content -->

</td>
</tr>

</table>
</div>
<br>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
