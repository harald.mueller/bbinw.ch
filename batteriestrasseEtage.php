<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2>Eigentumswohnung in Bottmingen (BL)</h2>

<p>
Sch&ouml;ne grosse und helle 5.5 Zimmer Eigentumswohnung im Obergeschoss mit Aussicht Richtung Oberwil und Basel, Wintergarten und Lift, Elternzimmer 19,2 qm, 1 Zimmer &agrave; 12,2 und 1 Zimmer &agrave; 10,0 qm. Wohnzimmer 49,5 qm, K&uuml;che 13,2 qm, Entr&eacute;e 22 qm, weiteres Zimmer &agrave; 10,7 qm, 1 Reduit, Balkon 16,7 qm, Wintergarten 15 qm, Waschk&uuml;che 5,4 und Keller 6,6 qm, W&auml;rmepumpe mit Erdsonde. Ein Besucherparkplatz und ein Parkplatz in der Tiefgarage sind vorhanden. Lage nahe beim Bruderholzspital. Diese Etagenwohnung wurde f&uuml;r CHF 1.25 Mio. verkauft.
</p>
<br><br>
<img width="450" src="./images/bottmingenE1.jpg" ><br><br>
<img width="450" src="./images/bottmingenE2.jpg" ><br><br>
<img width="450" src="./images/bottmingenE3.jpg" ><br><br>
<img width="450" src="./images/bottmingenE4.jpg" ><br><br>
<a href="referenzen.php"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
