<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>
<div class="clearfix"></div>
<!--Start Content -->
<br><br>
<div>
<h1>Immobilienangebote</h1>
<p>
<br><br><a class="link" href="./immobilien.php"><b>&rArr;&nbsp;Kaufobjekte</b></a>
<br><br><a class="link" href="./vermietung.php"><b>&rArr;&nbsp;Mietobjekte</b></a>
<br><br><a class="link" href="./aktuelles.php"><b>&rArr;&nbsp;Projekte in Planung</b></a>
<br><br><a class="link" href="./investment.php"><b>&rArr;&nbsp;Investorenobjekte</b></a>
<br><br><a class="link" href="./ortsinformationen.php"><b>&rArr;&nbsp;Ortsinformationen</b></a>
<br><br><a class="link" href="./glossar.php"><b>&rArr;&nbsp;Immobilienglossar</b></a>
<br><br>
</p>
<p><a href="ortsinformationen.php">Ortsinformationen</a>&nbsp;&nbsp;&nbsp;<a href="referenzen.php">Referenzen</a></p>

<br><br><a href="downloads/Stellenangebot_Immobilien-Verwaltung-Immobilientreuhand.pdf"><img src="icon/pdf.png" > Stellenangebot f&uuml;r Immobilien-Verwaltung, Immobilien-Treuhand</a>
<br>
</div>
<!--End Content -->
</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
