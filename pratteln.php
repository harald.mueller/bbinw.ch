<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h2>Eigentumswohnung in Pratteln (BL)</h2>

<p>
Gut eingeteilte Gartenwohnung mit der M&ouml;glichkeit einen Chemin&eacute;eofen einzubauen, und einem gut isolierten Lift. Die Wohnung verf&uuml;gt &uuml;ber keramische Bodenbel&auml;ge und Parkettb&ouml;den, sowie eine hochwertige Einbauk&uuml;che mit Granitabdeckung von Poggenphohl. Diese attraktive Eigentumswohnung, in Pratteln (BL) wurde verkauft f&uuml;r 820.000.-- CHF.
</p>
<br><br>
<div class="compressContainer">
<img width="450" src="./images/pratteln1.jpg" ><br><br>
<img width="450" src="./images/pratteln2.jpg" ><br><br>
<img width="450" src="./images/pratteln3.jpg" ><br><br>
</div>
<a href="referenzen.php"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
