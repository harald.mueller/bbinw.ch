<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<div>
<h2>Unsere Leistungen<br>Ihr Gewinn</h2>

<p>
<a class="link" href="./verwaltung.php"><b>Immobilienverwaltung<br>
(&rArr; Weitere Informationen finden Sie hier...)</b></a>
<br><br>
<b>Individuelle Beratung rund um Liegenschaften</b><br>
&bull; Standortevaluation und Bauplanung<br>
&bull; Baumanagement (Architektur, Bauhandwerk, Bewilligungen, etc.)<br>
&bull; Finanzdienstleistungen<br>
&bull; Erstellen von Verkehrswertsch&auml;tzungen und Expertisen<br>
&bull; <a href="treuhand.php"><b>Treuhandmandate</b> zu Liegenschaftsverk&auml;ufen und Vermietungen<br>
<b><i>&nbsp;&nbsp;&nbsp;(&rArr; Weitere Informationen finden Sie hier...)</i></b>
<br><br></a>
</p>
<p>
<b>Imageberatung - Raumgestaltung</b><br>
&bull; Inneneinrichtung, Antike M&ouml;bel, Kunst am Bau<br>
&bull; Auserlesene M&ouml;bel f&uuml;r B&uuml;ro-, Restaurant-, und Wohnbereich<br>
&bull; Beleuchtungsplanung und Installation<br><br>
</p>
<p>
<b>Konditionen f&uuml;r Makler- und Beratungst&auml;tigkeit</b><br>
&bull; Beratung zu CHF 160.--/Std., erste Stunde wird nicht verrechnet<br>
&bull; Besondere Aufwendungen werden separat verrechnet<br>
&bull; Kilometerspesen CHF 1.--/km<br>
&bull; Maklerprovision gem&auml;ss SVIT:<br>
&nbsp;&nbsp;&nbsp;&bull; 3 % des Verkaufspreises bei Liegenschaften<br>
&nbsp;&nbsp;&nbsp;&bull; 5 % des Verkaufspreises bei Bauland<br>
&nbsp; oder nach Stundenaufwand<br>
&bull; bei Spezialobjekten ist ein Pauschalpreis m&ouml;glich<br>
&bull; Individuelles Werbebudget nur auf Vorauszahlung des Auftraggebers<br>
<br>
</p>
<p>
<b>Administrative Aufgaben - B&uuml;rodienstleistungen</b><br>
&bull; Sekretariat pro Stunde CHF 95.-- zuz&uuml;glich MwSt.<br>
&bull; Flyers erstellen inkl. einf&uuml;gen der Fotos mit Hilfe des prov. Photoshops<br>
&bull; Stellensuche (Schreiben Ihrer Lebensl&auml;ufe und Bewerbungsschreiben)<br>
&bull; Inseratencampagnen in Print- und Internetmedien<br>
&bull; Inseratenkosten werden separat verrechnet<br>
&bull; Zahlungs- und Buchhaltungsauftr&auml;ge<br>
&bull; Debitorenberater-Dienstleistungen (Delcreding)<br>
&bull; Stundenerfassungen<br>
&bull; Bauabrechnungen<br>
<br>
</p>
<p>
<b>Gesch&auml;ftsadresse - Gesch&auml;ftsdomizil</b><br>
&bull; Anschrift aussen<br>
&bull; Postweiterleitung<br>
&bull; Telefonnummer und Faxservice<br>
&bull; Monatspauschale CHF 350.--<br>
<br>
</p>
<p>
<b>Telefonservice</b><br>
&bull; werktags von 08.00 bis 11.30 und 13.00 bis 17.00 Uhr<br>
&bull; Montasabo  CHF 350.--<br>
<br>
</p>
<p>
<b>Telefonservice - Abo light</b><br>
&bull; werktags von 08.00 bis 11.30 und 13.00 bis 17.00 Uhr<br>
&bull; Grundpauschale 1. Woche CHF  50.--<br>
&bull; jede weitere Woche CHF 25.--, sowie zus&auml;tzlich pro Anruf CHF 1.50<br>
<br>
</p>
</div>


<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
