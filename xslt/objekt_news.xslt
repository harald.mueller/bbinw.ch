<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="NonHtml fn xdt xs xsi" xmlns:NonHtml="http://www.progress.com/StylusStudio/NonHtml">
	<xsl:output version="1.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
	<xsl:param name="SV_OutputFormat" select="'HTML'"/>
	<xsl:variable name="XML1" select="/"/>
	<xsl:template match="/">
			<xsl:for-each select="$XML1">
				<xsl:for-each select="Immos">
					<xsl:for-each select="Immo">
						<xsl:if test="Allgemeines/@Status = '2'">
						<xsl:if test= "@Referenznummer = $objektnummer ">
							
								<table width="700" border="0">
										<tr>
											<td width="450">
												
													<font face="Arial">
														<b>	
																<xsl:value-of select="Adresse/@Postleitzahl"/>&#160;
																<xsl:value-of select="Adresse/@Ort"/>&#160;
																<xsl:value-of select="Adresse/@Land"/>&#160;
														</b><p></p>	
													</font>
													<font face="Arial">
														
															<xsl:value-of select="@Titel"/>
														
													</font>
												<br></br>
												<br></br>
												<a href='objekt_test.php5?objektnummer={$objektnummer}'>weitere Informationen finden Sie hier</a>
													
												
											</td>
											<td>
							<xsl:for-each select="Multimedia">
								<xsl:for-each select="Bild">
								<xsl:if test="@Nr = '1'">
											<xsl:for-each select="@Url">
												
													<font face="Arial" size="1">
															<xsl:value-of select="../@Beschriftung"/>
													</font>
												
												
													<img ondblclick="height=180" onclick="window.open('objekt_test.php5?objektnummer={$objektnummer}', '_blank')" width="180" hspace="10" vspace="10" border="1"><xsl:attribute name="alt"/><xsl:attribute name="src">
												<xsl:if test="substring(string(.), 2, 1) = ':'">
													<xsl:text>file:///</xsl:text>
												</xsl:if>
												<xsl:value-of select="translate(string(.), '\', '/')"/>
											</xsl:attribute></img>
												
											</xsl:for-each>
									
								</xsl:if>
								</xsl:for-each>
							</xsl:for-each>
							
							</td>
							</tr>
							</table>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:for-each>
		<!--
		</body>
	</html>
	-->
</xsl:template>
<xsl:template name="NewTemplate0"/>
</xsl:stylesheet>
