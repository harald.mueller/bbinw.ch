<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="NonHtml fn xdt xs xsi" xmlns:NonHtml="http://www.progress.com/StylusStudio/NonHtml">
        <xsl:output version="1.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
        <xsl:param name="SV_OutputFormat" select="'HTML'"/>
        <xsl:variable name="XML1" select="/"/>
        <xsl:template match="/">

                        <xsl:for-each select="$XML1">
                                <xsl:for-each select="Immos">
                                        <xsl:for-each select="Immo">
                                                <xsl:if test="Allgemeines/@Status = '2'">
                                                <xsl:if test= "@Referenznummer = $objektnummer ">
                                                        <p>
                                                                <table width="600" border="0" align="center">
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td width="600">

                                                                                            <font face="Arial">
                                                                				   <h2><div align="center">
                                                                                                        <xsl:value-of select="Adresse/@Postleitzahl"/>&#160;
                                                                                                        <xsl:value-of select="Adresse/@Ort"/>&#160;
                                                                                                        <xsl:value-of select="Adresse/@Land"/>
                                                                                            	</div></h2>
                                                                                            </font>

                                                                                            <font face="Arial" style="font-size:95%" >
                                                                                            <b><div align="center" style="margin:2em">
                                                                                                        <xsl:value-of select="@Titel"/>
                                                                                                 </div></b>
                                                                                            </font>

                                                                                                 <div align="center" style="margin:2em">
                                                                					       <img height="300" hspace="10" vspace="10" border="1" src="{Multimedia/Bild/@Url}"/>
                                                                                                 </div>

                                                                                            <font face="Arial">
                                                                                            	<div align="justify">
                                                                                                 <xsl:value-of select="Beschreibungen/Objekt"/>
                                                                                                 </div>
                                                                                            </font>

                                                                                        </td>
                                                                                </tr>
                                                                        </tbody>
                                                                </table>
                                                        </p>

                                                        <br></br>


                                                        <div align="center">
                                                                <table width="600" bgColor="#b3b3b3" border="0" align="center">
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Referenznummer</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="@Referenznummer"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Baujahr</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Baujahr"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                		<td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Nettowohnfl&#228;che (m&#178;)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Nettowohnflaeche"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Letzte Renovation</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Renovation"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>


                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Bruttowohnfl&#228;che (m&#178;)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Bruttowohnflaeche"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Zustand</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Zustand"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Grundst&#252;cksfl&#228;che (m&#178;)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Grundstuecksflaeche"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Frei</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@FreiAbText"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                </tr>

                                                                                                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Raumh&#246;he (m)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Raumhoehe"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">

                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">

                                                                                                </p>
                                                                                        </td>

                                                                                </tr>

                                                                        		<tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Kubatur (m&#179;)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Kubatur"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">

                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">

                                                                                                </p>
                                                                                        </td>

                                                                                </tr>

                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Zimmer</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlZimmer"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Kaufpreis</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Kauf/@Kaufpreis"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                </tr>

                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl B&#228;der</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlBaeder"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Miete Brutto</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Miete/@Bruttomiete"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                        <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl G&#228;ste-WC</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlGaesteWC"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Miete Netto</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Miete/@Nettomiete"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Parkpl&#228;tze</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlParkplaetze"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>

                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Kaution/Depot</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Miete/@Kaution"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>

                                                                        </tbody>
                                                                </table>
                                                                </div>
                                                </xsl:if>
                                                </xsl:if>
                                        </xsl:for-each>
                                </xsl:for-each>
                        </xsl:for-each>
</xsl:template>
<xsl:template name="NewTemplate0"/>
</xsl:stylesheet>