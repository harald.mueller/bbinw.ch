<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!-- Start Content -->

<br><br>
<h4>Angebote  im  Bereich  Administrations- und  Haushaltshilfe</h4>

<p>
Fachgerechte B&uuml;gelhilfe und Kleiderreinigung
<br><br>
Fachgerechte Flick- und &Auml;nderungsarbeiten (eigenes Atelier vorhanden)
<br><br>
Einkaufen und Kochen nach Ihren W&uuml;nschen
<br><br>
Gartenarbeiten (Schneiden von Rosen, Unkraut herausstechen etc.)
<br><br>
Fachgerechtes und sorgf&auml;ltiges Reinigen (nachhaltig, keine Sch&auml;den beim Wiederverkauf)
<br><br>
Hausbewachung und Verwaltung Ihrer Liegenschaft
<br><!-- <br>
Renovationen (neue Bodenbel&auml;ge, neue K&uuml;che, neues Badezimmer etc.)
<br> --><br>
Elektronischer Zahlungsverkehr, Schreiben von Briefen, Flyer erstellen etc.
<br><br>
Telefonservice
<br><br>


<b>Wir freuen uns auf Ihre Kontaktaufnahme.</b>
<br><br>
Administrationshilfe: administrationshilfe@bbinw.ch, 061 599 27 46
<br>
Haushaltshilfe: haushaltshilfe@bbinw.ch
<!-- <br><br>
oder auf Tel.: +41 (061) 599 27 46 -->
</p>
<br>

<!-- End Content -->

</td>
</tr>
<tr>

</tr>
</table>
<br>
<a id="follow-to-the-next-page" href="javascript:window.history.back();" title="Zur&uuml;ck">(&rArr; zur&uuml;ck)</a>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
