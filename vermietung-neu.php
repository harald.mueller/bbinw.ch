<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>
<style>
table {
	float: left;
	margin: 2em 2em 1em 0;
	width: 300px;
}
</style>
		<br>
		<br>
		<div><h1>Mietobjekte</h1></div>

<div class="iframe">
	
<div>
  <h2>  Öffentliche   Angebote!</h2></div>
	
	<div class="iframeGF">
		<b>MIET-OBJEKTE</b>

	<div class="iframecode">
<script src="https://casaframe.ch/js/responsive-iframe.js" type="text/javascript"></script>
<iframe src="https://casaframe.ch/de/publisher/nhaXjYbMxCZtCz7fANatEkCBNQzrsPgU/?segment=rent" frameborder="0" width="100%" style="max-width: 100%; overflow: hidden;" id="casaframe" scrolling="no"></iframe>
	</div>
	</div>	
<hr>
	
</div>

	
<div>
<h2>Unsere BBINW internen  Miet-Angebote!</h2>
<p>Sie suchen eine Wohnung, ein Haus, ein B&#252;ro zum mieten ? BBINW bietet Ihnen eine reichhaltige Auswahl an attraktiven Immobilien.</p>
<p><a href="ortsinformationen.php">Ortsinformationen</a>&nbsp;&nbsp;&nbsp;<a href="referenzen.php">Referenzen</a></p><br><br></div>

<div class="inhalt-objekte">
      <div class="inhalt-left fluid-gf-3">
		 	 <div class="objekt-gf">
				<div class="objekt-gf-nummer"><b>BIE</b></div>
				<div class="objekt-gf-titel"><b>5079 Zeihen, Schweiz </b></div>
		  		<div class="objekt-gf-inhalt">2 m&ouml;blierte R&auml;ume mit kleiner K&uuml;che, vollst&auml;ndig m&ouml;bliert und eingerichtet, ruhige Lage per sofort zu vermieten
				M&uuml;hliweiher 9, 5079 Zeihen (AG, Fricktal) </div>
				 <div class="objekt-gf-photo"><a href="downloads/2016_12_12_voll_eingerichtete_Raeume_Zeihen.pdf" target="_blank"><img src="images/2raeume_muehliweg9_zeihen_klein.jpg" style="height:200px;"></a><a href="downloads/2017_09_02_NeuesteVerkaufsobjekte.pdf" 
						target="_blank"></a><a href="downloads/BIE170518_25ZiMietwohnung.pdf" target="_blank"></a></div>
				 <div class="objekt-gf-dokumente"><a href="downloads/2016_12_12_voll_eingerichtete_Raeume_Zeihen.pdf" target="_blank">Dokumentation</a><br>
				   <a href="sendmailinteresse.php?objektnummer=BIE%202RaeumeZiehen">Bin intessiert</a> &nbsp; <a href="sendmail.php?objektnummer=BIE%202RaeumeZiehen">Link senden</a><br>
				 </div>
	   		 </div>

      </div>
      <div class="inhalt-mitte fluid-gf-3">
	  		<div class="objekt-gf">
			  <div class="objekt-gf-nummer"><b>BIE 700012 </b></div>
			  <div class="objekt-gf-titel"><b>4414 Arlesheim, Schweiz </b></div>
	  		  <div class="objekt-gf-inhalt">Angrenzende B&uuml;ror&auml;ume per sofort zu vermieten
			  Neumattstrasse 8, 4144 Arlesheim</div>
	 		  <div class="objekt-gf-photo"><a href="downloads/BIE_700012_26_Oktober_Flyer-und_Doku-Vorlage.pdf" target="_blank"><img src="images/arlesheim_neumattstrasse_8_klein.jpg" style="height:200px;"></a><a href="downloads/2017_09_02_NeuesteVerkaufsobjekte.pdf" 
						target="_blank"></a><a href="downloads/2017_11_07_35Zi_u_45Zi_Luesselweg_4226_Breitenbach.pdf" target="_blank"></a></div>
				 <div class="objekt-gf-dokumente"><a href="downloads/BIE_700012_26_Oktober_Flyer-und_Doku-Vorlage.pdf" target="_blank">Dokumentation</a> <br>
				   <a href="sendmailinteresse.php?objektnummer=BIE%20700012">Bin intessiert</a> &nbsp; <a href="sendmail.php?objektnummer=BIE%20700012">Link senden</a><br>
			  </div>
			</div>
      </div>
	
    <div class="inhalt-rechts fluid-gf-3">
	  <div class="objekt-gf">
				<div class="objekt-gf-nummer"><b>BIE 50 000 </b></div>
			  <div class="objekt-gf-titel"><b>4414 Arlesheim, Schweiz </b></div>
	  		  <div class="objekt-gf-inhalt">Mietwohnungen am Gartenweg 3, 4144 Arlesheim <br>
        </div>
				<div class="objekt-gf-photo"><a href="downloads/2017_08_17_MietwohnungenAmGartenweg3Arlesheim.pdf" target="_blank"><img src="images/arlesheim_gartenstrasse_3_klein.jpg" style="height:200px;"></a><a href="downloads/2017_09_02_NeuesteVerkaufsobjekte.pdf" 
						target="_blank"></a></div>
			
				<div class="objekt-gf-dokumente"><a href="downloads/2017_08_17_MietwohnungenAmGartenweg3Arlesheim.pdf" target="_blank">Dokumentation</a> <br>
				  <a href="sendmailinteresse.php?objektnummer=BIE%2050%20000">Bin intessiert</a> &nbsp; <a href="sendmail.php?objektnummer=BIE%2050%20000">Link senden</a><br>
	    </div>
			
	    </div>
	  </div>

	
	</div>
<div class="clearfix"><br>
		<br>
  </div>
<h3>Suchen auch Sie ein Mietobjekt?</h3>
		<br> <img width="308px" src="./images/vermietung.jpg"><br>
		<br>
		<p>Gerne suchen wir Ihre Wunschwohnung. Bitte geben Sie uns per Mail
			(<a href="mailto:immobilien@bauland-nw.ch">immobilien@bauland-nw.ch</a>) Ihre Suchw&uuml;nsche bekannt. Preis ab,
			Preis bis, Lage, Anzahl Zimmer, Gr&ouml;sse der Wohnung,
			Ausbaustandart, Anzahl G&auml;ste-WC und Badezimmer, Best&auml;tigung
			f&uuml;r den Suchauftrag gegen entsprechende
			Aufwandsentsch&auml;digung gem. SVIT.</p>

		<span class="normal"> <br>
		<br>
		<a class="link"
			href="./downloads/Leistungsbeschrieb_Verwaltung_Bewirtschaftung.pdf"
			target="_blank">Leistungsbeschreibung: Verwaltung und Bewirtschaftung
				von Mietliegenschaften (PDF)</a> <br>
		<br>
		<a class="link" href="./downloads/erstvermietung_20080709.pdf"
			target="_blank">Leistungsbeschreibung: Erstvermietung (PDF)</a>

		</span>
		</p>
		<!--End Content -->

		</td>
		</tr>
		<tr>

		</tr>
		</table>
</div>
	<br>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
