<?php include 'inc.head.html';?>
	<body>
		<?php include 'inc.menuStickyTop.html';?>
			<div class="container">

				<?php include 'inc.secNavBar.html';?>

				<div class="container">
					<br>
					<h1 class="moveDown40">Sch&ouml;ne und funktionale M&ouml;bel</h1>
					<h2 class="goneForGood">BBINW baut mit Innovation f&uuml;r die Nordwestschweiz</h2>
					<h3 id="hide_undertitle">&Uuml;berzeugen Sie sich selbst</h3>
				</div>

			</div>

				<div class="clearfix"></div>


				<div class="container">
					<div class="gallery-container"><br>
						<div class="thumbs">
							<a class="example-image-link" href="images/moebel/web01.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web01.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web02.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web02.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web03.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web03.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web04.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web04.jpg" alt=""></a>

							<a class="example-image-link" href="images/moebel/web05.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web05.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web06.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web06.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web07.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web07.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web08.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web08.jpg" alt=""></a>

							<a class="example-image-link" href="images/moebel/web09.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web09.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web010.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web010.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web011.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web011.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web012.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web012.jpg" alt=""></a>

							<a class="example-image-link" href="images/moebel/web013.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web013.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web014.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web014.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web015.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web015.jpg" alt=""></a>
							<a class="example-image-link" href="images/moebel/web016.jpg" title="" data-lightbox="example-set" data-title=""><img class="example-image" src="images/moebel/thumbs/web016.jpg" alt=""></a>
						</div>
					</div>
					<p>Klicken Sie auf die Bilder um sie zu vergr&ouml;ssern</p>
					<br><br>
					<a href="downloads/ZuecoErgo201507.pdf" target="_blank">Ergonomie erleben (@ Z&Uuml;CO)</a>
					<br><br>
					
				</div>

				<div class="clearfix"></div>
				
				<script src="js/lightbox-plus-jquery.min.js"></script>
			<?php include 'inc.footer.html';?>
		<?php include 'inc.menuitemsM.html';?>
	</body>
</html>
