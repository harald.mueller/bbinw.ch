<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!--Start Content -->
		<br>
		<br>
		<div><h1>Kaufobjekte</h1></div>

<br><br>
<h2>Unsere BBINW internen  Angebote!</h2>
<p>Sie suchen eine Wohnung, ein Haus, ein B&#252;ro? BBINW bietet Ihnen eine reichhaltige Auswahl an attraktiven Immobilien.</p>
<p><a href="ortsinformationen.php">Ortsinformationen</a>&nbsp;&nbsp;&nbsp;<a href="referenzen.php">Referenzen</a></p><br><br>
	

<table align="left" width="200"  cellspacing="0" cellpadding="0" border="0">
	
<!-- Start menue -->
<table align="left">
         <tr>
         <form action="immo_request_ort.php" method="post" accept-charset="utf-8">
              <td>
								Ort<br>
              <select style="width: 170px; height: 30px;" name="ort">
             <option selected="" value="Aesch">Aesch (BL)</option>
              <option selected="" value="Arlesheim">Arlesheim (BL)</option>
<!--               <option selected="" value="Arisdorf">Arisdorf (BL)</option> -->
<!--               <option value="Basel">Basel (BS)</option> -->
<!--               <option value="Binningen">Binningen (BL)</option> -->
              <option value="Blauen">Blauen (BL)</option>
<!--               <option value="Bottmingen">Bottmingen (BL)</option>   -->
<!--               <option value="Dornach">Dornach (SO)</option> -->
              <option value="Effingen">Effingen (AG)</option>
<!--               <option value="Fuellinsdorf">F&#252;llinsdorf (BL)</option> -->
<!--               <option value="Gempen">Gempen (SO)</option> -->
              <option value="Grellingen">Grellingen (BL)</option>
<!--               <option value="Hornussen">Hornussen (AG)</option> -->
              <option value="Liesberg">Liesberg (BL)</option>
<!--               <option value="Nunningen">Nunningen (SO)</option> -->
              <option value="Schupfart">Schupfart (AG)</option>
<!--               <option value="Seewen">Seewen (SO)</option> -->
              <option value="Zeihen">Zeihen (AG)</option>
<!--               <option value="Zeiningen">Zeiningen (AG)</option> -->
              </select>
              </td>
              <td>
              <input type="submit" value="Anzeigen" style="width: 136px; margin-top: 25px;"/><br>
              </td>
         </form>
         </tr>

         <tr>
         <form action="immo_request_region.php" method="post" accept-charset="utf-8">
              <td>
								Region<br>
              <select style="width: 170px; height: 30px;" name="region">
              <option selected="" value="Basel-Land">Basel-Landschaft</option>
              <option value="Basel-Stadt">Basel-Stadt</option>
              <option value="Aargau">Aargau</option>
              <option value="Solothurn">Solothurn</option>
              <option value="Jura">Jura</option>
              <option value="Luzern">Luzern</option>
              <option value="Baden">Baden</option>
              <option value="Elsass">Elsass</option>
              </select>
              </td>
              <td>
              <input type="submit" value="Anzeigen" style="width: 136px; margin-top: 50px;"/><br><br>
              </td>
         </form>
         </tr>

         <tr>
         <form action="immo_request_land.php" method="post" accept-charset="utf-8">
              <td>
								Land<br>
              <select style="width: 170px; height: 30px;" name="land">
              <option selected="" value="Schweiz">Schweiz</option>
              <option value="Deutschland">Deutschland</option>
              <option value="Frankreich">Frankreich</option>
              </select>
              </td>
              <td>
              <input type="submit" value="Anzeigen" style="width: 136px; margin-top: 24px;"/>
              </td>
         </form>
         </tr>
</table>
<!-- End menue -->
</td>
</tr>
<tr>

</tr>
</table>


</div>

<div class="clearfix"></div>

<div class="iframe">
	
<div>
  <h2>  Öffentliche   Angebote!</h2></div>
	
	<div class="iframeGF">
		<b>KAUF-OBJEKTE</b>
		<p>---</p>
<script src="https://casaframe.ch/js/responsive-iframe.js" type="text/javascript"></script>
<iframe src="https://casaframe.ch/de/publisher/nhaXjYbMxCZtCz7fANatEkCBNQzrsPgU/?segment=buy" frameborder="0" width="100%" style="max-width: 100%; overflow: hidden;" id="casaframe" scrolling="no"></iframe>


	</div>	

	
	
</div>

<div class="space320"></div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
