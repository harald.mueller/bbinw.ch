<?php include 'inc.head.html';?>
	<body>
		<?php include 'inc.menuStickyTop.html';?>
			<div class="container">

				<?php include 'inc.secNavBar.html';?>

				<div class="container">
					<br><br>
					<h1>Wir freuen uns auf Ihr Feedback</h1><h2 class="goneForGood">BBINW baut mit Innovation f&uuml;r die Nordwestschweiz</h2>
					<h3 id="hide_undertitle">BBINW Nordwestschweiz</h3>
					<br>
				</div>

				<nav>
					<div class="far-left">
						<section>
							<h2>Feedback Formular</h2>
							<br>
							<p>Falls Sie mehr Informationen ben&ouml;tigen, kontaktieren Sie uns bitte &uuml;ber unseren <a id="follow-to-the-next-page" href="bbinw-kontakt.php" title="Kontakt Formular"><strong>Kontaktformular</strong>.</a></p>
							<br>
							<p>Wir werden uns so schnell wie m&ouml;glich mit Ihnen in Verbindung setzen.</p>
							<br>
							<p><i class="material-icons">train</i><a id="follow-to-the-next" href="osmaps.php" title="BBINW Standort"><strong> Hier finden Sie uns</strong></a></p>
						</section>
					</div>
				</nav>

				<div class="content">

					<div class="content-left">
						<div class="form">
							<form class="cmxform" id="commentForm" novalidate="novalidate" action="mailto:immobilien@bauland-nw.ch?subject=Feedback&amp;body=Feedback%20" method="POST" enctype="text/plain">
								<fieldset class="formFieldset">
									<legend>Feedbackformular</legend>
									<p>
										<label for="name">Name *</label><br>
										<input id="name" name="name" type="text" required>
									</p>
									<p>
										<label for="email">E-Mail *</label><br>
										<input id="email" type="email" name="email" required>
									</p>
									<p>
										<label for="comment">Feedback *</label><br>
										<textarea rows="4" cols="40" id="comment" name="comment" required></textarea>
									</p>
									<p>
										<button class="btnSubmit" id="download" type="submit" value="Senden">Senden</button>
										</p>
								</fieldset>
							</form>
						</div>
					</div>

					<div class="content-right">
						<div itemscope itemtype="http://schema.org/LocalBusiness">
							<a itemprop="url" href="http://bauland-nw.ch"></a><div itemprop="name"><strong>BBINW</strong> <a href="downloads/Firmenvorstellung.pdf" download="downloads/Firmenvorstellung.pdf" title="BBINW Firmenvorstellung" target="_blank"><i class="material-icons">file_download</i></a></div>
							<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<div><span itemprop="addressLocality">Arlesheim</span>, <span itemprop="addressRegion">BL</span><br></div>
							</div>
						</div>
						<br>
						<div itemscope itemtype="http://schema.org/Person">
							<a itemprop="url" href="http://bauland-nw.ch"></a><div itemprop="name"><strong>Susanne Bieli</strong> </div>
							<div itemprop="jobtitle">Immobilien Maklerin</div>
							<br>
							<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<div itemprop="streetAddress">Neumattstrasse 8</div>
								<div><span itemprop="postalCode">4144</span> <span itemprop="addressLocality">Arlesheim</span>, <span itemprop="addressRegion">BL</span></div>
							</div>
							<br>
							<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<div itemprop="streetAddress">M&uuml;hliweiher 9</div>
								<div><span itemprop="postalCode">5079</span> <span itemprop="addressLocality">Zeihen</span>, <span itemprop="addressRegion">AG</span></div>
							</div>
							<br>
							<div href="mailto:immobilien@bauland-nw.ch" itemprop="email">immobilien@bauland-nw.ch</div>
							<div itemprop="telephone">+41 61 599 27 46</div>
							<div itemprop="telephone">+41 76 413 1936</div>
						</div>
					</div>

				</div>

			</div>

			<div class="clearfix"></div>
			
			<div class="container">
				Hier k&ouml;nnen Sie bequem unsere Formulare und Dokumente im pdf Format downloaden
				<br>
				<a href="downloads.php"> &rArr; Downloads</a>
				<br>
			</div>
				
			<div class="clearfix"></div>					
			
			<div class="container">
				<a href="downloads/gutschein.pdf"><img src="icon/pdf.png"> &rArr; Gutschein</a>
			</div>
					
			
				<aside></aside>
			<?php include 'inc.footer.html';?>
		<?php include 'inc.menuitemsM.html';?>

		<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.min.js"></script>
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
		<!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/additional-methods.min.js"></script> -->
		<script> $("#commentForm").validate(); </script>
		<script src="js/contact.js"></script>
		<script src="js/download.js"></script>
	</body>
</html>
