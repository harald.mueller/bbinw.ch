<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>


  <!--Start Content -->  
<br>
  </br>
	
	
	
<div class="inhalt-objekte">
	
	 <div class="inhalt-zweiteilig-links fluid-gf-2 ">
		 	 <div class="objekt-gf padding5">
				 
<table width="392">
  <tr>
<td width="225">
<h2>Unsere Anschrift</h2>
	<p><span class="normal"><p><b>BBINW - Arlesheim<br></b>Neumattstrasse 8<br>4144 Arlesheim     
	<p><br>
	  <a href="tel:+41615992746">061 599 27 46 phone</a><br>061 599 47 45 fax<br>
	  <a href="tel:+41764131936">076 413 1936 mobile</a><br>
      <a href="tel:+41764181938">076 418 1938 mobile</a><br><br>
      <b>BBINW - Zeihen</b> <br>M&uuml;hliweiher 9<br>5079 Zeihen<br><br>
      <a href="tel:+41628762746">062 876 27 46 phone</a><br>
      <a href="tel:+41764131936">076 413 1936 mobile</a>
  <br>
  <a href="tel:+41764181938">076 418 1938 mobile</a>    
	<br>
		<br>
	<p><strong>Unternehmens-Identifikationsnummer:</strong><br>
	  <p>UID-Nr.: CHE-115.315.381<br>
	    <p><br>
    <table border="0" cellspacing="0" cellpadding="0">
  <tr>
  <td><strong>E-Mail:&nbsp;</strong></td><td><a class="link" href="mailto:&#105;&#109;&#109;&#111;&#098;&#105;&#108;&#105;&#101;&#110;&#064;&#098;&#097;&#117;&#108;&#097;&#110;&#100;&#045;&#110;&#119;&#046;&#099;&#104;">immobilien@bauland-nw.ch</a></td>
  </tr>
  <tr>
  <td>&nbsp;</td><td><a class="link" href="mailto:&#105;&#110;&#110;&#101;&#110;&#101;&#105;&#110;&#114;&#105;&#099;&#104;&#116;&#117;&#110;&#103;&#101;&#110;&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;">inneneinrichtungen@bbinw.ch</a></td>
  </tr>
  <tr>
  <td>&nbsp;</td><td><a class="link" href="mailto:&#102;&#105;&#110;&#097;&#110;&#122;&#100;&#105;&#101;&#110;&#115;&#116;&#108;&#101;&#105;&#115;&#116;&#117;&#110;&#103;&#101;&#110;&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;">finanzdienstleistungen@bbinw.ch</a></td>
  </tr>
  <tr>
  <td>&nbsp;</td><td><a class="link" href="mailto:&#097;&#100;&#109;&#105;&#110;&#105;&#115;&#116;&#114;&#097;&#116;&#105;&#111;&#110;&#115;&#104;&#105;&#108;&#102;&#101;&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;">administrationshilfe@bbinw.ch</a></td>
  </tr>
  <tr>
  <td>&nbsp;</td><td><a class="link" href="mailto:&#104;&#097;&#117;&#115;&#104;&#097;&#108;&#116;&#115;&#104;&#105;&#108;&#102;&#101;&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;">haushaltshilfe@bbinw.ch</a></td>
  </tr>
  <tr>
  <td>&nbsp;</td><td><a class="link" href="mailto:&#097;&#116;&#101;&#108;&#105;&#101;&#114;&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;">atelier@bbinw.ch</a></td>
  </tr>
</table></span>
<p>&nbsp;</p>
<p><a href="bbinw-kontakt.php"><strong>Kontakt-Formular</strong></a></p>
<p>&nbsp;</p>
<p><br>
  <b>Skype-Name: </b><a href="skype:bauland-nw?call">bauland-nw</a>
</p>
</p>
</p>
	<br>

<p><a href="https://www.facebook.com/BBINW/" target="_blank"><img src="icon/facebook-th.jpg" alt="Facebook Link" width="80" height="80"/></a></p>
<p>&nbsp;</p>
</td>
<td width="155"><img src="images/2017-11-09-Portrait-mit-Modell-G177_186.jpg" alt="Susanne Bieli" width="122" height="182" title="Susanne Bieli BBINW Arlesheim"/><i><br>Susanne Bieli<br>Gesch&auml;ftsf&uuml;hrerin<br></i>

</td>
</tr>
</table>
		 	 </div>

    </div>
		
      <div class="inhalt-zweiteilig-rechts fluid-gf-2">
		  <div class="objekt-gf padding5">
		  	
			 
		  <h2>Unser Standort</h2>
		 	 <div class="center">
	  		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1347.847092956489!2d7.618364332308731!3d47.49587061091385!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1810fa63e530005a!2sBBINW+Bauland+und+Immobilien+NW!5e0!3m2!1sde!2sus!4v1510169267379" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		  &nbsp;&nbsp;&nbsp;<img src="images/2014/buero-aussen-451.jpg" alt="BBINW B&uuml;ro Arlesheim" width="500" height="213" title="BBINW B&uuml;ro Arlesheim">
			  
			  <br>
			  </div>
<p>
Unser B&uuml;ro an der Neumattstrasse 8, 4144 Arlesheim<br>
10-er Tram bis Station "Arlesheim Dorf"<br>
Besucherparkpl&auml;tze vorhanden.<br>
</p>
	 		  
	
		  </div>
      </div>
	

	
	</div>
	


<!--End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
