<?php include 'inc.head.html';?>
	<body>
		<?php include 'inc.menuStickyTop.html';?>
			<div class="container">

				<?php include 'inc.secNavBar.html';?>

				<div class="container">
					<br><br>
					<h1>Kontakt & Impressum</h1>
					<h2 class="goneForGood">BBINW baut mit Innovation f&uuml;r die Nordwestschweiz</h2>
					<h3 id="hide_undertitle">BBINW Nordwestschweiz</h3>
					<br>
				</div>

				<nav>
					<div class="far-left">
						<section>
							<h2>Kontakt Formular</h2>
							<br>
							<p>Setzen Sie sich noch heute mit uns in Verbindung und vereinbaren Sie einen Termin oder hinterlassen Sie uns eine Nachricht mittels unserem Kontaktformular.</p>
							<br>
							<p>Wir werden uns so schnell wie m&ouml;glich mit Ihnen in Verbindung setzen.</p>
							<br>
							<p>Falls Sie uns ein Feedback senden wollen, benutzen Sie bitte unser <a id="follow-to-the-next-page" href="bbinw-feedback.php" title="Feedback Formular"><strong>Feedbackformular</strong></a>.</p>
							<br>
							<p><i class="material-icons">train</i><a id="follow-to-the-next" href="kontakt.php" title="BBINW Standort"><strong> Hier finden Sie uns</strong></a></p>
						</section>
					</div>
				</nav>

				<div class="content">

					<div class="content-left">
						<div class="form">
							<!-- <form class="cmxform" id="commentForm" action="javascript:download()" method="POST"> -->
							<form class="cmxform" id="commentForm" novalidate="novalidate" action="mailto:immobilien@bauland-nw.ch?subject=Kontakt&amp;body=Kontakt%20" method="POST" enctype="text/plain">
								<fieldset class="formFieldset">
									<legend>Kontaktformular</legend>
										<p>
											<label for="firstname">Vorname *</label><br>
											<input id="firstname" name="firstname"  type="text" required>
										</p>
										<p>
											<label for="lastname">Name *</label><br>
											<input id="lastname" name="lastname"  type="text" required>
										</p>
										<p>
											<label for="email">Email *</label><br>
											<input id="email" type="email" name="email" required>
										</p>
										<p>
											<label for="phone">Telefonnummer</label><br>
											<input id="phone" type="tel" name="phone">
										</p>
										<p>
											<label for="subject">Betreff *</label><br>
											<input id="subject" name="subject"  type="text" required>
										</p>
										<p>
											<label for="comment">Nachricht *</label><br>
											<textarea rows="4" id="comment" name="comment"  maxlength="1000" required></textarea>
										</p>
										<p>
											<button class="btnSubmit" id="download" type="submit" value="Senden">Senden</button>
										</p>
								</fieldset>
							</form>
						</div>
					</div>

					<div class="content-right">
						<div itemscope itemtype="http://schema.org/LocalBusiness">
							<a itemprop="url" href="http://bauland-nw.ch"></a><div itemprop="name"><strong>BBINW</strong> <a href="downloads/Firmenvorstellung.pdf" download="downloads/Firmenvorstellung.pdf" title="BBINW Firmenvorstellung" target="_blank"><i class="material-icons">file_download</i></a></div>
							<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<div><span itemprop="addressLocality">Arlesheim</span>, <span itemprop="addressRegion">BL</span><br></div>
							</div>
						</div>
						<br>
						<div itemscope itemtype="http://schema.org/Person">
							<a itemprop="url" href="http://bauland-nw.ch"></a><div itemprop="name"><strong>Susanne Bieli</strong> </div>
							<div itemprop="jobtitle">Immobilien Maklerin</div>
							<br>
							<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<div itemprop="streetAddress">Neumattstrasse 8</div>
								<div><span itemprop="postalCode">4144</span> <span itemprop="addressLocality">Arlesheim</span>, <span itemprop="addressRegion">BL</span></div>
							</div>
							<br>
							<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
								<div itemprop="streetAddress">M&uuml;hliweiher 9</div>
								<div><span itemprop="postalCode">5079</span> <span itemprop="addressLocality">Zeihen</span>, <span itemprop="addressRegion">AG</span></div>
							</div>
							<br>
							<div href="mailto:immobilien@bauland-nw.ch" itemprop="email">immobilien@bauland-nw.ch</div>
							<div itemprop="telephone">+41 61 599 27 46</div>
							<div itemprop="telephone">+41 76 413 1936</div>
						</div>
					</div>
			</div>

			<div class="clearfix"></div>					

			<div class="container">
				Hier k&ouml;nnen Sie bequem unsere Formulare und Dokumente im pdf Format downloaden
				<br>
				<a href="downloads.php"> &rArr; Downloads</a>
				<br>
			</div>
				
			<div class="clearfix"></div>					
			
			<div class="container">
				<a href="downloads/gutschein.pdf"><img src="icon/pdf.png"> &rArr; Gutschein</a>
			</div>
					
			<div class="container">

				<nav>
					<div class="far-left">
						<section>
							<h2>Impressum</h2>
						</section>
					</div>
				</nav>
				
				<div class="content-left"><!-- class="circular" -->      <!-- class="circular-image" -->
						<section>
							<p><b>BBINW</b><br>Neumattstrasse 8<br>4144 Arlesheim<br><br><a href="tel:+41615992746">061 599 27 46 phone</a>
							<br>061 599 47 45 fax<br><a href="tel:+41764131936">076 413 1936 mobile</a>
							<br><a href="tel:+41764181938">076 418 1938 mobile</a>
							<br>
							<br><b>Gesch&auml;ftsf&uuml;hrerin</b>
							<div><img src="img/susanne_bieli1.jpg" style="height: 130px;" alt="Susanne Bieli - Gesch&auml;ftsf&uuml;hrerin" ></div>
							<p>Frau Susanne Bieli <br>
							<br>
							<i>E-Mail Adresse des Betreibers:</i>
							<br>
							<b>E-Mail:</b> <a class="link" href="mailto:immobilien@bauland-nw.ch">immobilien@bauland-nw.ch</a><br>
							<br>
							<a href="skype:bauland-nw?call" title=""><b>Skype-Name: </b>bauland-nw</a><br>
							<a class="link" href="mailto:webmaster@bauland-nw.ch">webmaster@bauland-nw.ch</a></p>
							<br>
							<p><strong>Unternehmens-Identifikationsnummer:</strong><br>
							  UID Nr.: CHE-115.316.381<br>
						  </p>
							<p>&nbsp;</p>
							<p><strong>Urheberrechtshinweis:</strong><br>
							Alle Inhalte dieses Internetangebotes, insbesondere Texte,
							Fotografien und Grafiken, sind urheberrechtlich gesch&uuml;tzt
							(&copy; Copyright BBINW Arlesheim 2009-2017). Das Urheberrecht liegt,
							soweit nicht ausdr&uuml;cklich anders gekennzeichnet, bei
							BBINW Arlesheim. Bitte fragen Sie BBINW Arlesheim, falls Sie
							die Inhalte dieses Internetangebotes verwenden m&ouml;chten.
							<br><br>
							Wer gegen das Urheberrecht verst&ouml;&szlig;t (z.B. die Inhalte
							unerlaubt auf die eigene Homepage kopiert), macht sich gem&auml;ss
							Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt
							und muss Schadensersatz leisten. Kopien von Inhalten k&ouml;nnen
							im Internet ohne gro&szlig;en Aufwand verfolgt werden.
							<br><br>
								<p><strong>Datenschutzerkl&auml;rung:</strong><br>
								Cookies<br>
								Beim Besuch von Webseiten des Anbieters erhebt, speichert und
								verwendet der Anbieter sowohl von registrierten als auch von nicht
								registrierten Nutzern allgemeine, nicht personenbezogene Daten.
								Dazu werden u.a. sogenannte „Cookies“ eingesetzt. Dabei handelt es sich
								um Textdateien, die auf dem Benutzer-System des Anbieters sowie allf&auml;lligen
								Dritten gespeichert werden und die eine Analyse der Benutzung der Produkte ermöglicht.
								Diese Technologie erlaubt es, einzelne Nutzer als Besucher zu erkennen und ihnen
								individualisierte Dienstleistungen und Produkte zu unterbreiten.
								Solche Trackingdaten werden getrennt von allf&auml;llig erhobenen Personendaten gespeichert.
								Die Erhebung erfolgt ausschliesslich in anonymer Form, womit zu keinem Zeitpunkt ein R&uuml;ckschluss
								auf den einzelnen Nutzer möglich ist. Es wird einzig eine Verbindung zur bekannten
								IP-Adresse ermöglicht ohne dahinter den jeweiligen Nutzer persönlich identifizieren zu können.
								Nutzer von Webseiten des Anbieters können das Speichern von Cookies verhindern, indem sie
								in ihren Browser- Einstellungen "keine Cookies akzeptieren" w&auml;hlen.
								Dies kann jedoch die Funktionalit&auml;t der Produkte behindern und eine optimierte Nutzung der Online-Dienste f&uuml;r den Nutzer erschweren.
								<br><br>
								Google Analytics<br>
								Dieses Angebot nutzt ebenfalls den Webanalysedienst Google Analytics, ein Programm der Google Inc.
								(„Google, USA“). Die durch das Tracking erfassten Informationen zu Ihrer Nutzung
								dieser Website werden auf einem Server von Google in den USA gespeichert.
								Durch eine sogenannte IP-Anonymisierung wird Ihre IP-Adresse von Google innerhalb
								 von Mitgliedstaaten der Europ&auml;ischen Union oder in anderen Vertragsstaaten des
								 Abkommens &uuml;ber den Europ&auml;ischen Wirtschaftsraum zuvor gek&uuml;rzt. Somit ist der
								 Standort Ihres Browsers lediglich regional zuortbar, nicht aber ihrer Person.
								 Google kann Besucherverhalten auswerten, um Reports &uuml;ber die Websiteaktivit&auml;ten
								 zusammenzustellen. Auch weitere mit der Websitenutzung und der Internetnutzung
								 verbundene Dienstleistungen können so gegen&uuml;ber dem Websitebetreiber erbracht werden.
								<br><br>
								BBINW Arlesheim 29. Mai 2018<br>
							<br>
						  </p>
						</section>
						<!-- <div class="circular"><img src="img/nadine-gordimer.jpg" title="nadine gordimer" alt="nadine gordimer" class="circular-image"></div>
						<br>
							<p>Wie jemand aussieht ist einem guten Foto egal.</p> -->
				</div>
				

				<div class="content-right">
					<article>
						<h2></h2>
						<br>
						<p></p>
					</article>
				</div>					
				
				
				
			</div>

		</div>

		<div class="clearfix"></div>

		<?php include 'inc.footer.html';?>
		<?php include 'inc.menuitemsM.html';?>

		<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.min.js"></script>
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
		<!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/additional-methods.min.js"></script> -->
		<script> $("#commentForm").validate(); </script>
		<script src="js/contact.js"></script>
		<script src="js/download.js"></script>
	</body>
</html>
