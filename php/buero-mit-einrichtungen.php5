<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<div>
<h2>Unser B&uuml;ro mit Einrichtungen von unseren Vertriebspartnern</h2>



<img src="images/logos/bosse-claim-300.jpg">
<br><br>
<img src="images/logos/zueco-300.jpg">
<br><br>
<img src="images/logos/tamos-300.jpg">
<br><br>
<img src="images/logos/balette-300.jpg">
<br><br>
<img src="images/logos/fischbacher-300.jpg">
<br><br>
<img src="images/logos/albis-300.jpg">
<br><br>
<img src="images/2014/schaufenstermoebel.jpg">
<!--
<p>
<a class="link" href="./verwaltung.php5"><b>Immobilienverwaltung<br>
(&rArr; Weitere Informationen finden Sie hier...)</b></a>
<br><br>
<b>Individuelle Beratung rund um Liegenschaften</b><br>
-  Standortevaluation und Bauplanung<br>
-  Baumanagement (Architektur, Bauhandwerk, Bewilligungen, etc.)<br>
-  Finanzdienstleistungen<br>
-  Erstellen von Verkehrswertsch&auml;tzungen und Expertisen<br>
-  <a href="treuhand.php5"><b>Treuhandmandate</b>zu Liegenschaftsverk&auml;ufen und Vermietungen<br>
<b><i>&nbsp;&nbsp;&nbsp;(&rArr; Weitere Informationen finden Sie hier...)</i></b>
<br><br></a>
</p>
<p>
<b>Imageberatung - Raumgestaltung</b><br>
-  Inneneinrichtung, Antike M&ouml;bel, Kunst am Bau<br>
-  Auserlesene M&ouml;bel - f&uuml;r B&uuml;ro-, Restaurant-, und Wohnbereich<br>
-  Beleuchtungsplanung und Installation<br><br>
</p>
<p>
<b>Konditionen - f&uuml;r Makler- und Beratungst&auml;tigkeit</b><br>
-  Beratung zu CHF 160.--/Std., erste Stunde wird nicht verrechnet<br>
-  Besondere Aufwendungen werden separat verrechnet<br>
-  Kilometerspesen CHF 1.--/km<br>
-  Maklerprovision gem&auml;ss SVIT:<br>
&nbsp;&nbsp;&nbsp;&bull; 3 % des Verkaufspreises bei Liegenschaften<br>
&nbsp;&nbsp;&nbsp;&bull; 5 % des Verkaufspreises bei Bauland<br>
&nbsp; oder nach Stundenaufwand<br>
-  bei Spezialobjekten ist ein Pauschalpreis m&ouml;glich<br>
-  Individuelles Werbebudget nur auf Vorauszahlung des Auftraggebers<br>
<br>
</p>
<p>
<b>Administrative Aufgaben - B&uuml;rodienstleistungen</b><br>
-  Sekretariat pro Stunde CHF 95.-- zuz&uuml;glich MwSt.<br>
-  Flyers erstellen inkl. ein- &uuml;gen der Fotos mit Hilfe des prov. Photoshops<br>
-  Stellensuche (Schreiben Ihrer Lebensl&auml;ufe und Bewerbungsschreiben)<br>
-  Inseratencampagnen in Print- und Internetmedien<br>
-  Inseratenkosten werden separat verrechnet<br>
-  Zahlungs- und Buchhaltungsauftr&auml;ge<br>
-  Debitorenberater-Dienstleistungen (Delcreding)<br>
-  Stundenerfassungen<br>
-  Bauabrechnungen<br>
<br>
</p>
<p>
<b>Gesch&auml;ftsadresse - Gesch&auml;ftsdomizil</b><br>
-  Anschrift aussen<br>
-  Postweiterleitung<br>
-  Telefonnummer und Faxservice<br>
-  Monatspauschale CHF 350.--<br>
<br>
</p>
<p>
<b>Telefonservice</b><br>
-  werktags von 08.00 bis 11.30 und 13.00 bis 17.00 Uhr<br>
-  Montasabo  CHF 350.--<br>
<br>
</p>
<p>
<b>Telefonservice - Abo light</b><br>
-  werktags von 08.00 bis 11.30 und 13.00 bis 17.00 Uhr<br>
-  Grundpauschale 1. Woche CHF  50.--<br>
-  jede weitere Woche CHF 25.--, sowie zus&auml;tzlich pro Anruf CHF 1.50<br>
<br>
</p>
-->




</div>


<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>