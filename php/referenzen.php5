<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Referenzen</h2>

<table border="1" width="450">
<tr>
<td>
<img width="150px" src="./images/muehli_plan1.jpg" ></a><br><br>
</td>
<td>
<a href="muehliweiher.php5"><b>1. Erstellung einer Doppel-EFH-H&auml;lfte:</b><br><br>
M&uuml;hliweiher 9 in Zeihen<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>
<tr>
<td>
<img width="150px" src="./images/projekt_muenchenstein.jpg" ></a><br><br>
</td>
<td>
<a href="muenchenstein.php5"><b>2. Einfamilienhaus:</b><br><br>
am Nollenrain in M&uuml;nchenstein (BL)<br><br>
Verkauf ab Plan<br><br>
Preisvorgabe f&uuml;r Projekt<br>
durch BBINW<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>
<tr>
<td>
<img width="150px" src="./images/gempenturm.jpg" ></a><br><br>
</td>
<td>
<a href="gempen.php5"><b>3. Restaurant Gempenturm:</b><br><br>
Verkauf an 3. Interessenten<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/sockel.jpg" ></a><br><br>
</td>
<td>
<a href="sockel.php5"><b>4. Ersteigerungsobjekt:</b><br><br>
Eigentumswohnung in Arlesheim (BL)<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/stein_plan4.jpg" ></a><br><br>
</td>
<td>
<a href="stein.php5"><b>5. Areal&uuml;berbauung:</b><br><br>
Projekt in Stein (AG)<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/tannen1.jpg" ></a><br><br>
</td>
<td>
<a href="tannen.php5"><b>6. Bauprojekte:</b><br><br>
Im Lee Gebiet in Arlesheim (BL)<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/arlesheim1.jpg" ></a><br><br>
</td>
<td>
<a href="arlesheim.php5"><b>7. Div. Einfamilienh&auml;user:</b><br><br>
am Zirkelacker in Arlesheim (BL)<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/batteriestrasse2b.jpg" ></a><br><br>
</td>
<td>
<a href="batteriestrasse.php5"><b>8. Garten-Eigentumswohnung:</b><br><br>
Batteriestrasse 19 in Bottmingen (BL)<br><br>
Verkauf aller verk&auml;uflichen Wohnungen dieser Immobilie in Konkurrenz zu 3 weiteren Immobilienanbietern<br><br>
Verkauf der Gartenwohnung (185 qm)<br>
an den zweiten Interessenten f&uuml;r
<br>VP CHF 1.35 Mio.<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/pratteln2.jpg" ></a><br><br>
</td>
<td>
<a href="pratteln.php5"><b>9. Garten-Eigentumswohnung:</b><br><br>
Sehr grosse Wohnung<br>
beim Schloss in Pratteln (BL)<br><br>
Verkauf an Fam. Forster<br>
inkl. Inneneinrichtungsberatung.<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/BIE 40020_1_800x600-75.jpg" ></a><br><br>
</td>
<td>
<a href="bierastrasse.php5"><b>10. Attika-Eigentumswohnung:</b><br><br>
Bierastrasse in Bottmingen (BL)<br><br>
Wohnf&auml;che inkl. Balkon 306 qm<br><br>
Verkauf an 1. Interessenten f&uuml;r<br>
VP 1.64 Mio. CHF<br><br>
Verkauf innert 4 Wochen<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/bottmingenE1.jpg" ></a><br><br>
</td>
<td>
<a href="batteriestrasseEtage.php5"><b>11. Eigentumswohnung:</b><br><br>
Batteriestrasse 19 in Bottmingen (BL)<br><br>
Verkauf aller verk&auml;uflichen Wohnungen dieser Immobilie in Konkurrenz zu 3 weiteren Immobilienanbietern<br><br>

Verkauf der Etagenwohnung (170 qm) f&uuml;r <br>
VP CHF 1.25 Mio.<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/BIE 40010_13.jpg" ></a><br><br>
</td>
<td>
<a href="schellenberg.php5"><b>12. 4.5 Zi-Eigentumswohnung:</b><br><br>
Unterm Schellenberg in Riehen<br><br>
Verkauf an 2. Interessenten<br><br>
Mandat durch neue Besitzer<br>
der Eigentumswohnung von<br>
Referenzobjekt-Nr. 9<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/BIE 20026_2_800x600-75" ></a><br><br>
</td>
<td>
<a href="vendlincourt.php5"><b>13. Vendlincourt (Ju):</b><br><br>
Maison individuelle adapt&eacute;e<br>
pour chaise roulante<br><br>
(Willensvollstreckungsmandat)<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="http://bauland-nw.ch/php/webservice/images/BIE%2090009_1_800x600-75.jpg" ></a><br><br>
</td>
<td>
<a href="olsberg.php5"><b>14. Olsberg (AG):</b><br><br>
Restaurant 'Zum R&ouml;ssli' in Olsberg<br>
<br><br>
<br><br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="http://bauland-nw.ch/php/webservice/images/BIE%2020061_a1_800x600-75.jpg" ></a><br><br>
</td>
<td>
<b>15. W&ouml;lflinswil (AG):</b><br><br>
EFH mit Einliegerwohnung<br>
<br><br>
<br><br>
</td>
</tr>

<tr>
<td>
<img width="150px" src="./images/buro_hr.jpg" ></a><br><br>
</td>
<td>
<a href="./downloads/Bueroraum HR_Flyer.pdf"><b>16. Arlesheim (BL):</b><br><br>
B&uuml;ror&auml;umlichkeiten<br>
<b><i>(&rArr; weitere Informationen...)</i></b>
</td>
</tr>

<tr>
<td>
<img width="150px" src="http://bauland-nw.ch/php/webservice/images/BIE 10009_1_800x600-75.jpg" ></a><br><br>
</td>
<td>
<b>17. Zeiningen (AG):</b><br><br>
Verkauf von Bauland mit Panorama-Blick f&uuml;r den Bau eines 3-Familienhauses und Vermittlung eines geeigneten Architekten.<br>
<br><br>
<br><br>
</td>
</tr>

<tr>
<td>
<img width="150px" src="http://bauland-nw.ch/php/webservice/images/BIE 50005_2_800x600-75.jpg" ></a><br><br>
</td>
<td>
<b>18. Basel (BS):</b><br><br>
Verwaltungsmandat Aug. 10 bis Dez. 2012

MFH f&uuml;r 4 Familien und separate Garage,

am Morgartenring 173 in Basel.<br>
<br>
Geleistete Arbeiten: <br>

3 Mietvertr&auml;ge, Wohnungsrenovation innert 13 Tagen f&uuml;r eine 120 m2 Wohnung
R&auml;umungsklage mit Ausweisung innert k&uuml;rzester Zeit.  <br>
<br><br>
<br><br>
</td>
</tr>

<tr>
<td>
<img width="150px" src="http://bauland-nw.ch/php/webservice/images/BIE 10015_1_800x600-75.jpg" ></a><br><br>
</td>
<td>
<b>19. Nunningen (SO):</b><br><br>
Verkauf diverser Baugrundst&uuml;cke in Nunningen, teils inklusive Bauprojekten.<br>
<br><br>
<br><br>
</td>
</tr>

<tr>
<td>
<img width="150px" src="http://bauland-nw.ch/php/webservice/images/BIE 10013_2_800x600-75.jpg" ></a><br><br>
</td>
<td>
<b>20. Rickenbach (LU):</b><br><br>
Verkauf ca. 5000 m2 Bauland in Rickenbach.<br>
<br><br>
<br><br>
</td>
</tr>

</table>

<br><br>
<p>
Des weiteren konnte BBINW diverse Landst&uuml;cke im Fricktal erfolgreich<BR>
vermitteln. Suchen auch Sie einen K&auml;ufer f&uuml;r ein Landst&uuml;ck, oder planen<BR>
Sie ein Bauvorhaben und suchen noch einen passenden Standort?<br>
Bitte kontaktieren Sie uns. Wir werden gerne f&uuml;r Sie t&auml;tig.
</p>

<br><br><a class="link" href="./downloads/informationen_referenzobjekte.pdf"><b><i>&rArr; Referenzen (pdf-Version)</i></b></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>