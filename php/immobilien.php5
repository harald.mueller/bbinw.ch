<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2>Unsere aktuellen Angebote!</h2>
<p>Sie suchen eine Wohnung, ein Haus, ein B&#252;ro? BBINW bietet Ihnen eine reichhaltige Auswahl an attraktive Immobilien.</p><br><br>

<table align="left" width="200"  cellspacing="0" cellpadding="0" border="0"   >

<!-- Start menue -->
<table align="left">
         <tr>
         <form action="immo_request_ort.php5" method="post" accept-charset="utf-8">
              <td>Ort</td>
              <td>
              <select style="width: 170px;" name="ort">
              <option selected="" value="Aesch">Aesch (BL)</option>
              <option selected="" value="Arlesheim">Arlesheim (BL)</option>
              <option selected="" value="Arisdorf">Arisdorf (BL)</option>
              <option value="Basel">Basel (BS)</option>
              <option value="Binningen">Binningen (BL)</option>
              <option value="Blauen">Blauen (BL)</option>
              <option value="Bottmingen">Bottmingen (BL)</option>  
              <option value="Dornach">Dornach (SO)</option>
              <option value="Effingen">Effingen (AG)</option>
              <option value="Fuellinsdorf">F&#252;llinsdorf (BL)</option>
              <option value="Gempen">Gempen (SO)</option>
              <option value="Grellingen">Grellingen (BL)</option>
               <option value="Hornussen">Hornussen (AG)</option>
               <option value="Liesberg">Liesberg (BL)</option>
              <option value="Nunningen">Nunningen (SO)</option>
              <option value="Schupfart">Schupfart (AG)</option>
              <option value="Seewen">Seewen (SO)</option>
              <option value="Zeihen">Zeihen (AG)</option>
              <option value="Zeiningen">Zeiningen (AG)</option>
              </select>
              </td>
              <td>
              <input type="submit" value="Objekte anzeigen" />
              </td>
         </form>
         </tr>

         <tr>
         <form action="immo_request_region.php5" method="post" accept-charset="utf-8">
              <td>Region</td>
              <td>
              <select style="width: 170px;" name="region">
              <option selected="" value="Basel-Land">Basel-Landschaft</option>
              <option value="Basel-Stadt">Basel-Stadt</option>
              <option value="Aargau">Aargau</option>
              <option value="Solothurn">Solothurn</option>
              <option value="Jura">Jura</option>
              <option value="Luzern">Luzern</option>
              <option value="Baden">Baden</option>
              <option value="Elsass">Elsass</option>
              </select>
              </td>
              <td>
              <input type="submit" value="Objekte anzeigen" />
              </td>
         </form>
         </tr>

         <tr>
         <form action="immo_request_land.php5" method="post" accept-charset="utf-8">

              <td>Land</td>
              <td>
              <select style="width: 170px;" name="land">
              <option selected="" value="Schweiz">Schweiz</option>
              <option value="Deutschland">Deutschland</option>
              <option value="Frankreich">Frankreich</option>
              </select>
              </td>
              <td>
              <input type="submit" value="Objekte anzeigen" />
              </td>
         </form>
         </tr>
</table>
<!-- End menue -->
</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>
