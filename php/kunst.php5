<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Wohnraum - Kunstraum</h2>
<p>Wohnr&auml;ume und deren Gestaltung sind ein Ausdruck der Pers&ouml;nlichkeit. Nirgendwo sonst verbringt man gew&ouml;hnlich mehr Zeit mit der Betrachtung von Kunst als in den eigenen vier W&auml;nden. Dem Betrachter ein entspannendes und gleichzeitig interessantes Sehvergn&uuml;gen zu verschaffen sollte hier angestrebt werden, eine Harmonie zwischen Architektur und Bilderwelt.</p>

<br><br><a class="link" href="kunst1.php5"><b>K&uuml;nstler aus der Region</b><br><i>(&rArr; Weitere Informationen finden Sie hier...)</i></a>

<br><br><a class="link" href="kunst2.php5"><b>Chinesische Malerei</b><br><i>(&rArr; Weitere Informationen finden Sie hier...)</i></a>

<br>
<br>Bitte kontaktieren Sie uns, wenn wir Ihr Interesse geweckt haben.<br><br></p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>