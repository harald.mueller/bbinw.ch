<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Unsere Linkliste</h2>
<p><span class="normal"><br>

<a class="link" href="http://www.beyeler-dienstleistung.ch/" target="_blank">
<img width="150px" src="./images/logo_beyeler.jpg" ><br><br>
<b>Beyeler-Dienstleistung.ch</b><br><i>
Der professionelle Dienstleister f&uuml;r KMU und Private</i></a><br><br>

<a class="link" href="http://www.albis.ch/" target="_blank"><b>ALBIS Bettwarenfabrik AG</b><br><i>Duvets und Kissen</i></a><br><br>

<a class="link" href="http://www.fischbacher.ch/" target="_blank"><b>Christian Fischbacher Co. AG</b><br><i>Bettw&auml;sche, Frottier, Daunen und Federn, Einrichtungsstoffe</i></a><br><br>

<a class="link" href="straumann.php5" target="_blank"><b>Straumann</b><br><i>Rohr- und Ablaufreinigungs-Service</i></a><br><br>

<a class="link" href="http://www.toyourassistance.ch/" target="_blank"><b>to.Your.assistance</b><br><i>Personal-, Relocation & Event-Assistance</i></a><br><br>

<a class="link" href="http://www.antikstyle.ch" target="_blank"><b>Antik Style</b><br><i>Antiquit&auml;ten, Kuriosit&auml;ten, Lampen,...</i></a><br><br>

<a class="link" href="http://www.businessasia.ch/" target="_blank"><b>businessasia.ch</b><br><i>Der Asienspezialist f&uuml;r Wirtschaft und Kultur</i></a><br><br>

<a class="link" href="http://www.efficiency-club.ch/" target="_blank"><b>Efficiency Club Basel</b><br><i>Trinationale Wirtschaftsnetzwerk der Region metroBasel</i></a><br><br>

<a class="link" href="http://www.schnell-natursteine.ch/" target="_blank"><b>Schnell Natursteine AG</b><br><i>Bildhauerei, Steinhauerei</i></a><br><br>

<b><u>Interessante Links zu Energiefragen (Neubau und Renovationen)</u></b>
<br><br>

<a class="link" href="http://www.bfe.admin.ch/" target="_blank"><b>Energie Schweiz</b><br><i>Beratungsstelle der Kantone</i></a><br><br>

<a class="link" href="http://www.bfe.admin.ch/bauschlau/index.html?lang=de" target="_blank"><b>bau-schlau</b><br><i>Energie-Spar-Tipps</i></a><br><br>

<a class="link" href="http://www.wwf.ch/de/tun/tipps_fur_den_alltag/wohnen/heizen/" target="_blank"><b>WWF - for a living planet</b><br><i>Welche Heizung darf es sein?</i></a><br><br>

<a class="link" href="http://www.klimarappen.ch/" target="_blank"><b>Stiftung Klimarappen</b><br><i>Informationen &uuml;ber F&ouml;rderbeitr&ouml&auml;ge</i></a><br><br>

<a class="link" href="http://www.vin-du-jura.ch/" target="_blank"><b>Vin du Jura (Bio-Wein)- Winzer Martin Buser</b><br><i>Excellente Bio-Weine f&uuml;r Kenner</i></a><br><br>

</span></p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>