<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Landverkauf im Fricktal</h2>
<p>
F&uuml;r unsere Mandanten haben wir diverse Landst&uuml;cke im Fricktal erfolgreich vermittelt. Suchen auch Sie einen K&auml;ufer f&uuml;r ein Landst&uuml;ck, oder planen Sie ein Bauvorhaben und suchen noch einen passenden Standort? Bitte kontaktieren Sie uns. Wir werden gerne f&uuml;r Sie t&auml;tig.
</p>
<br><br>
<img width="450" src="./images/BIE 10001_1_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 10001_2_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 10001_3_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 10004_1_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 10004_2_800x600-75.jpg" ><br><br>
<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>