<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Impressum</h2>
<span class="normal"><p><i>Name und Anschrift des Betreibers:</i><b><br><br>BBINW - Arlesheim</b><br>Neumattstrasse 8<br>4144 Arlesheim<br><br>061 599 27 46 phone<br>061 599 47 45 fax<br>076 413 19 36 mobile<br>076 418 19 38 mobile<br><br><b>Gesch&auml;ftsf&uuml;hrerin</b><br>Frau Susanne Bieli<br><br><i>E-Mail Adresse des Betreibers:</i><br><br><b>E-Mail:</b> <a class="link" href="mailto:%28keine%20Angaben%29" target="_blank">immobilien@bauland-nw.ch</a><br><br><b>Skype-Name: </b>bauland-nw<br><br>
<a class="link" href="mailto:%28keine%20Angaben%29" target="_blank">webmaster@bauland-nw.ch</a><br>
<br>
<i>Urheberrechtshinweis:</i><br>
<br>
Alle Inhalte dieses Internetangebotes, insbesondere Texte,<br>
Fotografien und Grafiken, sind urheberrechtlich gesch&uuml;tzt<br>
(&copy; Copyright BBINW Arlesheim 2009). Das Urheberrecht liegt,<br>
soweit nicht ausdr&uuml;cklich anders gekennzeichnet, bei<br>
BBINW Arlesheim. Bitte fragen Sie BBINW Arlesheim, falls Sie<br>
die Inhalte dieses Internetangebotes verwenden m&ouml;chten.<br>
<br>
Wer gegen das Urheberrecht verst&ouml;&szlig;t (z.B. die Inhalte<br>
unerlaubt auf die eigene Homepage kopiert), macht sich gem&auml;ss<br>
Urhebergesetz strafbar. Er wird zudem kostenpflichtig abgemahnt<br>
und muss Schadensersatz leisten. Kopien von Inhalten k&ouml;nnen<br>
im Internet ohne gro&szlig;en Aufwand verfolgt werden.<br>
<br>
BBINW Arlesheim 01. Dezember 2009<br>
<br>
</p></span>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>