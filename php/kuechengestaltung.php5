<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2 align="left">Suchen Sie nach Ihrer Individuellen K&uuml;che?<br></h2>
<p>Wir beraten Sie gerne bei der Gestaltung Ihrer individuellen K&uuml;che.<br><br>
Bitte kontaktieren Sie uns, wenn wir Ihr Interesse geweckt haben.

<br><br><a class="link" href="./downloads/musterkuechen_20080715.pdf">Download: &Uuml;bersicht K&uuml;chenplanung (PDF)</a>
<br><br><a class="link" href="./downloads/checkliste_kueche_20080709.pdf">Checkliste K&uuml;chenplanung (PDF)</a>

</p>

<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>