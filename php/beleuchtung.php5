<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2 align="left">Lust auf mehr Licht?<br></h2>
<p>Wir beraten Sie gerne bei der Auswahl der passenden<br>
Beleuchtung f&uuml;r den Innen- und Aussenbereich.</p>
<br>
<br>

<b><img src="./images/fira5.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/fira1.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Licht-Kunst mit FIRALUX Design</b>
<p>schafft eine positive Atmosphere.<br clear="all"></p>
<br><br>

<b><img src="./images/fira2.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">FIRALUX Design</b>
<p>ist ein starker Partner f&uuml;r clevere Beleuchtungsl&ouml;sungen.<br clear="all"></p>
<br><br>

<b><img src="./images/fira3.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?">Suchen Sie eine optimale L&ouml;sung?</b>
<p>Auf Wunsch ber&auml;t Sie ein ausgewiesener Fachmann f&uuml;r Beleuchtungsfragen.<br>
Herr Beat Sch&ouml;nenberger bringt Ihnen gerne Lampen zur Auswahl.<br clear="all"></p>
<br><br>

<b><img src="./images/fira4.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?">Licht schafft Ambiente</b>
<p>Wir freuen uns, Ihnen die sch&ouml;nste Beleuchtung anbieten zu k&ouml;nnen.<br>
<p>Vereinbaren Sie noch heute einen Besprechungstermin und<br>
realisieren Sie Ihr individuelles Beleuchtungskonzept.<br clear="all"></p>
<br><br>
<a class="link" href="./downloads/informationen_beleuchtung.pdf"><b><i>&rArr;&nbsp;Flyer Beleuchtung (pdf)</i></b></a>
<br><br>



<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>