<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
	
	
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->
<div align="center" cellspacing="0" cellpadding="0" border="0" style="padding:10px;">

<a href="kontakt.php5">Kontakt</a>	<a href="wir.php5">Firmenvorstellung</a>	<a href="kontakt.php5">Unser Standort</a>	<a href="impressum.php5">Impressum</a>
	<a href="agb.php5">AGB`s</a>	<a href="buero-mit-einrichtungen.php5">Partner</a>	<a href="jobs.php5">Ihre Karriere bei BBINW</a>
</div>

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->
<!--Start Content -->

	
<h2>Herzlich willkommen!</h2>

Wie bieten Ihnen s&auml;mtliche Dienstleistungen rund um Immobilien und Ihr Image 
an. <br>
      <br>
      <br>

<h3>Bereich <font size="26">Immobilien</font></h3>

<h4>Neuste Verkaufsobjekte</h4>
<table class="hometable" >

<tr>
    <td valign="top"  style="padding-right:20px;"><strong>Div. flache und g&uuml;nstige 
      Grundst&uuml;cke im</strong> sonnigen Nunningen, geeignet f&uuml;r seniorenfreundliches 
      Wohnen (Bungalow)</td>
    <td valign="top" style="padding-right:20px;"><strong>Verkauf div. Bungalow`s</strong> in 
      Nunningen.
      <p>Bauvorschlag<br>
      </p></td>
    <td valign="top" style="padding-right:20px;"><strong>G&uuml;nstiges Bauprojekt<br>
      in Grellingen</strong> nahe an der Bahn<br></td>
<td valign="top" style="padding-right:20px;"><strong>Investorenobjekte</strong></td>
</tr>

<tr>
          
    <td valign="top" style="padding-right:20px;">Foto von <strong>zwei Grundst&uuml;cken</strong> 
      in Nunningen. Weitere<br>
      Grundst&uuml;cke auf Anfrage.<br>
      <br><br>
      <img src="images/verkaufsobjekte/nunningen-130.jpg">
          </td>
    <td valign="top" style="padding-right:20px;">Bild von <strong>Bungalow</strong> f&uuml;r die flachen 
      Grundst&uuml;cke in Nunningen<br><br><br><img src="images/verkaufsobjekte/bungalow-130.jpg"></td>
    <td valign="top" style="padding-right:20px;">Bild von Projekt mit 5,5 Zimmer Haus 
      in<br>
      Grellingen<br><br><br><img src="images/verkaufsobjekte/einfamilien-haus-130.jpg"></td>
    <td valign="top" style="padding-right:20px;">Ueberbauung Bubendorf<br>
      10 Wohnungen. Investor<br>
      gesucht.<br><img src="images/verkaufsobjekte/investorenobjekt-130.jpg"></td>
</tr>


</table>
	

<p>&nbsp;</p>
<p>Wir bauen f&uuml;r Sie H&auml;user aus mit Fertigbauelementen, mit Ytong (Gasbetonstein), 
  Liapor, Backsteinen etc.</p>
<p>&nbsp;</p>
<h3>Folgende Haustypen k&ouml;nnen wir Ihnen anbieten:
</h3>


<table class="hometable" >
<tr>
<td width="30%">&nbsp;</td>
<td width="30%">Vorteil</td>
<td width="30%">Nachteil</td>
</tr>
<tr>
<td valign="top">Bungalow</td>
<td valign="top">	-  F&uuml;r flache Grundst&uuml;cke	
	-  Alles auf einem Boden
	-  Wohnen im Alter</td>
<td valign="top">mehr Bauland</td>
</tr>
<tr>
<td valign="top">H&auml;user mit Satteld&auml;cher</td>
<td valign="top"> 	-  Sch&uuml;tzt die Fassade
	-  Dachstuhl zum Aufbewahren</td>
<td valign="top">&nbsp;</td>
</tr>

<tr>
<td valign="top">H&auml;user mit Flachd&auml;cher</td>
<td valign="top"> -  Sch&ouml;ne, kubische Formen	Fassade ohne Schutz
	-  Geeignet f&uuml;r Hanglange oder MFH</td>
<td valign="top">&nbsp;</td>
</tr>

<tr>
<td valign="top">Doppel-EFH</td>
<td valign="top">
	-  Weniger Bauland
	-  Kosteneinsparung</td>
<td valign="top">&nbsp;</td>
</tr>

<tr>
<td valign="top">Villen</td>
<td valign="top">&nbsp;</td>
<td valign="top">&nbsp;</td>
</tr>

<tr>
<td valign="top">Turm-Landh&auml;user</td>
<td valign="top">Link auf alle H&auml;userbeispiele</td>
<td valign="top">&nbsp;</td>
</tr>

 </table>			

<br><br>




	

	

	




<h3>Suchprofile von unseren Mandanten<br>
Immobilien- Baulandsuche etc. </h3>

<table class="hometable" >
<tr>
<td valign="top">Grundst&uuml;ck oder Liegenschaft in Ronco und Ascona (Hohes Kostendach)<br>
Mandat von K. </td>
<td valign="top">1 bis 2,5 Zimmer
Eigentumswohnung
im Birstal/BL, vorzugsweise in Arlesheim
</td>
<td valign="top">Liegenschaften Arlesheim</td>
</tr>
</table>
<br>
<br>
Bitte richten Sie Ihre passenden Angebote an: <a class="link" href="mailto:&#105;&#109;&#109;&#111;&#098;&#105;&#108;&#105;&#101;&#110;&#064;&#098;&#097;&#117;&#108;&#097;&#110;&#100;&#045;&#110;&#119;&#046;&#099;&#104;" target="_blank">immobilien@bauland-nw.ch</a> oder 0041 (0)61 599 27 46 richten. 



<br><br>



<h3>Sie m&ouml;chten eine Liegenschaft verkaufen. Mit Hilfe von Ihren korrekten Angaben k&ouml;nnen wir Ihnen Kaufinteressenten vermitteln. </h3>
   <a href="downloads/wunschformular20101108.pdf" target="_blank">Bitte 
      f&uuml;llen Sie das Suchprofil aus.</a> <br>
      <br>
	  <h3>Wir bieten Ihnen kostenlose Schatzungen zur Ermittlung von Ihrem Liegenschaftswert an.</h3>
	  Bitte richten Sie Ihre passenden Angebote an: <a class="link" href="mailto:&#105;&#109;&#109;&#111;&#098;&#105;&#108;&#105;&#101;&#110;&#064;&#098;&#097;&#117;&#108;&#097;&#110;&#100;&#045;&#110;&#119;&#046;&#099;&#104;" target="_blank">immobilien@bauland-nw.ch</a> oder 0041 (0)61 599 27 46 richten. 

<h3>Dienstleistungen im Immobilienbereich</h3>

- Marktforschung, Projektentwicklung, Bautreuhand<br>
- Baumanagement<br>
- Vermarktung und Suche von Immobilien und Grundst&uuml;cken<br>
- Erstvermietung und Suche von Mietobjekten<br>
- Verwaltung von Liegenschaften<br>
- Innenausbauten und Inneneinrichtungen<br>
- Umbauten, Sanierungen<br>
<br>

<strong>Auf Wunsch bieten wir Ihnen den kompletten Service rund um Ihre Immobilie und Ihren Auftritt an. </strong>
<br>
      <br>
      <br>

      <table border="0" cellspacing="0" cellpadding="0">
<tr>
          <td rowspan="7"  width="320" valign="top" style="padding-right:20px;"><img src="images/logos/bbinw-2014.jpg" ><br>
            <br>
            Bieli Bauland Immobilien Nordwestschweiz<br>
            <br>
            Bieli baut mit Innovation f&uuml;r die Nordwestschweiz<br>
          </td>
          <td width="260"><strong>Immobilien</strong><br>
            <a class="link" href="mailto:&#105;&#109;&#109;&#111;&#098;&#105;&#108;&#105;&#101;&#110;&#064;&#098;&#097;&#117;&#108;&#097;&#110;&#100;&#045;&#110;&#119;&#046;&#099;&#104;" target="_blank">immobilien@bauland-nw.ch</a><br>
            <br>
          </td>
</tr>
<tr>
          <td valign="top"><strong>Finanzdienstleistungen</strong> <strong>und Versicherungen</strong><br>
            <a class="link" href="mailto:&#102;&#105;&#110;&#097;&#110;&#122;&#100;&#105;&#101;&#110;&#115;&#116;&#108;&#101;&#105;&#115;&#116;&#117;&#110;&#103;&#101;&#110;&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;" target="_blank">finanzdienstleistungen@bbinw.ch<br>
            </a><br>
          </td>
</tr>
<tr>
          <td valign="top"><strong>Verwaltungen </strong><br>
            <a class="link" href="mailto:immobilien&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;" target="_blank">immobilien@bbinw.ch</a><br>
            <br>
          </td>
</tr>
<tr>
          <td valign="top"><strong>Inneneinrichtungen, Innenausbauten</strong><br>
            <a class="link" href="mailto:inneneinrichtungen&#064;&#098;&#098;&#105;&#110;&#119;&#046;&#099;&#104;" target="_blank">inneneinrichtungen@bbinw.ch</a><br>
            <br>
          </td>
</tr>
<tr>
          <td valign="top"><strong>Administration</strong><br>
            <a class="link" href="mailto:administrationshilfe@bbinw.ch" target="_blank">administrationshilfe@bbinw.ch</a><br>
            <br>
          </td>
</tr>
<tr>
          <td valign="top"><strong>Haushaltshilfe</strong><br>
            <a class="link" href="mailto:haushaltshilfe@bbinw.ch" target="_blank">haushaltshilfe@bbinw.ch</a><br>
            <br>
          </td>
</tr>
<tr>
          <td valign="top"><strong>Atelier Bieli</strong><br>
            <a class="link" href="mailto:atelier@bbinw.ch" target="_blank">atelier@bbinw.ch</a></td>
</tr>
</table>


<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
 </body>
</html>