<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2 align="left">Suchen Sie nach Ihrer Individuellen K&uuml;che?<br></h2>
<p>Wir beraten Sie gerne bei der Gestaltung Ihrer individuellen K&uuml;che<br>
</p>
<br>
<br>

<img src="./images/kueche.jpg" width="350" align="left" vspace="0" hspace="20" alt="gelbe K&uuml;che">
<p><br clear="all"></p>
<br><br>
<table width="350">
<tr>
<th>
<b>Die folgende &Uuml;bersicht zeigt, welche Anordnung sich f&uuml;r die verschiedenen Grundrisse anbietet:</b>
</th>
</tr>
<tr>
<th>
<br><span style="font-weight:normal">1.Einzeilige K&uuml;che: Geeignet f&uuml;r kleinere Wohnungen oder Appartements mit nur einer Stellwand. Alle Funktionen befinden sich auf kleinstem Raum.</span>
</th>
</tr>
<tr>
<th>
<p><img src="./images/k-kueche1.jpg" width="350" align="center" vspace="0" hspace="20" alt="K&uuml;che"></p>
<br clear="all">
<br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">2.K&uuml;che in L-Form: Bei dieser vielseitigen K&uuml;chenform sind die Wege kurz und viele Planungsvarianten m&ouml;glich. Diese Form eignet sich besonders f&uuml;r R&auml;ume, die &uuml;ber mehrere T&uuml;ren verf&uuml;gen.</span>
</th>
</tr>
<tr>
<th>
<b><img src="./images/k-kueche2.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">3.Zweizeilige K&uuml;che: Mehr Platz bedeutet auch mehr Arbeitsfl&auml;che. Die zweizeilige Anordnung erschliesst viele Gestaltungsm&ouml;glichkeiten und erm&ouml;glicht eine optimale Raumnutzung. Diese Variante bietet sich an, wenn sich an den Schmalseiten des Raumes T&uuml;ren oder Fenster befinden.</span>
</th>
</tr>
<tr>
<th>
<br><b><img src="./images/k-kueche3.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">4.K&uuml;che mit Freiraum (mit fester Wand): Wandseitige Funktionen, wie K&uuml;hlschrank, Backofen, Dampfgarer, K&uuml;chenmaschinen, Vorr&auml;te, etc. Freistehende Insel mit Wasch- und Kochzentrum (zweiseitig nutzbar).</span>
</th>
</tr>
<tr>
<th>
<b><img src="./images/k-kueche4.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">5.K&uuml;che in U-Form: Die repr&auml;sentative U-Form ben&ouml;tigt ausreichend Platz, um gen&uuml;gend Bewegungsfreiheit zu gew&auml;hrleisten. Grossz&uuml;gige Wohnk&uuml;chen sind mit dieser Variante in vielen Formen zu realisieren.</span>
</th>
</tr>
<tr>
<th>
<b><img src="./images/k-kueche5.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">6.K&uuml;che in Halbinselform: Wenn die K&uuml;che und der Wohn-Essbereich fliessend ineinander &uuml;bergehen, bietet sich dieser Grundriss an, bei dem die Halbinsel oft die Funktion eines optischen Raumteilers &uuml;bernimmt.</span>
</th>
</tr>
<tr>
<th>
<b><img src="./images/k-kueche6.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">7.K&uuml;che mit Kochinsel: Wer &uuml;ber einen K&uuml;chenraum von mindestens 15 qm verf&uuml;gt, dem bieten sich mit dieser Variante repr&auml;sentative architektonische M&ouml;glichkeiten. Wer die Installation eines Kochfeldes mit einer Abzugshaube mitten in der K&uuml;che scheut, kann die Insel auch als reines Vorbereitungszentrum planen.</span>
</th>
</tr>
<tr>
<th>
<b><img src="./images/k-kueche7.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">8.Die zweizeilige K&uuml;che mit Wasch- und Arbeitsinsel: Durch diese Aufteilung entsteht eine grossz&uuml;gige zentrische Arbeitsfl&auml;che mit Wasserstelle. Es l&auml;sst sich an ihr allseitig arbeiten. Die Hochschr&auml;nke sind links und rechts vom Kochen positioniert.</span>
</th>
</tr>
<tr>
<th>
<b><img src="./images/k-kueche8.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
<tr>
<th>
<span style="font-weight:normal">9.K&uuml;che mit drei getrennten Arbeitszentren: Die Hochschr&auml;nke f&uuml;r K&uuml;hlen, Backen und D&auml;mpfen (Kaffeemaschinen und K&uuml;chenger&auml;te), die Wandzeile als Herd und die Insel als R&uuml;st- und Zubereitungszentrum.</span>
</th>
</tr>
<tr>
<th>
<b><img src="./images/k-kueche9.jpg" width="350" align="left" vspace="0" hspace="20" alt="K&uuml;che"></b>
<br clear="all">
<br><br>
<hr noshade width="350" size="3" align="left">
</th>
</tr>
</table>
<br><br><a class="link" href="./downloads/musterkuechen_20080715.pdf">Download: &Uuml;bersicht K&uuml;chenplanung (PDF)</a>
<br><br><a class="link" href="./downloads/checkliste_kueche_20080709.pdf">Checkliste K&uuml;chenplanung (PDF)</a>  

<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>