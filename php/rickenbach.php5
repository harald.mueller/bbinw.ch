<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>&Uuml;berbauung Rickenbach (LU)</h2>
<p>
Beim Fabrikweg in Rickenbach planen wir eine Wohnungs&uuml;berbauung. Dies auf ca. 5.743 qm, 8 Minuten ab Autobahnanschluss Sursee und 30 Autominuten von Luzern entfernt. Das Bauland ist sehr gut besonnt und befindet sich mitten im Dorf. Post, Bushaltestelle und Einkaufsl&auml;den, Schule und Kintergarten sind in wenige Gehminuten erreichbar.
</p>
<br><br>
<img width="450" src="./images/rickenbach1.jpg" ><br><br>
<img width="450" src="./images/rickenbach2.jpg" ><br><br>
<img width="450" src="./images/rickenbach3.jpg" ><br><br>
<a href="aktuelles.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>