<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Download Bereich</h2>
<p>Hier k&ouml;nnen Sie bequem unsere Formulare und<br>Dokumente im pdf Format downloaden</p><br><br>
<p>
<span class="normal">
<a class="link" href="./downloads/gutschein.pdf">&rArr;&nbsp;Gutschein</a>
<br><br><a class="link" href="./downloads/maklervertrag20090711.pdf">&rArr;&nbsp;Maklervertrag</a>
<br><br><a class="link" href="./downloads/maklervertrag-gastro-20080301.pdf">&rArr;&nbsp;Maklervertrag f&uuml;r Gastro- und landwirt. Betriebe</a>
<br><br><a class="link" href="./downloads/vorvertrag20080826.pdf">&rArr;&nbsp;Vorvertrag</a>
<br><br><a class="link" href="./downloads/wunschformular20101108.pdf">&rArr;&nbsp;Wunschformular</a>
<br><br><a class="link" href="./downloads/wunschformulaeng20070402.pdf">&rArr;&nbsp;Application</a>
<br><br><a class="link" href="./downloads/checkliste_kueche_20080709.pdf">&rArr;&nbsp;Checkliste K&uuml;chenplanung</a>
<br><br><a class="link" href="./downloads/Leistungsbeschrieb_Verwaltung_Bewirtschaftung.pdf">&rArr;&nbsp;Leistungsbeschreibung: Verwaltung und Bewirtschaftung von Mietliegenschaften</a>
<br><br><a class="link" href="./downloads/AB_Hausverwaltungsvertrag.pdf">&rArr;&nbsp;Allgemeine Bedingungen zum Hausverwaltungsvertrag</a>
<br><br><a class="link" href="./downloads/Hausverwaltungsvertrag.pdf">&rArr;&nbsp;Hausverwaltungsvertrag</a>
<br><br><a class="link" href="./downloads/erstvermietung_20080709.pdf">&rArr;&nbsp;Leistungsbeschreibung: Erstvermietung</a>
<br><br><a class="link" href="./downloads/allgemeinegeschaeftsbedingungen20080401.pdf">&rArr;&nbsp;Allgemeine Gesch&#228;ftsbedingungen (AGB) BBINW</a>
</span>
</p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>