<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<div>
<h2>Wir über uns</h2>
<p>
<p>Permanente Marktbeobachtung, ein erfahrenes dynamisches Team sowie
der hohe Anspruch unserer Kunden, stellen in ihrer Gesamtheit unser
Qualitätsmerkmal dar.</p><br>
<p>Wir bieten unseren Kaufinteressenten an, ein Objekt zu finden, das
ganz ihren individuellen Vorstellungen entspricht und ermöglichen den
Eigentümern, ihr zum Verkauf stehendes Objekt marktgerecht zu
plazieren.</p><br>
<a class="link" href="./dienst.php5"><b>⇒ Dienstleistung</b></a>
<br><br>
<a class="link" href="./zusammenarbeit.php5"><b>⇒ Interdisziplinäre Zusammenarbeit</b></a>
<br><br>
<a class="link" href="./suchen.php5"><b>⇒ Wir suchen</b></a>
<br><br>
<a class="link" href="./jobs.php5"><b>⇒ Jobs</b></a>
<br><br>
<a class="link" href="./referenzen.php5"><b>⇒ Referenzen</b></a>
<br><br>
<a class="link" href="./downloads/gutschein.pdf"><b>⇒ Vermittlungsgutschein</b></a>
<br><br>
<a class="link" href="./dokumente.php5"><b>⇒ Formulare und Dokumente</b></a>
<br><br>
<a class="link" href="./links.php5"><b>⇒ Interessante Links</b></a>
<br><br>
<a class="link" href="./impressum.php5"><b>⇒ Impressum</b></a>
<br><br>
<a class="link" href="./agb.php5"><b>⇒ AGBs</b></a>
<br><br>
<a class="link" href="./buero-mit-einrichtungen.php5"><b>⇒ Unser Büro mit Einrichtungen von unseren Vertriebspartnern</b></a>
<br><br>
<p>Vertrauen, qualifizierte Beratung und entsprechende Diskretion sind die Säulen unseres Unternehmens.</p>
<h2>Fordern Sie uns heraus!</h2>
</p>
</div>


<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>