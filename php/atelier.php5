<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header3.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Atelier</h2>
<p>
Wir freuen uns, einige langj&auml;hrige Kunden von Damen-Mode Rosy T&auml;schler in Oberwil weiterhin bedienen zu d&uuml;rfen. 
</p><br>
Eigenes Atelier vorhanden <br><br>
<img width="150px" src="./images/kleider.jpg" ></a><br><br><br>
Folgende Arbeiten k&ouml;nnen wir Ihnen abnehmen (Imageberatung/Haushaltservice)
<br><br>
-   Absteckarbeiten bei Ihnen zu Hause, kleine &Auml;nderungen. Reparaturen
<br><br>
-   Fachgerechte Kleiderreinigung (Haushaltservice)
<br><br>
-   Anfertigung von Inneneinrichtungsgegenst&auml;nden (Beispiel spezielle
    Tagesdecken, Zierkissen etc.)
<br><br>
-   Entw&uuml;rfe von passendem Kleidungsst&uuml;ck ohne Anfertigung
    (Referenzen vorhanden)
<br><br>
-   Besorgung von passenden Kleidungsst&uuml;cken. 
<br><br>
-   Begleitung beim Kleidereinkauf mit entsprechender fachgerechter
    Beratung.
<br><br>
-   Entsorgung Ihrer alten Kleidungsst&uuml;cke (Haushaltservice)
<br><br>
-   Consulting, Pr&auml;vention und Unterst&uuml;tzung beim Bek&auml;mpfen
    von Mottenbefall (Haushaltservice)
<br><br>
<br>

<a class="link" href="http://www.elbeo.de/" target="_blank"><b>Hochwertige Strumpfwaren von ELBEO f&uuml;r den 
Business-Bereich und die gehobenen Anspr&uuml;che
in einem guten Preis-Leistungsverh&auml;ltnis (Mindest-
Abnahme). 
 </b></a>
<br><br><br>
<a class="link" href="http://www.maxschindler.ch/" target="_blank"><b>Vertrieb von Designer- und englischen Stoffen.
F&uuml;r die Bekleidung.</b><br><b><i>Max Schindler, L&uuml;thi</i></b></a><br><br>
<img src="images/2014/stoffe.jpg" width="300"><br><br><br>
<a class="link" href="http://www.fischbacher.ch/" target="_blank"><b>Stoffe f&uuml;r den Inneneinrichtungsbereich</b>
<br><b><i>von Christian Fischbacher</i></b></a><br><br>
<img src="images/2014/kissen.jpg" width="300">
<br><br>
Leder, Kunststoffe, Stoffe etc. von Winter 
<br><br>
<img src="images/2014/leder.jpg" width="300">
<br><br><br>


Bettinhalte von <a href="http://www.balette.ch/" class="link" target="_blank">Balette</a> und <a class="link" href="http://www.albis.ch/" target="_blank">Albis</a> <br><br>
<table class="hometabelle">
<tr><td>
Dauneninhalt    
</td><td>
Wildseide
</td>
</tr>
<tr>
<td><img src="images/2014/dauneninhalt.jpg" width="300"></td>
<td><img src="images/2014/wildseide.jpg"  width="300"></td>
</tr>
</table>
<br><br>

<h5>Gerne beraten wir Sie in unserem B&uuml;ro in Arlesheim, zeigen Ihnen unsere Stoff- Leder- und Kunststoffkollektionen sowie m&ouml;gliche Bettinhalte etc.  </h5>


<br><br>
<b>Bitte kontaktieren Sie uns!</b>
<br><br>
<img width="450" src="./images/visit_atelier.jpg">
<br><br>
<b>Wer alleine arbeitet addiert - wer zusammen arbeitet multipliziert</b>

<!--End Content -->

</td>
</tr>
<!-- Start footer -->
<?php
?>
<!-- End footer -->
</table>
</body>
</html>