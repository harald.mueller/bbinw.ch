<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2 align="left">Ersteigerungsobjekt: Eigentumswohnung in Arlesheim (BL)<br></h2>
<br>
<p>
Diese attraktive 2,5-Zimmer Eigentumswohnung mit 70 qm Wohnfl&auml;che, Balkon, Atelier, grossem Kellerabteil und Parkplatz, wurde als neu renoviertes Ersteigerungsobjekt erfolgreich vermittelt.
</p>
<br>
<br>
<b><img src="./images/sockel.jpg" width="350" align="center" vspace="0" hspace="20" alt="Text?"></b>
<p>
<br clear="all"></p>
<br><br>


<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>

<!--End Content -->
</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>