<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2 align="left">Areal&uuml;berbauung in Stein (AG)<br></h2>
<br>
<p>Projektentwurf einer &Uuml;berbauung in Stein (AG), in Hanglage mit weitem Blick &uuml;ber das Rheintal, mit attraktiven H&auml;usern, Wohnungen und Tiefgaragen, in unterschiedlicher Gr&ouml;sse und Lage. Implenia hat den Entwurf gepr&uuml;ft und f&uuml;r gut befunden. Aufgrund der Lage wurde das Projekt nicht realisiert.</p>
<br>
<br>
<b><img src="./images/stein_plan1.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p>
<br clear="all"></p>
<br><br>
<b><img src="./images/stein_plan2.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?"></b>
<p>Die Anfahrt erfolgt &uuml;ber eine Ortstrasse zum Eigenheim. Der PKW wird in einer unterirdischen Tiefgarage abgestellt.
<br clear="all"></p>
<br><br>
<b><img src="./images/stein_plan3.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p>Zum Haus f&uuml;hren Wege, die f&uuml;r Fussg&auml;nger, Rollstuhlfahrer und zeitweilig auch f&uuml;r den eigenen PKW nutzbar sind.
<br clear="all"></p>
<br><br>
<b><img src="./images/stein_plan4.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?"></b>
<p>Das Bauland wird begr&uuml;nt und erh&auml;lt Sitz- und Spielm&ouml;glichkeiten f&uuml;r die Bewohner und deren Besucher.
<br><br>
<br clear="all"></p>
<br><br>
<b><img src="./images/stein_plan5.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p>Die unterschiedlich grossen Wohneinheiten sind meistens zweigeschossig, mit grosser vorgelagerter Terrasse mit Blick &uuml;ber die eigenen begr&uuml;nten Flachd&auml;cher talw&auml;rts.
<br clear="all"></p>
<br><br>
<b><img src="./images/stein_plan6.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?"></b>
<p>Die Wohnungsgr&ouml;sse und die Aufteilung sind planbar - eigene W&uuml;nsche und Vorstellungen k&ouml;nnen verwirklicht werden.
<br clear="all"></p>
<br><br>
<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>

<!--End Content -->
</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>