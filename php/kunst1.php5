<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Wohnraum - Kunstraum</h2>
<p>Wir unterst&uuml;tzen regionale K&uuml;nstler, um der Kunst und dem <br>Menschen mehr Raum zu schaffen.</p><br>Bitte kontaktieren Sie uns, wenn wir Ihr Interesse geweckt haben.<br><br></p>
<i>
<img width="250" src="./images/kunst1.jpg" ><p>Der Engel des Tanzes</p><br>
<img width="250" src="./images/kunst2.jpg" ><p>Die T&auml;nzer</p><br>
<img width="250" src="./images/kunst3.jpg" ><p>Die Welle</p><br>
<img width="250" src="./images/kunst4.jpg" ><p>Die Pianistin</p><br>
<img width="250" src="./images/kunst5.jpg" ><p>Kiss me - &Ouml;l auf Leinwand</p><br>
<img width="250" src="./images/kunst6.jpg" ><p>Das Bottminger Schloss - Aquarell</p><br>
<img width="250" src="./images/kunst7.jpg" ><p>Trommler & Schiff - Aquarell</p><br>
<img width="250" src="./images/kunst8.jpg" ><p>Halbkugel aus Zangen</p><br>
<img width="250" src="./images/kunst9.jpg" ><p>Kleine Kugel</p><br>
<img width="250" src="./images/kunst10.jpg" ><p>Grosse Kugel</p><br>
</i>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>