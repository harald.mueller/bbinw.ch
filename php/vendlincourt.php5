<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Maison individuelle adapt&eacute;e pour chaise roulante</h2>
<p>
Maison de 3,5 pi&egrave;ces, emplacement ensoleill&eacute; et tranquille, situation centr&eacute;e. Entr&eacute;e 10,35 m2, cuisine 14,25 m2, salon 53.06 m2, r&eacute;duit 6,40 m2, WC/douche 6,40 m2, chambre 15.05 m2, chambre &agrave; coucher 21,62 m2, cave 14,70 m2, terrasse couverte 18,48 m2, garage 25,20 m2. Armoire dans chambre &agrave; coucher avec ascenseur &agrave; habits en noyer. Toutes les portes en noyer, sols en marbre, cuisine blanche avec granite, table accord&eacute;e entre cuisine et salon. Surface nette: 193,94 m2, surface du terrain: 1028 m2, nombre de chambres: 3.5, nombre de salle de bain: 1, nombre WC visites: 1, Ann&eacute;e de construction: 1998
</p>
<br><br>
<img width="450" src="./images/BIE 20026_1_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_2_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_3_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_4_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_5_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_6_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_7_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_8_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_9_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_10_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_11_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_12_800x600-75" ><br><br>
<img width="450" src="./images/BIE 20026_13_800x600-75" ><br><br>
<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>