<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Eigentumswohnung in Pratteln (BL)</h2>
<p>
Gut eingeteilte Gartenwohnung mit der M&ouml;glichkeit einen Chemin&eacute;eofen einzubauen, und einem gut isolierten Lift. Die Wohnung verf&uuml;gt &uuml;ber keramische Bodenbel&auml;ge und Parkettb&ouml;den, sowie eine hochwertige Einbauk&uuml;che mit Granitabdeckung von Poggenphohl. Diese attraktive Eigentumswohnung, in Pratteln (BL) wurde verkauft f&uuml;r 820.000.-- CHF.
</p>
<br><br>
<img width="450" src="./images/pratteln1.jpg" ><br><br>
<img width="450" src="./images/pratteln2.jpg" ><br><br>
<img width="450" src="./images/pratteln3.jpg" ><br><br>
<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>