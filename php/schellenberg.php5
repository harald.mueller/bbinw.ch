<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>4.5 Zi-Eigentumswohnung</h2>
<p>
F&uuml;r unsere Mandanten konnten wir diese 4,5 Zimmer Eigentumswohnung in Riehen, Unterm Schellenberg, erfolgreich verkaufen. Diese Wohnung befindet sich im Hochparterre, ist Baujahr 1974 und wurde 2004 renoviert. Da der K&auml;ufer mit der gesamten Beratung, inkl. Inneneinrichtungsberatung, in hohem Masse zufrieden war, erhielten wir ebenfalls den Auftrag, die bisherige Wohnung des K&auml;ufers zu verkaufen.
<br><br>
DETAILS: 1 Zimmer 12.45 m2, Schlafzimmer 18.40 m2, Vorplatz 8.65 m2, Zimmer mit 12.26 m2, Balkon in Sichtbeton mit eingelassenem grossen Pflanzentrog. Vom Wohnzimmer aus gibt es eine isolierte Katzent&#252;r.,
Wohnzimmer mit 24.84 m2, autom. Storen mit Fernbedienung. Ausrichtung S&#252;d-Ost, angrenzender Essecken mit 2.28 m2 und der 12.20 m2 grossen Zeilenk&#252;che mit 4 Gaskochfelder, Electrolux-Swissline C-Ger&#228;ten und einem grossen AEG-K&#252;hlschrank. Eine Schiebet&#252;re kann den Essbereich vom Wohnbereich abtrennen.
Die Wohnung ist mit Rauhfasertapete tapeziert, ausgenommen dem Negativ-Abrieb in der K&#252;che. Die B&#246;den haben Laminatb&#246;den. 
BAD: Das Badezimmer mit Zugang vom Schlafzimmer aus hat eine Fl&#228;che von 5.16 m2. Badezimmer mit Dusche und WC sowie Lavabo 2.80 m2. Blaur&#252;nes Mosaik an den W&#228;nden. Graues Mosaik auf den B&#246;den.  
KELLER: 10 qm, Waschmaschine und Tumbler, Personenlift, Gasheizung, Kabel-TV
SOUS-SOL: Hauseingang mit Windfang, Zugang zur Autoeinstellhalle, Autoeinstellhalle mit 12 Pl&#228;tzen
</p>
<br><br>
<img width="450" src="./images/BIE 40010_1.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_2.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_3.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_4.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_5.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_6.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_7.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_8.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_9.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_10.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_11.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_12.jpg" ><br><br>
<img width="450" src="./images/BIE 40010_13.jpg" ><br><br>
<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>