<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Referenzen</h2>
<p>

<a href="muehliweiher.php5">1. Doppel-EFH-H&auml;lfte am M&uuml;hliweiher 9 in Zeihen<br><br>
<img width="150px" src="./images/muehli_plan1.jpg" ></a><br><br>

<a href="muenchenstein.php5">2. EFH am Nollenrain in M&uuml;nchenstein (BL),
<br>Verkauf ab Plan, Preisvorgabe f&uuml;r Projekt durch BBINW<br><br>
<img width="150px" src="./images/projekt_muenchenstein.jpg" ></a><br><br>

<a href="gempen.php5">3. Restaurant Gempenturm, Verkauf an 3. Interessenten<br><br>
<img width="150px" src="./images/gempenturm.jpg" ></a><br><br>

<a href="sockel.php5">4. Ersteigerungsobjekt: Eigentumswohnung in Arlesheim (BL)<br><br>
<img width="150px" src="./images/sockel.jpg" ></a><br><br>

<a href="stein.php5">5. Areal&uuml;berbauung in Stein (AG)<br><br>
<img width="150px" src="./images/stein_plan4.jpg" ></a><br><br>

<a href="tannen.php5">6. Bauprojekte im Lee Gebiet in Arlesheim (BL)<br><br>
<img width="150px" src="./images/tannen1.jpg" ></a><br><br>

<a href="arlesheim.php5">7. Div. EFH's am Zirkelacker in Arlesheim (BL)<br><br>
<img width="150px" src="./images/arlesheim1.jpg" ></a><br><br>

<a href="batteriestrasse.php5">8. Garten-Eigentumswohnung an der Batteriestrasse 19 in Bottmingen (BL),<br>185 qm Wohnfl&auml;che, Verkauf an 2. Interessenten,<br>VP 1.35 Mio. CHF<br><br>
<img width="150px" src="./images/batteriestrasse2b.jpg" ></a><br><br>

<a href="pratteln.php5">9. Garten-Eigentumswohnung beim Schloss in Pratteln (BL)<br>
Grosse Wohnung, Verkauf an Fam. Forster<br><br>
<img width="150px" src="./images/pratteln2.jpg" ></a><br><br>

<a href="bierastrasse.php5">10. Attika-Eigentumswohnung, Bierastrasse in Bottmingen (BL),<br>Wohnf&auml;che inkl. Balkon 306 qm, Verkauf an 1. Interessenten, 
<br>VP 164 Mio. CHF, Verkauf innert 4 Wochen<br><br>
<img width="150px" src="./images/BIE 40020_1_800x600-75.jpg" ></a><br><br>

</p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>