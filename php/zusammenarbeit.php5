<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<div>
<h2>Interdisziplin&auml;re Zusammenarbeit</h2>
<p>

<br>
&diams; BPS (Suisse), Cedric B&uuml;rgin<br>
&nbsp;&nbsp;&nbsp;&bull; Finanzierung von Liegenschaften, MFH's mit Gastroobjekten, etc.<br>
&nbsp;&nbsp;&nbsp;&bull; phone 058 855 30 03<br>
&nbsp;&nbsp;&nbsp;&bull; email cedric.buergin@bps-suisse.ch<br>
<br>
&diams; CS Basel, Konrad Stegm&uuml;ller<br>
&nbsp;&nbsp;&nbsp;&bull; phone 061 266 74 45<br>
&nbsp;&nbsp;&nbsp;&bull; email konrad.stegmueller@credit-suisse.com<br>
<br>
&diams; NAB Frick, Remo Deiss<br>
&nbsp;&nbsp;&nbsp;&bull; phone 062 865 17 75<br>
<br>
&diams; PostFinance Basel, Roberto Caruso<br>
&nbsp;&nbsp;&nbsp;&bull; Kundenberater, Region Nordwestschweiz<br>
&nbsp;&nbsp;&nbsp;&bull; phone 061 753 44 33<br>
&nbsp;&nbsp;&nbsp;&bull; email roberto.caruso@postfinance.ch<br>
<br>
&diams; UBS Arlesheim, Linda Hermann<br>
&nbsp;&nbsp;&nbsp;&bull; phone 061 706 26 36<br>
&nbsp;&nbsp;&nbsp;&bull; email linda.hermann@ubs.com<br>
<br>
<br>
&diams; UBS Breitenbach, Daniela Strauss<br>
&nbsp;&nbsp;&nbsp;&bull; phone 061 785 75 10<br>
&nbsp;&nbsp;&nbsp;&bull; email daniela.strauss@ubs.com<br>
<br>
&diams; UBS Reinach, Martin Wespi<br>
&nbsp;&nbsp;&nbsp;&bull; Prokurist, Teamleiter Privatkunden Individual<br>
&nbsp;&nbsp;&nbsp;&bull; phone 061 716 72 00<br>
<br>
&diams; Kantonale Hauseigent&uuml;merverb&auml;nde<br>
<br>
&diams; Regionale Wirtschaftspr&uuml;fer, Treuhandunternehmen, Investoren<br>
<br>
&diams; Beste Konditionen f&uuml;r indirekte Amortisationen, Bauherrenhaftpflicht-<br>
&nbsp;&nbsp;&nbsp;und Risikoversicherungen<br>
<br>
&diams; Finanz, Invest und Vorsorge<br>

<br><br>

<a class="link" href="straumann.php5" target="_blank"><b>&diams; Straumann:</b><i> Rohr- und Ablaufreinigungs-Service<br>&nbsp;&nbsp;&nbsp;(&rArr; Weitere Informationen finden Sie hier...)</i></a><br><br>
</p>
</div>


<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>