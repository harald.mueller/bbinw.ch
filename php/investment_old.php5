<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Investorenobjekte</h2>
<p>
<span class="normal">

<b>BAULAND:</b> Wir suchen Investoren f&uuml;r die &Uuml;bernahme von 3000 qm Bauland im Fricktal (AG) in zentraler Lage (Wohn- und Gewerbezone), mit m&ouml;glichem &Uuml;berbauungsobjekt.  
<br><br>

<b>BAULAND:</b> Ca. 50.000 qm Land in Parkanlage mit alten B&auml;umen, Privat, nicht einsehbar, ruhig (1 Villa und 1 Haus f&uuml;r Bedienstete), beste Lage mit Blick auf den Genfersee und Berge. Ein Teil des Grundst&uuml;ckes kann in kleinere bebaubare Parzellen unterteilt werden. Eigentumsrecht f&uuml;r Ausl&auml;nder in Ordnung. Investitionssumme ca. 60 Mio. F&uuml;r weitere Ausk&uuml;nte stehen wir Ihnen gerne zur Verf&uuml;gung.<br><br>

<a class="link" href="./downloads/Ueken_Arztpraxen.pdf"><b>IMMOBILIEN:</b> Arztpraxen in Ueken<br><b><i>(&rArr; Weitere Informationen finden Sie hier...)</i></b></a>

<br><br><a class="link" href="./downloads/projekt_ueken.pdf"><b>IMMOBILIE:</b> Wohnen und Arbeiten in Ueken - geplante &Uuml;berbauung im Fricktal  in unmittelbarer N&auml;he der Busstation und Autobahnauffahrt - gute Lage, nahe an Deutschland und der Chemie, viel Gr&uuml;nfl&auml;che mit B&auml;chlein, Eigentums- oder Mietwohnungen. Es werden Investoren f&uuml;r die Abnahme der Bl&ouml;cke gesucht, ebenfalls f&uuml;r einige Wohnungen. Das Areal l&auml;sst ein stilles Gewerbe wie z. B. Arztpraxen etc. zu. Der Standort bietet sich dazu an.<br><b><i>(&rArr; Weitere Informationen finden Sie hier...)</i></b></a>

<br><br><a class="link" href="./downloads/wuerenlingen.pdf"><b>IMMOBILIEN:</b> &Uuml;berbauung Chilerai in 5303 W&uuml;renlingen<br><b><i>(&rArr; Weitere Informationen finden Sie hier...)</i></b></a>

<br><br>
<i><b>Mehrwerte unserer Immobilien-Projekte:</i></b><br>
&diams; Lage: mit OeV oder zu Fuss zu allen wichtigen Orten.<br>
&diams; Hindernisfreiheit: Lift, keine internen Treppen<br>
&diams; Flexibilit&auml;t: Reduktion der Tragw&auml;nde, hohe Flexibil&auml;t<br>
&diams; Individualit&auml;t: Individuelle Beratung und Gestaltungsm&ouml;glichkeiten<br>
&diams; Serviceleistungen f&uuml;r den ganzen Lebensbereich<br>
<br><br>

</p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>