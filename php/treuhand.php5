<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<div>
<h2>Das Treuhand-Mandat</h2>
<p>
<table border="1" cellspacing="1" cellpadding="1" align="left">
  <tr>
    <th>Parteien</th>
    <td>Auftraggeber / Beauftrager</td>
  </tr>
  <tr>
    <th>Entstehung</th> 
    <td>formlos durch Annahme der Besorgung OR 395</td>
  </tr>
  <tr>
    <th>Umfang</th>
    <td>bestimmt sich nach der Natur des zu besorgenden Gesch&auml;ftes OR 396</td>
  </tr>
    <tr>
    <th>Vollmachten</th>
    <td>z.T. sind besondere Erm&auml;chtigungen notwendig OR 396/3</td>
  </tr>
    <tr>
    <th>Verpflichtungen</th>
    <td>der Beauftragte ist zur vorschriftsgem&auml;ssen Ausf&uuml;hrung verpflichtet OR 397</td>
  </tr>
    <tr>
    <th>Haftung</th>
    <td>der Beauftragte haftet f&uuml;r getreue und sorgf&auml;ltige Ausf&uuml;hrung</td>
  </tr>
    <tr>
    <th>Durchf&uuml;hrung</th>
    <td>pers&ouml;nlich, ausgenommen wenn der Beauftragte erm&auml;chtigt ist, bestimmte Aufgaben einem Dritten zu &uuml;bertragen OR 398/3 oder OR 399</td>
  </tr>
    <tr>
    <th>Rechenschaft</th>
    <td>jederzeit auf Verlangen des Auftraggebers &uuml;ber OR 400</td>
  </tr>
  <tr>
    <th>Rechte</th>
    <td>gehen auf den Auftraggeber &uuml;ber OR 401</td>
  </tr>
    <tr>
    <th>Pflichten</th>
    <td>der Auftraggeber verpflichtet sich um Auslagenersatz samt Zinsen und Befreiung von Verpflichtungen OR 402</td>
  </tr>
    <tr>
    <th>Haftung mehrerer</th>
    <td>alle Auftraggeber und Beauftragte haften unter sich solidarisch OR 403</td>
  </tr>
    <tr>
    <th>Beendigung</th>
    <td>beidseitig jederzeit m&ouml;glich durch Widerruf bzw. K&uuml;ndigung OR 404</td>
  </tr>
    <tr>
    <th>Erl&ouml;schen</th>
    <td>durch Tod, Handlungsunf&auml;higkeit oder Konkurs OR 405</td>
  </tr>
    <tr>
    <th>Wirkung<br>des Erl&ouml;schens</th>
    <td>Verpflichtungen vor dem Erl&ouml;schen bleiben Verbindlich OR 406</td>
  </tr>
</table>
</p>
</div>


<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>