<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Mietobjekte</h2>

</p>
<br><br>
<b><u>Basel (BS)</u></b>
<br><br>
<p>
<b><i>Mietobjekt gesucht:</i></b><br><br>
Gewerbefl&auml;che an zentraler Lage in Basel per sofort gesucht.<br>
<br><br>
<p>
<a href="objekt_test.php5?objektnummer=BIE 50005"><b><i>Zu vermieten:</i></b><br><br>Zentral gelegene 4 Zimmer-Wohnung zu vermieten in Basel, Morgartenring 173, 2. Stock<br><br><b><i>&rArr;&nbsp;Hier erfahren Sie mehr</i></b><br><br>
<img width="150px" src="webservice/images/BIE 50005_1_800x600-75.jpg" ></a><br>
<br><br>
</p>
<br><br>

<p>
<a href="objekt_test.php5?objektnummer=BIE 50006"><b><i>Zu vermieten:</i></b><br><br>Zentral gelegene 4 Zimmer-Wohnung zu vermieten in Basel, Morgartenring 173, 3. Stock<br><br><b><i>&rArr;&nbsp;Hier erfahren Sie mehr</i></b><br><br>
<img width="150px" src="webservice/images/BIE 50005_2_800x600-75.jpg" ></a><br>
<br><br>
</p>

</p>
<br><br>
<b><u>Binningen (BL)</u></b>
<br><br>

<a href="downloads/praxisraum_binningen.pdf"><b><i>Zentral gelegener Praxisraum zu vermieten in Binningen, Hauptstrasse 24<br><br><b><i>&rArr;&nbsp;Hier erfahren Sie mehr</i></b><br><br>
<img width="150px" src="webservice/images/BIE 50003_1_800x600-75.jpg" ></a><br>
<br><br> 
</p>

</p>
<br><br>
<b><u>Binningen (BL)</u></b>
<br><br>
<p>
<a href="http://www.bauland-nw.ch/php/objekt_test.php5?objektnummer=BIE%2050004"><b><i>Zentral gelegene 4 Zimmer-Wohnung zu vermieten in Binningen, Hauptstrasse 22<br><br><b><i>&rArr;&nbsp;Hier erfahren Sie mehr</i></b><br><br>
<img width="150px" src="webservice/images/BIE%2050004_1_800x600-75.jpg" ></a><br>
<br><br>
</p>

</p>
<br><br>
<b><u>Zeihen (AG)</u></b>
<br><br>
<p>
<a href="objekt_test.php5?objektnummer=BIE 50000"><b><i>Zu vermieten:</i></b><br><br>Neu m&ouml;bl. Zimmer in neuem Doppel-EFH in Zeihen (AG),<br>zwischen Basel und Z&uuml;rich<br>vbr><b><i>&rArr;&nbsp;Hier erfahren Sie mehr</i></b><br><br>
<img width="150px" src="webservice/images/BIE 50000_1_800x600-75.jpg" ></a><br>
<br><br>
</p>

<b>Suchen Sie ein Mietobjekt?</b><br><br>
<img width="300px" src="./images/vermietung.jpg" ><br><br>
<p>
Gerne suchen wir Ihre Wunschwohnung. Bitte geben Sie uns per Mail (immobilien@bauland-nw.ch) Ihre Suchw&uuml;nsche bekannt. Preis ab, Preis bis, Lage, Anzahl Zimmer, Gr&ouml;sse der Wohnung, Ausbaustandart, Anzahl G&auml;ste-WC und Badezimmer, Best&auml;tigung f&uuml;r den Suchauftrag gegen entsprechende Aufwandsentsch&auml;digung gem. SVIT.
</p>

<span class="normal">

<br><br><a class="link" href="./downloads/Leistungsbeschrieb_Verwaltung_Bewirtschaftung.pdf">Leiszungsbeschreibung: Verwaltung und Bewirtschaftung von Mietliegenschaften (PDF)</a>

<br><br><a class="link" href="./downloads/erstvermietung_20080709.pdf">Leiszungsbeschreibung: Erstvermietung (PDF)</a>

</span>
</p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>