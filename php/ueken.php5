<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>&Uuml;berbauung Ueken (AG)</h2>
<p>
In Ueken (AG) planen wir 2 bis 3 Mehrfamilienh&auml;user mit ca. 20 Eigentumswohnungen und Gewerbem&ouml;glichkeiten (stilles Gewerbe). Es k&ouml;nnen Wohnungen oder Bl&ouml;cke gekauft werden.
<br><br>
Wohnfl&auml;che 1.375 qm,<br>
Gewerbefl&auml;che 555 qm,<br>
Keller/Abstellr&auml;ume 220 qm,<br>
Parking 500 qm (20 Parkpl&auml;tze)<br>
= Total 2.650 qm<br>
<br>
Mutmassliches Bauvolumen ca. 10.050 Kubikmeter nach SIA 416.<br>
<br>
Mutmassliche Anlagekosten (+/- 20 %)<br>
<br>
Grundst&uuml;ck 3.051 qm &agrave; CHF 250/qm = CHF 762.750.--,<br>
Baukosten 10.050 m3 &agrave; CHF 520.--/Kubikmeter = CHF 5.226.000.--,<br>
NK 17 % = CHF 888.420.-= Total Anlagekosten<br>
von CHF 6.877.170.--. Ertrag CHF 458.200 = Rendite von 6.66 %.<br>
<br>
Weitere Informationen durch Direktanfrage an immobilien@bauland-nw.ch
<br><br>
</p>
<br><br>
<img width="450" src="./images/BIE 40009_1_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 40009_2_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 40009_3_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 40009_4_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 40009_G1_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 40009_G2_800x600-75.jpg" ><br><br>
<img width="450" src="./images/BIE 40009_G3_800x600-75.jpg" ><br><br>
<a href="aktuelles.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>