<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<div>
<h2>Unsere Leistungen<br>Ihr Gewinn</h2>
<p>
<a class="link" href="./verwaltung.php5"><b>Immobilienverwaltung<br>
(&rArr; Weitere Informationen finden Sie hier...)</b></a>
<br><br>
<b>Individuelle Beratung rund um Liegenschaften</b><br>
&diams; Standortevaluation und Bauplanung<br>
&diams; Baumanagement (Architektur, Bauhandwerk, Bewilligungen, etc.)<br>
&diams; Finanzdienstleistungen<br>
&diams; Erstellen von Verkehrswertsch&auml;tzungen und Expertisen<br>
&diams; <a href="treuhand.php5"><b>Treuhandmandate</b>zu Liegenschaftsverk&auml;ufen und Vermietungen<br>
<b><i>&nbsp;&nbsp;&nbsp;(&rArr; Weitere Informationen finden Sie hier...)</i></b>
<br><br></a>
</p>
<p>
<b>Imageberatung - Raumgestaltung</b><br>
&diams; Inneneinrichtung, Antike M&ouml;bel, Kunst am Bau<br>
&diams; Auserlesene M&ouml;bel f&uuml;r B&uuml;ro-, Restaurant-, und Wohnbereich<br>
&diams; Beleuchtungsplanung und Installation<br><br>
</p>
<p>
<b>Konditionen f&uuml;r Makler- und Beratungst&auml;tigkeit</b><br>
&diams; Beratung zu CHF 160.--/Std., erste Stunde wird nicht verrechnet<br>
&diams; Besondere Aufwendungen werden separat verrechnet<br>
&diams; Kilometerspesen CHF 1.--/km<br>
&diams; Maklerprovision gem&auml;ss SVIT:<br>
&nbsp;&nbsp;&nbsp;&bull; 3 % des Verkaufspreises bei Liegenschaften<br>
&nbsp;&nbsp;&nbsp;&bull; 5 % des Verkaufspreises bei Bauland<br>
&nbsp; oder nach Stundenaufwand<br>
&diams; bei Spezialobjekten ist ein Pauschalpreis m&ouml;glich<br>
&diams; Individuelles Werbebudget nur auf Vorauszahlung des Auftraggebers<br>
<br>
</p>
<p>
<b>Administrative Aufgaben - B&uuml;rodienstleistungen</b><br>
&diams; Sekretariat pro Stunde CHF 95.-- zuz&uuml;glich MwSt.<br>
&diams; Flyers erstellen inkl. einf&uuml;gen der Fotos mit Hilfe des prov. Photoshops<br>
&diams; Stellensuche (Schreiben Ihrer Lebensl&auml;ufe und Bewerbungsschreiben)<br>
&diams; Inseratencampagnen in Print- und Internetmedien<br>
&diams; Inseratenkosten werden separat verrechnet<br>
&diams; Zahlungs- und Buchhaltungsauftr&auml;ge<br>
&diams; Debitorenberater-Dienstleistungen (Delcreding)<br>
&diams; Stundenerfassungen<br>
&diams; Bauabrechnungen<br>
<br>
</p>
<p>
<b>Gesch&auml;ftsadresse - Gesch&auml;ftsdomizil</b><br>
&diams; Anschrift aussen<br>
&diams; Postweiterleitung<br>
&diams; Telefonnummer und Faxservice<br>
&diams; Monatspauschale CHF 350.--<br>
<br>
</p>
<p>
<b>Telefonservice</b><br>
&diams; werktags von 08.00 bis 11.30 und 13.00 bis 17.00 Uhr<br>
&diams; Montasabo  CHF 350.--<br>
<br>
</p>
<p>
<b>Telefonservice - Abo light</b><br>
&diams; werktags von 08.00 bis 11.30 und 13.00 bis 17.00 Uhr<br>
&diams; Grundpauschale 1. Woche CHF  50.--<br>
&diams; jede weitere Woche CHF 25.--, sowie zus&auml;tzlich pro Anruf CHF 1.50<br>
<br>
</p>
</div>


<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>