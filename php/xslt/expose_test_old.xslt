<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="NonHtml fn xdt xs xsi" xmlns:NonHtml="http://www.progress.com/StylusStudio/NonHtml">
        <xsl:output version="1.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
        <xsl:param name="SV_OutputFormat" select="'HTML'"/>
        <xsl:variable name="XML1" select="/"/>
        <xsl:template match="/">

                        <xsl:for-each select="$XML1">
                                <xsl:for-each select="Immos">
                                        <xsl:for-each select="Immo">
                                                <xsl:if test="Allgemeines/@Status = '2'">
                                                <xsl:if test= "@Referenznummer = $objektnummer ">
                                                        <p>
                                                                <table width="750" border="0">
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td width="750">
                                                                                                <p>
                                                                                                        <font face="Arial">
                                                                                                                        <h2>
                                                                                                                                <xsl:value-of select="Adresse/@Postleitzahl"/>&#160;
                                                                                                                                <xsl:value-of select="Adresse/@Ort"/>&#160;
                                                                                                                                <xsl:value-of select="Adresse/@Land"/>
                                                                                                                        </h2>
                                                                                                        </font>
                                                                                                        <font face="Arial">
                                                                                                                <b>
                                                                                                                        <xsl:value-of select="@Titel"/>
                                                                                                                </b>
                                                                                                        </font>
                                                                                                </p>&#160;&#160; <p>
                                                                                                        <font face="Arial">
                                                                                                                <xsl:value-of select="Beschreibungen"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                        </tbody>
                                                                </table>
                                                        </p>

                                                        <br></br>

                                                        <div style="text-align:left; width:500">
                                                        <p>
                                                                <table width="335" bgColor="#b3b3b3" border="0">
                                                                        <tbody>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Referenznummer</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="@Referenznummer"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Baujahr</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Baujahr"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Zustand</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Zustand"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Gesamtfläche (qm)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Gesamtflaeche"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Wohnfläche (qm)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Wohnflaeche"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Etagen</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlEtagen"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Zimmer</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlZimmer"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Schlafzimmer</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlSchlafzimmer"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Bäder</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlBaeder"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Frei ab</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@FreiAb"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Kaufpreis</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Kauf/@Kaufpreis"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
                                                                                </tr>
                                                                        </tbody>
                                                                </table>
                                                                </p>
                                                                </div>

                                                                <br></br>
                                                        <xsl:for-each select="Multimedia">
                                                                <xsl:for-each select="Bild">
                                                                        <p>
                                                                                <div align="center">
                                                                                        <xsl:for-each select="@Url"> <b>Hallo</b> 

                                                                                        <p>

                                                                                        <img height="300" hspace="10" vspace="10" border="1">
                                                                                        		<xsl:attribute name="alt"/>

                                                                                                 <xsl:attribute name="src">

                                                                                                		<xsl:if test="substring(string(.), 2, 1) = ':'">
                                                                                                        		<xsl:text>file:///</xsl:text>
                                                                                                		</xsl:if>

                                                                                                		<xsl:value-of select="translate(string(.), '\', '/')"/>
                                                                                        		</xsl:attribute>
                                                                                        </img>

                                                                                        </p>

                                                                                        </xsl:for-each>
                                                                                </div>
                                                                        </p>
                                                                </xsl:for-each>
                                                        </xsl:for-each>

                                                </xsl:if>
                                                </xsl:if>
                                        </xsl:for-each>
                                </xsl:for-each>
                        </xsl:for-each>
</xsl:template>
<xsl:template name="NewTemplate0"/>
</xsl:stylesheet>