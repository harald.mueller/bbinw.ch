<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="NonHtml fn xdt xs xsi" xmlns:NonHtml="http://www.progress.com/StylusStudio/NonHtml">
        <xsl:output version="1.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
        <xsl:param name="SV_OutputFormat" select="'HTML'"/>
        <xsl:variable name="XML1" select="/"/>
        <xsl:template match="/">
                        <xsl:for-each select="$XML1">
                                <xsl:for-each select="Immos">
                                        <xsl:for-each select="Immo">
                                                <xsl:if test="Allgemeines/@Status = '2'">
                                                <xsl:if test="Allgemeines/@VMart = $vmart">
                                                <xsl:if test="Adresse/@Landkreis = $region">
                                                        <p>
                                                        <table width="450" border="0">
                                                               <tbody>
                                                               <tr>
                                                               <td width="350">
                                                                   <p>
                                                                   <font face="Arial">

                                                                         <b>
                                                                         <p>
                                                                         <xsl:value-of select="@Referenznummer"/>&#160;
                                                                         </p>
                                                                         <xsl:value-of select="Adresse/@Postleitzahl"/>&#160;
                                                                         <xsl:value-of select="Adresse/@Ort"/>&#160;
                                                                         <xsl:value-of select="Adresse/@Land"/>
                                                                         </b>
                                                                         </font>
                                                                         </p>
                                                                         <p>
                                                                         <font face="Arial">
                                                                         <p align="left">
                                                                         <xsl:value-of select="@Titel"/>
                                                                         </p>
                                                                         <xsl:variable name="objektnummer" select="@Referenznummer"/>
									<xsl:variable name="pdfs" select="Multimedia/PDF/@Url"/>
                                                                         <xsl:variable name="pdfss" select="Multimedia/PDFS/@Url"/>
                                                                         <xsl:variable name="pdfsd" select="Multimedia/PDFD/@Url"/>
                                                                         <xsl:variable name="pdfsort" select="Multimedia/PDFORT/@Url"/>
<xsl:choose>
                                                                         <xsl:when test="Multimedia/PDFS/@Url and string-length(Multimedia/PDFS/@Url) &gt; 0 ">
                                                                              <a href="{$pdfss}"><b>Schaufensterwerbung</b></a>                          
                                                                         </xsl:when>
                                                                         <xsl:otherwise>
                                                                         <a href="objekt_test.php5?objektnummer={$objektnummer}"><b>Schaufensterwerbung</b></a>
									 </xsl:otherwise>
                                                                        </xsl:choose>
                                                                         <br></br>

									<xsl:choose>
									 <xsl:when test="Multimedia/PDF/@Url and string-length(Multimedia/PDF/@Url) &gt; 0 ">
                                                                              <a href="{$pdfs}"><b>Kurzdokumentation</b></a>                          
                                                                         </xsl:when>
									 <xsl:otherwise>
                                                                           <a href="expose_test.php5?objektnummer={$objektnummer}"><b>Kurzdokumentation</b></a>
									 </xsl:otherwise>
									</xsl:choose>

                                                                         <br></br>

                                                                        <xsl:choose>
                                                                         <xsl:when test="Multimedia/PDFD/@Url and string-length(Multimedia/PDFD/@Url) &gt; 0 ">
                                                                              <a href="{$pdfsd}"><b>Dokumentation</b></a>                          
                                                                         </xsl:when>
                                                                         <xsl:otherwise>
                                                                         <a href="flyer_test.php5?objektnummer={$objektnummer}"><b>Dokumentation</b></a>
                                                                        </xsl:otherwise>
                                                                        </xsl:choose>
									<br></br>
									<br></br>
                                                                        <xsl:choose>
                                                                         <xsl:when test="Multimedia/PDFORT/@Url and string-length(Multimedia/PDFORT/@Url) &gt; 0 ">
                                                                              <a href="{$pdfsort}"><b>Ortsinformation</b></a>                          
                                                                         </xsl:when>
                                                                        </xsl:choose>
                                                                         
									<br></br>
									<br></br>
                                                                         <a href="sendmailinteresse.php5?objektnummer={$objektnummer}"><b>Bin intessiert</b></a>
									&#160;&#160;&#160;&#160;
                                                                         <a href="sendmail.php5?objektnummer={$objektnummer}"><b>Link senden</b></a>
									
                                                                         </font>
                                                                         </p>
                                                               </td>
                                                               <td width="100">
                                                                                  <img align="top" width="100" hspace="0" vspace="0" border="0" src="{Multimedia/Bild/@Url}"/>
                                                               </td>
                                                        </tr>
                                                        </tbody>
                                                        </table>
                                                        </p>
                                                                        <p>&#160;</p>
                                                                        <p>&#160;<![CDATA[ ]]></p>
                                                </xsl:if>
                                                </xsl:if>
                                                </xsl:if>
                                        </xsl:for-each>
                                </xsl:for-each>
                        </xsl:for-each>
</xsl:template>
<xsl:template name="NewTemplate0"/>
</xsl:stylesheet>
