<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" exclude-result-prefixes="NonHtml fn xdt xs xsi" xmlns:NonHtml="http://www.progress.com/StylusStudio/NonHtml">
	<xsl:output version="1.0" method="html" indent="no" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>
	<xsl:param name="SV_OutputFormat" select="'HTML'"/>
	<xsl:variable name="XML1" select="/"/>
	<xsl:template match="/">
	<!--
	<html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xdt="http://www.w3.org/2005/xpath-datatypes" xmlns:fn="http://www.w3.org/2005/xpath-functions"><head>
				<title></title>
			</head>
		<body>
		-->
			<xsl:for-each select="$XML1">
				<xsl:for-each select="Immos">
					<xsl:for-each select="Immo">
						<xsl:if test="Allgemeines/@Status = '2'">
						<xsl:if test= "@Referenznummer = $objektnummer ">
							<p>
								<table width="450" border="0">
									<tbody>
										<tr>
											<td width="450">
												<p>
													<font face="Arial">
															<h2>
																<xsl:value-of select="Adresse/@Postleitzahl"/>&#160;
																<xsl:value-of select="Adresse/@Ort"/>&#160;
																<xsl:value-of select="Adresse/@Land"/>
															</h2>
													</font>
													<font face="Arial">
														<b>
															<xsl:value-of select="@Titel"/>
														</b>
													</font>
												</p>&#160;&#160; <p>
													<font face="Arial">
                           
													<xsl:value-of select="Beschreibungen"/>

													</font>
												</p>
											</td>
										</tr>
									</tbody>
								</table>
							</p>
						<div style="text-align:left; width:500">
							<xsl:for-each select="Multimedia">
								<xsl:for-each select="Bild">
									<p>
										<div style="float: left">
											<xsl:for-each select="@Url">
												<p>&#160;&#160;
													<font face="Arial" size="1">
															<xsl:value-of select="../@Beschriftung"/>
													</font>
												</p>
												<p>
													<img ondblclick="height=200" onclick="height=400" height="200" hspace="10" vspace="10" border="1"><xsl:attribute name="alt"/><xsl:attribute name="src">
												<xsl:if test="substring(string(.), 2, 1) = ':'">
													<xsl:text>file:///</xsl:text>
												</xsl:if>
												<xsl:value-of select="translate(string(.), '\', '/')"/>
											</xsl:attribute></img>
												</p>
											</xsl:for-each>
										</div>
									</p>
								</xsl:for-each>
							</xsl:for-each>
							<p style="clear: left">&#160;</p>
							<xsl:for-each select="Multimedia">
								<xsl:for-each select="Grundriss">
										<xsl:for-each select="@Url">
											<img ondblclick="height=300" onclick="height=900" height="300" hspace="10" vspace="10" border="1"><xsl:attribute name="alt"/><xsl:attribute name="src">
												<xsl:if test="substring(string(.), 2, 1) = ':'">
													<xsl:text>file:///</xsl:text>
												</xsl:if>
												<xsl:value-of select="translate(string(.), '\', '/')"/>
											</xsl:attribute></img>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
								</div>
							<p>
                                                         	<table width="335" bgColor="#b3b3b3" border="0">
									<tbody>
										<tr>
											<td width="150">
												<p align="left">
													<font face="Arial" size="2">Referenznummer</font>
												</p>
											</td>
											<td width="150">
												<p align="left">
													<font face="Arial" size="2">
														<xsl:value-of select="@Referenznummer"/>
													</font>
												</p>
											</td>
										</tr>

										<tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Nettowohnfl&#228;che (m&#178;)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Nettowohnflaeche"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
                                                                                           <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Bruttowohnfl&#228;che (m&#178;)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Bruttowohnflaeche"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
											<td width="150">
												<p align="left">
													<font face="Arial" size="2">Grundst&#252;cksfl&#228;che (m&#178;)</font>
												</p>
											</td>
											<td width="150">
												<p align="left">
													<font face="Arial" size="2">
														<xsl:value-of select="Allgemeines/@Grundstuecksflaeche"/>
													</font>
												</p>
											</td>
										</tr>
										<tr>
                                                                                 	<td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Raumh&#246;he (m)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Raumhoehe"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
                                                                                 	<td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Kubatur (m&#179;)</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Kubatur"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
                                                                                          <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Baujahr</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Baujahr"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
                                                                                 	<td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Letzte Renovation</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Renovation"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
                                                                                 	<td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Zustand</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@Zustand"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
                                                                                 					<tr>
                                                                                 	<td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Frei</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@FreiAbText"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
                                                                                 					<tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Zimmer</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlZimmer"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>

                                                                                 					<tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Badezimmer</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlBaeder"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl WC</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlGaesteWC"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>

                                                                                 <tr>
                                                                                 	<td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Anzahl Parkpl&#228;tze</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">
                                                                                                                <xsl:value-of select="Allgemeines/@AnzahlParkplaetze"/>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>

                                                                                 <tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Kaufpreis</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Kauf/@Kaufpreis"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
                                                                                 <tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Miete Brutto</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Miete/@Bruttomiete"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
										<tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Miete Netto</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Miete/@Nettomiete"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
                                                                                 <tr>
                                                                                         <td width="150">
                                                                                                <p align="left">
                                                                                                        <font face="Arial" size="2">Kaution/Depot</font>
                                                                                                </p>
                                                                                        </td>
                                                                                        <td width="150">
                                                                                                <p align="left">
                                                                                                        <font size="2">
                                                                                                                <font face="Arial">
                                                                                                                        <xsl:value-of select="Finanzielles/@Waehrung"/>
                                                                                                                        <xsl:value-of select="Finanzielles/Miete/@Kaution"/>
                                                                                                                </font>
                                                                                                        </font>
                                                                                                </p>
                                                                                        </td>
										</tr>
									</tbody>
								</table>
								</p>
									<p>&#160;</p>
									<p>&#160;<![CDATA[ ]]></p>
							</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:for-each>
		<!--
		</body>
	</html>
	-->
</xsl:template>
<xsl:template name="NewTemplate0"/>
</xsl:stylesheet>