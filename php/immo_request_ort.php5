<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<table align="left" width="200"  cellspacing="0" cellpadding="0" border="0"   >

<?php
$str2 = "http://bauland-nw.ch/php/webservice/bbinw.xml";

$xml = new DOMDocument();
$xsl = new DOMDocument();
$proc = new XSLTProcessor();

$xml->load($str2);

$xsl->load('xslt/immo_request_ort.xslt');

$proc->setParameter('', 'vmart', '1' );
$proc->setParameter('', 'ort', $_POST['ort'] );
$proc->importStyleSheet($xsl);
$stre = $proc->transformToXML($xml);
echo($stre);
?>

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</body>
</html>
