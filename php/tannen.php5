<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2 align="left">Bauprojekte im Lee Gebiet in Arlesheim (BL)<br></h2>
<br>
<p>Diverse Bauprojekte, in verschiedenen Varianten, im Lee Gebiet Arlesheim.<br><br>
<b>Variante Kaltdach ohne Dachausbau:</b><br> Erdgeschoss Wohn/Esszimmer 47 qm, K&uuml;che 9 qm, 1. OG Zimmer 13 qm, Zimmer 15 qm, Zimmer 13 qm und Zimmer 15 qm, 3 Kellerr&auml;ume 29 qm, 7 qm und 29 qm.<br><br>
<b>Variante Dachausbau:</b><br> Dachgeschoss 1 Zimmer ca. 35 qm, 8 qm Ankleideraum, 8 qm Bad/WC, 1. OG 1 Zimmer 14 qm, 1 Zimmer 16 qm, 1 Zimmer 18 qm, Erdgeschoss Wohn/Esszimmer 37 qm, 1 Reduit, K&uuml;chen 14 qm, 2 Kellerraeume, 10 qm, 28 qm und 19 qm.<br>
<br>
<br>
<b><img src="./images/tannen1.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p>
<br clear="all"></p>
<br><br>
<b><img src="./images/tannen3.jpg" width="350" align="right" vspace="0" hspace="20" alt="Text?"></b>
<p>Das Areal befindet sich in einer traumhaften Lage, bei gleichzeitiger N&auml;he von Schulen, Einkaufsm&ouml;glichkeiten und &ouml;ffentlichen Verkehrsmitteln.
<br clear="all"></p>
<br><br>
<b><img src="./images/tannen2.jpg" width="350" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p>Gr&uuml;nzonen laden ein, zu ausgedehnten Spazierg&auml;ngen an der frischen Luft, oder einem Waldlauf nach Feierabend.
<br clear="all"></p>
<br><br>
<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>

<!--End Content -->
</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>