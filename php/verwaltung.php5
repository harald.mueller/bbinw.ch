<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Verwaltung</h2>
<p>
<img src="./images/verwaltung.jpg" width="250" >
<br><br>
</p>
Wir &uuml;bernehmen Verwaltungen f&uuml;r einzelne Wohnungen und Gastronomiebetriebe im Stundenaufwand. Durch laufende Weiterbildung sind wir immer auf dem neusten Stand.
<p>
<span class="normal">

<br><br><a class="link" href="./downloads/Leistungsbeschrieb_Verwaltung_Bewirtschaftung.pdf">Leistungsbeschreibung: Verwaltung und Bewirtschaftung von Mietliegenschaften (PDF)</a>

<br><br><a class="link" href="./downloads/erstvermietung_20080709.pdf">Leistungsbeschreibung: Erstvermietung (PDF)</a>

</span>
</p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>