<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Wir suchen</h2>
<ul>
<li> Agrarland im Kanton AG oder LU - Fl&auml;che ca. 12 Hektar</li><br>
<li> grosse Eigentumswohnungen</li><br>
<li> H&auml;user und Villen</li><br>
<li> Bauland in BL und Fricktal</li><br>
<li> Wohnungen in Arlesheim, M&uuml;nchenstein, Dornach</li><br>
<li> Investoren f&uuml;r &Uuml;berbauungen, Seniorenwohnungen,<br>Gewerbeland und Investorenprodukte:</li><br>
<ul>
<li>&Uuml;berbauung in Ueken - 3 MFH mit ca. 18 Eigentumswohnungen. Es k&ouml;nnen Wohnungen oder Bl&ouml;cke gekauft werden.</li>
<li>2 bis 3 m&ouml;gliche Villen in klassischer Bauweise, sch&ouml;nste Hanglage mit Aussicht, ruhig gelegen, ab 7 Mio. in bevorzugter Gemeinde des Birseck-Tals.</li>
<li>Erstellung einer neuen Villa in Dornach.</li>
<li>Div. H&auml;user im sch&ouml;nsten Villenquartier und an zentraler Lage ab 1.3 Mio.</li>
<li>15.970 qm erschlossenes Bauland &agrave; CHF 350.--/qm. Geeignet f&uuml;r ein weiteres Logistik-Zentraum, Gewerbe, Industrie, Produktion und Lagerung. In unmittelbarer N&auml;he der Bahn. (Verkauf als ganzes Bauland.) Im Fricktal.</li>
<br>
</ul>
<li> eine Zusammenarbeit oder ein Zusammenschluss mit erfolgsorientieren Treuh&auml;ndlern, Juristen, Maklern, Versicherungsfachleuten, Graphiker, Architekten, Baufachleuten und Baufirmen, Investoren, Uebersetzern und Informatiker, zwecks Nutzung gemeinsamer Synergien.</li><br>
</ul>
<b>Wer alleine arbeitet addiert - wer zusammen arbeitet multipliziert</b>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>