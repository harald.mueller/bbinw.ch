<p>
<small>
<br><br>
Diese Offerte versteht sich freibleibend.

Die in dieser Dokumentation enthaltenen Informationen sind f&uuml;r den Empf&auml;nger bestimmt und
d&uuml;rfen ohne vorherige schriftliche Zusage der Firma BBINW f&uuml;r Dritte weder kopiert, reproduziert oder sonst wie zur Verf&uuml;gung gestellt werden.

Die enthaltenen Informationen beruhen zum Teil auf Fremdangaben und konnten von der Firma BBINW nicht alle unabh&auml;ngig &uuml;berpr&uuml;ft werden. Die Eigent&uuml;merschaft als auch die Firma BBINW &uuml;bernehmen somit keine Haftung, weder ausdr&uuml;cklich noch stillschweigend bez&uuml;glich Richtigkeit, Vollst&auml;ndigkeit oder Aussagekraft dieser Informationen oder der in diesem
Dokument wiedergegebenen Meinungen.

Die Eigent&uuml;merschaft als auch die Firma BBINW behalten sich ausdr&uuml;cklich das Recht vor, nach eigenem, freien Ermessen jederzeit das Angebot und/oder die Konditionen zu &auml;ndern oder vom Verkaufsverfahren vor dem Abschluss eines bindenden Vertrages zur&uuml;ckzutreten. 

Alle Interessenten m&uuml;ssen s&auml;mtliche in Verbindung mit Ihrer Teilnahme an diesem Verkaufsverfahren entstandenen Kosten selbst tragen und haben keinen Anspruch auf Erstattung Ihrer Aufwendungen. 
</small>
</p>
