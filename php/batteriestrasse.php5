<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Garten-Eigentumswohnung in Bottmingen (BL)</h2>
<p>
Sch&ouml;ne, grosse, helle Gartenwohnung mit 15 qm Wintergarten und 75 qm Gartenanteil, rollstuhlg&auml;ngig mit Lift, Elternschlafzimmer 21,5 qm, 3 Zimmer (je 13,5 qm), Wohnen 58,2 qm, K&uuml;che 10,9 qm, Entr&eacute;e 17,2 qm, Reduit 2,5 qm. Holz-Metallfl&uuml;gelfenster (32 dB). Erdsonden-W&auml;rmepumpe. Bodenheizung in s&auml;mltichen R&auml;umen, 4 Aussenparkplätze. Diese attraktive Eigentumswohnung, in der Batteriestrasse in Bottmingen (BL) wurde verkauft f&uuml;r 1.35 Millionen CHF.
</p>
<br><br>
<img width="450" src="./images/batteriestrasse1.jpg" ><br><br>
<img width="450" src="./images/batteriestrasse3.jpg" ><br><br>
<a href="referenzen.php5"><i><b>(&rArr; zur&uuml;ck)</b></i></a>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>