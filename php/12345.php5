<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->

<h2>Investitionsobjekte</h2>
<p>
<a class="link" href="./downloads/Molu_Tech_Businessplan.pdf"><b>VENTURE CAPITAL:</b> Businessplan Molu Tech GmbH AFC&#169; Umwelttechnologie &#8722; Investieren Sie in den Schutz der Umwelt und somit in unsere Zukunft. Das AFC&#169; &#8722; Verfahren behandelt Kl&auml;rschlammstr&ouml;me auf einer termophilen, biologischen Stufe. Die Entsorgungskosten werden damit dramatisch gesenkt und nebenbei entsteht eine bedeutende Einsparung von CO2 &#8722; Abgasen, in dem man den pyromanischen Prozess auf ein Minimum beschr&auml;nkt.</a>
<br>
<br>

</p>

<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>