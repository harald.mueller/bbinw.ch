<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="de">
<head>
<!-- Start title -->
<?php
include ("include/title.inc");
?>
<!-- End title -->
</head>

<body>
<!-- Header -->
<?php
include ("include/header.inc");
?>
<!-- End Header -->

<!-- Start Menue -->

<?php
include ("include/navi.inc");
?>

<!-- End Menue -->

<!--Start Content -->
<h2>Investorenobjekte</h2>
<p>
<span class="normal">


<b>Investor für baubewilligtes Projekt in Bubendorf mit 10 grossen Wohnungen an der Hauptstrasse in Bubendorf gesucht. VP ca. 6.4 Mio. Es besteht die M&ouml;glichkeit nur einen Block à 5 Wohnungen abzunehmen. Wir freuen uns auf Ihre Anfrage.
</b> 

<br><br>
<i><b>Mehrwerte unserer Immobilien-Projekte:</i></b><br>
&diams; Lage: mit OeV oder zu Fuss zu allen wichtigen Orten.<br>
&diams; Hindernisfreiheit: Lift, keine internen Treppen<br>
&diams; Flexibilit&auml;t: Reduktion der Tragw&auml;nde, hohe Flexibil&auml;t<br>
&diams; Individualit&auml;t: Individuelle Beratung und Gestaltungsm&ouml;glichkeiten<br>
&diams; Serviceleistungen f&uuml;r den ganzen Lebensbereich<br>
<br><br>

</p>
<!--End Content -->

</td>
</tr>
<tr>
<!-- Start footer -->
<?php
include ("include/footer.inc");
?>
<!-- End footer -->
</tr>
</table>
</body>
</html>
