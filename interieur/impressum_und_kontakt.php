<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<title>Innendesign Bieli</title>
<meta name="description" content="Dies ist eine Webseitenvorlage basierend auf HTML5">
<meta name="keywords" content="Innendesign">
<meta name="robots" content="index, follow">
<meta name="author" content="Hans Mustermann, Musterstrasse 1, 0000 Musterstadt">
<link rel="shortcut icon" href="img/favicon.gif">
<link rel="stylesheet" media="(max-width: 640px)" href="html5_stylesheet_mobile.css">
<link rel="stylesheet" media="(min-width: 640px)" href="html5_stylesheet.css">
<link rel="stylesheet" media="print" href="print.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
<!-- <header>Innendesign Bieli</header> -->
<header><img src="img/logo_big.jpg" id="logoMain" alt="Innendesign Bieli Logo" title=""></header>
<!-- <header>Atelier <img src="img/logo.png" id="logoMain" alt="Atelier Bieli Logo" title=""> Bieli</header> -->

<div id="hilfscontainer">

<aside> 
<nav>
<ul>
<li><a href="index.php">Startseite</a></li>
<li><a href="arbeitsraum.php">Arbeitsraum</a></li>
<li><a href="wohnraum.php">Wohnraum</a></li>
<li><a href="galerie.php">Galerie</a></li>
<li><a href="marken.php">Marken</a></li>
<li><a href="impressum_und_kontakt.php">Impressum und Kontakt</a></li>
</ul>
</nav>
</aside>

<main>
<article>    
<h1>Impressum und Kontakt</h1>
<figure><img src="img/slide2.jpg" width="600" height="370" alt="Dummy-Image">
<figcaption>Sorgt um die h&ouml;chste Kunden-Zufriedenheit</figcaption></figure>
<p><strong>Impressum</strong></p>
<p>  <!-- <strong>Angaben gem&auml;ß § 5 TMG:</strong><br> -->
  Susanne Bieli<br>
  Neumattstrasse 8<br>
  4144 Arlesheim<br>
  <br>
  <strong>Kontakt:</strong><br>
  Telefon:	061 599 27 46<br>
  Telefax:	061 599 47 45<br>
  Mobile:&nbsp;	076 413 19 36<br>
  E-Mail:	inneneinrichtungen@bbinw.ch</p>
<p><strong>Verantwortlich f&uuml;r den Inhalt der Seiten:</strong><br>
  Susanne Bieli, Neumattstrasse 8, 4144 Arlesheim<br>
  <br>
  <strong>Quellenangaben f&uuml;r die verwendeten Bilder und Grafiken:</strong><br>
  <p><strong>Quelle:</strong> <a href="http://www.bosse.de/" target="_blank">Bosse</a></p>
  <!-- <a href="http://dummy-image-generator.com/" target="_blank">dummy-image-generator.com</a></p>
<p><strong>Quelle:</strong> <a href="http://www.e-recht24.de" target="_blank">www.e-recht24.de</a></p> -->
</article>
</main>

</div>

<footer>&copy; BBINW, 2016</footer>

</body>
</html>
