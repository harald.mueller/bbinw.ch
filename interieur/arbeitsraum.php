<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<title>Innendesign Bieli</title>
<meta name="description" content="Dies ist eine Webseitenvorlage basierend auf HTML5">
<meta name="keywords" content="Innendesign">
<meta name="robots" content="index, follow">
<meta name="author" content="Hans Mustermann, Musterstrasse 1, 0000 Musterstadt">
<link rel="shortcut icon" href="img/favicon.gif">
<link rel="stylesheet" media="(max-width: 640px)" href="html5_stylesheet_mobile.css">
<link rel="stylesheet" media="(min-width: 640px)" href="html5_stylesheet.css">
<link rel="stylesheet" media="print" href="print.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
<!-- <header>Innendesign Bieli</header> -->
<header><img src="img/logo_big.jpg" id="logoMain" alt="Innendesign Bieli Logo" title=""></header>
<!-- <header>Atelier <img src="img/logo.png" id="logoMain" alt="Atelier Bieli Logo" title=""> Bieli</header> -->

<div id="hilfscontainer">

<aside> 
<nav>
<ul>
<li><a href="index.php">Startseite</a></li>
<li><a href="arbeitsraum.php">Arbeitsraum</a></li>
<li><a href="wohnraum.php">Wohnraum</a></li>
<li><a href="galerie.php">Galerie</a></li>
<li><a href="marken.php">Marken</a></li>
<li><a href="impressum_und_kontakt.php">Impressum und Kontakt</a></li>
</ul>
</nav>
</aside>

<main>
<article>    
<h1>Hochwertig und Langlebig</h1>
<h3>f&uuml;r das perfekte Arbeitsplatz</h3>
<figure><img src="img/slide6.jpg" width="600" height="370" alt="Dummy-Image">
<figcaption>Hier steht eine Bildunterschrift</figcaption></figure>
<p>Zwei flinke Boxer jagen die quirlige Eva und ihren Mops durch Sylt. Franz jagt im komplett verwahrlosten Taxi quer durch Bayern. Zwölf Boxk&auml;mpfer jagen Viktor quer &uuml;ber den großen Sylter Deich. Vogel Quax zwickt Johnys Pferd Bim. Sylvia wagt quick den Jux bei Pforzheim. Polyfon zwitschernd aßen M&auml;xchens Vögel R&uuml;ben, Joghurt und Quark. &quot;Fix, Schwyz! &quot; qu&auml;kt J&uuml;rgen blöd vom Paß. Victor jagt zwölf Boxk&auml;mpfer quer &uuml;ber den großen Sylter Deich. Falsches &uuml;ben von Xylophonmusik qu&auml;lt jeden größeren Zwerg. Heizölr&uuml;ckstoßabd&auml;mpfung. Zwei flinke Boxer jagen die quirlige Eva und ihren Mops durch Sylt. Franz jagt im komplett verwahrlosten Taxi quer durch Bayern. Zwölf Boxk&auml;mpfer jagen Viktor quer &uuml;ber den großen Sylter Deich. Vogel Quax zwickt Johnys Pferd Bim. Sylvia wagt quick den Jux bei Pforzheim. Polyfon zwitschernd aßen M&auml;xchens Vögel R&uuml;ben, Joghurt und Quark. &quot;Fix, Schwyz! &quot; qu&auml;kt J&uuml;rgen blöd vom Paß. Victor jagt zwölf Boxk&auml;mpfer quer &uuml;ber den großen Sylter Deich. Falsches &uuml;ben von Xylophonmusik qu&auml;lt jeden größeren Zwerg. Heizölr&uuml;ckstoßabd&auml;mpfung. Zwei flinke Boxer jagen die quirlige Eva und ihren Mops durch Sylt.</p>
</article>
</main>

</div>

<footer>&copy; Max Mustermann</footer>

</body>
</html>
