<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<title>Innendesign Bieli</title>
<meta name="description" content="Dies ist eine Webseitenvorlage basierend auf HTML5">
<meta name="keywords" content="Innendesign">
<meta name="robots" content="index, follow">
<meta name="author" content="Hans Mustermann, Musterstrasse 1, 0000 Musterstadt">
<link rel="shortcut icon" href="img/favicon.gif">
<link rel="stylesheet" media="(max-width: 640px)" href="html5_stylesheet_mobile.css">
<link rel="stylesheet" media="(min-width: 640px)" href="html5_stylesheet.css">
<link rel="stylesheet" media="print" href="print.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
<!-- <header>Innendesign Bieli</header> -->
<header><img src="img/logo_big.jpg" id="logoMain" alt="Innendesign Bieli Logo" title=""></header>
<!-- <header>Atelier <img src="img/logo.png" id="logoMain" alt="Atelier Bieli Logo" title=""> Bieli</header> -->

<div id="hilfscontainer">

<aside> 
<nav>
<ul>
<li><a href="index.php">Startseite</a></li>
<li><a href="arbeitsraum.php">Arbeitsraum</a></li>
<li><a href="wohnraum.php">Wohnraum</a></li>
<li><a href="galerie.php">Galerie</a></li>
<li><a href="marken.php">Marken</a></li>
<li><a href="impressum_und_kontakt.php">Impressum und Kontakt</a></li>
</ul>
</nav>
</aside>

<main>
<article>    
<h1>Interieur Angebote</h1>
<h3>Inneneinrichtung f&uuml;r den Arbeitsplatz & f&uuml;r Zuhause</h3>
<figure><img src="img/slide3.jpg" width="600" height="370" alt="Dummy-Image">
<figcaption>Hier steht eine Bildunterschrift</figcaption></figure>
<p>Weit hinten, hinter den Wortbergen, fern der L&auml;nder Vokalien und Konsonantien leben die Blindtexte. Abgeschieden wohnen sie in Buchstabhausen an der K&uuml;ste des Semantik, eines großen Sprachozeans. Ein kleines B&auml;chlein namens Duden fließt durch ihren Ort und versorgt sie mit den nötigen Regelialien. Es ist ein paradiesmatisches Land, in dem einem gebratene Satzteile in den Mund fliegen. Nicht einmal von der allm&auml;chtigen Interpunktion werden die Blindtexte beherrscht – ein geradezu unorthographisches Leben. Eines Tages aber beschloß eine kleine Zeile Blindtext, ihr Name war Lorem Ipsum, hinaus zu gehen in die weite Grammatik. Der große Oxmox riet ihr davon ab, da es dort wimmele von bösen Kommata, wilden Fragezeichen und hinterh&auml;ltigen Semikoli, doch das Blindtextchen ließ sich nicht beirren. Es packte seine sieben Versalien, schob sich sein Initial in den G&uuml;rtel und machte sich auf den Weg. Als es die ersten H&uuml;gel des Kursivgebirges erklommen hatte, warf es einen letzten Blick zur&uuml;ck auf die Skyline seiner Heimatstadt Buchstabhausen, die Headline von Alphabetdorf und die Subline seiner eigenen Straße, der Zeilengasse.</p>
</article>
</main>

</div>

<footer>&copy; Innendesign Bieli, 2016</footer>

</body>
</html>
