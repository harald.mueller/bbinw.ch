<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

	<div class="clearfix"></div>

<!--Start Content -->
<br><br>
<h1>Inneneinrichtungen</h1>
<h2>Wohn- und Arbeitsraumgestaltung<br></h2>

<p>Wir unterst&uuml;tzen sie bei der Konzeption und Umsetzung Ihres individuellen Wohn- und Arbeitsraumes.</p>
<p>Bitte <a href="bbinw-kontakt.php">kontaktieren</a> Sie uns, wenn wir Ihr Interesse geweckt haben.<br><br></p>

<p>
<a href="downloads/2016_11_29_Wohn-undArbeitsraumgestaltung.pdf" target="_blank">Wohn- und Arbeitsraumgestaltung.pdf</a>
,&nbsp;
<a href="downloads/2016_11_30_wartezimmer_fuer_kommunikation.pdf" target="_blank">Wartezimmer fuer die Kommunikation.pdf</a>
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<br>
	<div>
		<img src="images/inneneinrichtung/11.jpg" style="width: 840px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/01.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/02.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/03.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/04.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/05.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/06.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/07.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/08.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/09.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/10.jpg" style="width: 420px; margin:5px; float: left;">
	</div>
	<div class="clearfix"></div>
	<div>
		<img src="images/inneneinrichtung_bei_uns1.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung_bei_uns3.jpg" style="width: 420px; margin:5px; float: left;">
	</div>
	<br>
	<br>
	<br>
	<br>
	<div class="clearfix"></div>
	<div>
		<img src="images/inneneinrichtung/21.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/22.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/23.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/24.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/25.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/26.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/27.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/28.jpg" style="width: 420px; margin:5px; float: left;">
	<div>
	<div class="clearfix"></div>
	</div>
		<img src="images/inneneinrichtung/30.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/31.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/32.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/33.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/34.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/35.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/36.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/38.jpg" style="width: 420px; margin:5px; float: left;">
		<img src="images/inneneinrichtung/37.jpg" style="width: 420px; margin:5px; float: left;">
	</div>
	<div class="clearfix"></div>
<!-- 	
	<div>
		<img src="images/kommode_mit_grossen_schubladen.JPG" style="width: 420px; margin:5px; float: left;">
		<img src="images/nachtraeglich_eingebautes_wc.jpg" style="width: 420px; margin:5px; float: left;">
	</div>
	<div class="clearfix"></div>
 -->	
</div>
<div class="clearfix"></div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
