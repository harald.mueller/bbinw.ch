<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>

<div class="clearfix"></div>

<!-- Start Content -->
<br><br>
<h2 align="left">Lust auf mehr Licht?<br></h2>
<p>Wir beraten Sie gerne bei der Auswahl der passenden
Beleuchtung f&uuml;r den Innen- und Aussenbereich.</p>
<br>
<br>
<div class="compressContainer">

<b><img src="./images/fira5.jpg" width="308" align="left" vspace="0" hspace="20" alt="Text?"></b>
<p><br clear="all"></p>
<br><br>

<b><img src="./images/fira1.jpg" width="308" align="right" vspace="0" hspace="20" alt="Text?">Licht-Kunst mit FIRALUX Design</b>
<p>schafft eine positive Atmosphere.<br clear="all"></p>
<br><br>

<b><img src="./images/fira2.jpg" width="308" align="left" vspace="0" hspace="20" alt="Text?">FIRALUX Design</b>
<p>ist ein starker Partner f&uuml;r clevere Beleuchtungsl&ouml;sungen.<br clear="all"></p>
<br><br>

<b><img src="./images/fira3.jpg" width="308" align="right" vspace="0" hspace="20" alt="Text?">Suchen Sie eine optimale L&ouml;sung?</b>
<p>Auf Wunsch ber&auml;t Sie ein ausgewiesener Fachmann f&uuml;r Beleuchtungsfragen.<br>
Herr Beat Sch&ouml;nenberger bringt Ihnen gerne Lampen zur Auswahl.<br clear="all"></p>
<br><br>

<b><img src="./images/fira4.jpg" width="308" align="left" vspace="0" hspace="20" alt="Text?">Licht schafft Ambiente</b>
<p>Wir freuen uns, Ihnen die sch&ouml;nste Beleuchtung anbieten zu k&ouml;nnen.<br>
<p>Vereinbaren Sie noch heute einen Besprechungstermin und<br>
realisieren Sie Ihr individuelles Beleuchtungskonzept.<br clear="all"></p>
<br><br>
<a class="link" href="./downloads/informationen_beleuchtung.pdf" target="_blank"><b><i>&rArr;&nbsp;Flyer Beleuchtung (pdf)</i></b></a>
<br><br>
</div>
<!-- End Content -->

</td>
</tr>
<tr>

</tr>
</table>
</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
