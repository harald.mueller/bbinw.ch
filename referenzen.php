<?php include 'inc.head.html';?>
<body>
<?php include 'inc.menuStickyTop.html';?>

<div class="container">
<?php include 'inc.secNavBar.html';?>
<style>
table, td  {
	padding: 20px;
	border: 0;
	vertical-align: top;
}
.referenzenbilder {
	max-width: 100%;
	
}
</style>
<div class="clearfix"></div>
		<br>
		<br>
		<h1>Referenzen</h1>
		<br>
		<br>
		<table border="1" width="100%">
			<tr>
				<td colspan="2">
					<h2><br>01 - Erstellung einer Doppel-EFH-H&auml;lfte</h2>
				</td>
			</tr>
			<tr>
				<td><a href="muehliweiher.php"><img class="referenzenbilder" src="images/muehli_pic17.jpg"></a>
				</td>
				<td><h2>Zeihen (AG):</h2>
					Erstellung einer Doppel-EFH-H&auml;lfte am M&uuml;hliweiher 9 in Zeihen
					<br>Innenausbau- und Gartengestaltung durch BBINW sowie die Inneneinrichtung
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<h2><br>02 - Verkauf ab Plan</h2>
				</td>
			</tr>
			<tr>
				<td><a href="muenchenstein.php"><img class="referenzenbilder" src="images/projekt_muenchenstein.jpg"></a>
				</td>
				<td><h2>M&uuml;nchenstein (BL):</h2>
					Einfamilienhaus am Nollenrain in M&uuml;nchenstein (BL)
					<br> Verkauf ab Plan
					<br> Preisvorgabe f&uuml;r Projekt durch BBINW
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<h2><br>03 - Verkauf div. Eigentumswohnungen</h2>
				</td>
			</tr>
			<tr>
				<td><a href="batteriestrasse.php"><img class="referenzenbilder" src="images/batteriestrasse2b.jpg"></a>
				</td>
				<td><h2>Bottmingen (BL):</h2>
					Garten-Eigentumswohnung an der Batteriestrasse 19 in Bottmingen (BL)
					<br> Verkauf aller verk&auml;uflichen Wohnungen dieser Immobilie in
						Konkurrenz zu 3 weiteren Immobilienanbietern
					<br> Verkauf der Gartenwohnung (185 m2) an den zweiten
						Interessenten f&uuml;r <br>VP CHF 1.35 Mio.<br>
				</td>
			</tr>

			<tr>
				<td><a href="batteriestrasseEtage.php"><img class="referenzenbilder" src="images/bottmingenE1.jpg"></a>
				</td>
				<td><h2>Bottmingen (BL):</h2>
					Eigentumswohnung an der Batteriestrasse 19 in Bottmingen (BL)
					<br> Verkauf aller verk&auml;uflichen Wohnungen dieser Immobilie in
						Konkurrenz zu 3 weiteren Immobilienanbietern
					<br> Verkauf der Etagenwohnung (170 m2) f&uuml;r  VP CHF 1.25
						Mio.<br>
				</td>
			</tr>

			<tr>
				<td><a href="bierastrasse.php"><img class="referenzenbilder" src="images/BIE 40020_1_800x600-75.jpg"></a>
				</td>
				<td><h2>Bottmingen (BL):</h2>
					Attika-Eigentumswohnung an der Bierastrasse in Bottmingen (BL)
					<br> Wohnf&auml;che inkl. Balkon 306 m2
					<br> Verkauf an 1. Interessenten f&uuml;r<br> VP 1.64 Mio. CHF
					<br> Verkauf innert 4 Wochen
				</td>
			</tr>
			
			<tr>
				<td><a href="pratteln.php"><img class="referenzenbilder" src="images/pratteln2.jpg"></a>
				</td>
				<td><h2>Pratteln (BL):</h2>
					Garten-Eigentumswohnung.
					<br>Sehr grosse Wohnung beim Schloss in Pratteln (BL)
					<br> Verkauf an Fam. Forster inkl. Inneneinrichtungsberatung.
				</td>
			</tr>
			
			<tr>
				<td><a href="schellenberg.php"><img class="referenzenbilder" src="images/BIE 40010_13.jpg"></a>
				</td>
				<td><h2>Riehen (BS):</h2>
					Verkauf 4.5 Zi-Eigentumswohnung
					"Unterm Schellenberg" in Riehen
					an 2. Interessenten.
					Mandat durch neue Besitzer der Eigentumswohnung von
					Referenzobjekt in <a href="pratteln.php">Pratteln</a>.
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<h2><br>04 - Verkauf Bungalow im Jura mit Willensvollstreckungsmandat </h2>
				</td>
			</tr>
			<tr>
				<td><a href="vendlincourt.php"><img class="referenzenbilder"
					src="webservice/images/Vendlincourt_Eingang.jpg"></a>
				</td>
				<td><h2>Vendlincourt (JU):</h2>
					Maison individuelle adapt&eacute;e pour chaise roulante
					 (zus&auml;tzlich mit Willensvollstreckungsmandat)
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>05 - Verkauf Liegenschaft mit Einliegewohnung im Fricktal</h2>
				</td>
			</tr>
			<tr>
				<td><img class="referenzenbilder"
					src="webservice/images/BIE%2020061_a1_800x600-75.jpg">
				</td>
				<td><h2>W&ouml;lflinswil (AG):</h2>
					EFH mit Einliegerwohnung
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>06 - Verkauf Gewerbeliegenschaft</h2>
				</td>
			</tr>
			<tr>
				<td><a href="downloads/Bueroraum HR_Flyer.pdf"><img class="referenzenbilder" src="images/buro_hr.jpg"></a>
				</td>
				<td><h2>Arlesheim (BL):</h2>
					B&uuml;ror&auml;umlichkeiten
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>07 - Ersteigerungsobjekt, Eigentumswohnung mit Atelier</h2>
				</td>
			</tr>
			<tr>
				<td><a href="sockel.php"><img class="referenzenbilder" src="images/sockel.jpg"></a>
				</td>
				<td><h2>Arlesheim (BL):</h2>
					Ersteigerungsobjekt Eigentumswohnung in Arlesheim (BL)
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>08 - Verkauf von Gastroobjekten</h2>
				</td>
			</tr>
			<tr>
				<td><a href="olsberg.php"><img class="referenzenbilder"
					src="webservice/images/BIE%2090009_1_800x600-75.jpg"></a>
				</td>
				<td><h2>Olsberg (AG):</h2>
					<br> Restaurant 'Zum R&ouml;ssli' in Olsberg
				</td>
			</tr>

			<tr>
				<td><a href="gempen.php"><img class="referenzenbilder" src="images/gempenturm.jpg"></a>
				</td>
				<td><h2>Restaurant Gempenturm:</h2>
					Verkauf an 3. Interessenten
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>09 - Areal&uuml;berbauung</h2>
				</td>
			</tr>
			<tr>
				<td><a href="stein.php"><img class="referenzenbilder" src="images/stein_plan4.jpg"></a>
				</td>
				<td><h2>Stein (AG):</h2>
					Areal&uuml;berbauung, Projekt in Stein (AG)
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>10 - Div. Einfamilienh&auml;user</h2>
				</td>
			</tr>
			<tr>
				<td><a href="arlesheim.php"><img class="referenzenbilder" src="images/arlesheim1.jpg"></a>
				</td>
				<td><h2>Arlesheim (BL):</h2>
					Div. Einfamilienh&auml;user am Zirkelacker in Arlesheim (BL)
				</td>
			</tr>
					
			<tr>
				<td colspan="2">
					<h2><br>11 - Div. kleine und grosse Grundst&uuml;cke mit passender Architektur </h2>
				</td>
			</tr>
			<tr>
				<td><img class="referenzenbilder"
					src="webservice/images/BIE 10015_1_800x600-75.jpg">
				</td>
				<td><h2>Nunningen (SO):</h2>
					Verkauf diverser Baugrundst&uuml;cke in Nunningen, teils
					inklusive Bauprojekten.
				</td>
			</tr>

			<tr>
				<td><img class="referenzenbilder"
					src="webservice/images/BIE 10013_2_800x600-75.jpg">
				</td>
				<td><h2>Rickenbach (LU):</h2>
					Verkauf 5000 m2 Bauland in Rickenbach.
				</td>
			</tr>

			<tr>
				<td><img class="referenzenbilder"
					src="webservice/images/BIE 10009_1_800x600-75.jpg">
				</td>
				<td><h2>Zeiningen (AG):</h2>
					Verkauf von Bauland mit Panorama-Blick f&uuml;r den Bau eines
					3-Familienhauses und Vermittlung eines geeigneten Architekten.
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<h2><br>12 - Bauprojekte</h2>
				</td>
			</tr>
			<tr>
				<td><a href="tannen.php"><img class="referenzenbilder" src="images/tannen1.jpg"></a>
				</td>
				<td><h2>Arlesheim (BL):</h2>
					Bauprojekte Im Lee Gebiet in Arlesheim (BL)
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>13 - MFH</h2>
				</td>
			</tr>
			<tr>
				<td><a href="downloads/Duggingen_Inseratenvorlage_Helvetissimo.pdf"><img class="referenzenbilder"
					src="webservice/images/duggingen.jpg"></a>
				</td>
				<td><h2>Duggingen (BL):</h2>
					Verkauf MFH
				</td>
			</tr>

			<tr>
				<td><img class="referenzenbilder"
					src="webservice/images/2020_10-26_referenz1_arlesheim.jpg">
				    <img class="referenzenbilder"
					src="webservice/images/2020_10-26_referenz2_arlesheim.jpg">
				</td>
				<td><h2>Arlesheim (BL):</h2>
					Verkauf von 2 Mehrfamilienh&auml;user
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>14 - Verwaltungen</h2>
				</td>
			</tr>
			<tr>
				<td><img class="referenzenbilder"
					src="webservice/images/BIE 50005_2_800x600-75.jpg">
				</td>
				<td><h2>Basel (BS):</h2>
					Verwaltungsmandat Aug. 10 bis Dez. 2012 MFH f&uuml;r 4 Familien
					und separate Garage, am Morgartenring 173 in Basel. (Liegenschaft wurde verkauft)
					<br> <br>
					Geleistete Arbeiten: <br> 3 Mietvertr&auml;ge, Wohnungsrenovation
					innert 13 Tagen f&uuml;r eine 120 m2 Wohnung R&auml;umungsklage mit
					Ausweisung innert k&uuml;rzester Zeit.
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<h2><br>15 - Innenausbauten mit Inneneinrichtung </h2>
				</td>
			</tr>
			<tr>
				<td><img class="referenzenbilder"
					src="webservice/images/arlesheim_inneneinrichtung.jpg">
				</td>
				<td><h2>Arlesheim (BL):</h2>
					Innenausbauten mit Inneneinrichtung
				</td>
			</tr>
						
			<tr>
				<td colspan="2">
				Suchen auch Sie einen K&auml;ufer f&uuml;r ein Landst&uuml;ck, oder planen
				Sie ein Bauvorhaben und suchen noch einen passenden Standort?
				Bitte <a href="bbinw-kontakt.php">kontaktieren</a> Sie uns. Wir werden gerne f&uuml;r Sie t&auml;tig.
				</td>
			</tr>

			
		</table>

		<br>
		<br>
		<a class="link" href="downloads/informationen_referenzobjekte.pdf"
			target="_blank"><b><i>&rArr; Referenzen (pdf-Version)</i></b></a>
	</div>
<?php include 'inc.footer.html';?>
<?php include 'inc.menuitemsM.html';?>
</body>
</html>
