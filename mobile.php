<?php include 'inc.head.html';?>

	<body>
		<?php include 'inc.menuStickyTop.html';?>
			<div class="container">

				<?php include 'inc.secNavBar.html';?>

				<div class="container">
					<br>
					<h1 class="moveDown40">Nur das Beste ist gut genug f&uuml;r Sie</h1><h2 class="goneForGood">BBINW baut mit Innovation f&uuml;r die Nordwestschweiz</h2>
					<h3 id="hide_undertitle">BBINW Nordwestschweiz</h3>
					<br>
				</div>

				<nav>
					<div class="far-left">
						<div class="circular"><a href="immo.php" title=""><img src="img/batteriestrasse2b.jpg" title="" alt="" class="circular-image"></a></div>
						<br><br>
							<section>
								<h2>Immobilien</h2>
								<br>
								<p>Ob kaufen oder verkaufen oder Immobilien bewerten,  
								sed doeiusmod tempor incididunt ut labore et dolore magna 
								aliqua. Ut enimad minim veniam, quis nostrud exercitation 
								ullamco laboris nisi utaliquip ex ea commodo consequat. 
								Duis aute irure dolor inreprehenderit in voluptate velit 
								esse cillum dolore eu fugiat nullapariatur. Excepteur 
								sint occaecat cupidatat non proident, sunt inculpa qui 
								officia deserunt.</p>
								<a class="follow-to-the-next-page" href="immo.php" title=""> Weiter zu Immobilien...</a></p>
							</section>
					</div>
				</nav>

				<div class="content">

					<div class="content-left">
						<div class="circular"><a href="bbinw-kontakt.php" title=""><img src="img/susanne_bieli.jpg" title="" alt="" class="circular-image"></a></div>
						<br><br>
							<section>
								<h2>Pers&ouml;nliche Beratung</h2>
								<br>
								<p>Auf pers&ouml;nliche Beratung legen wir sehr grossen 
								Wert. Vom ersten Kontakt bis zur Eigentums&uuml;bertragung 
								stehen wir allen Beteiligten zur Verf&uuml;gung. Auch nach 
								dem Verkauf: zum Beispiel mit der Abrechnung der 
								Grundst&uuml;ckgewinnsteuer. Wir entlasten Sie vollumf&auml;nglich 
								und sind Ihr Anspr&auml;chspartner f&uuml;r alle Fragen. 
								<strong><em>Wir thematisieren Ihre W&uuml;nsche und 
								Erwartungen.</em></strong></p>
								<a class="follow-to-the-next-page" href="bbinw-kontakt.php" title=""> Weiter zum Kontakt-Formular...</a></p>
							</section>
						</div>

						<div class="content-right">
							<div class="circular"><a href="bauland.php" title="Bauland und Bauvorschl&auml;ge"><img src="img/BIE_10001_1_800x600-75.jpg" title="" alt="" class="circular-image"></a></div>
							<br><br>
							<article>
								<h2>Bauland</h2>
								<br>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
								sed doeiusmod tempor incididunt ut labore et dolore magna 
								aliqua. Ut enimad minim veniam, quis nostrud exercitation 
								ullamco laboris nisi utaliquip ex ea commodo consequat. 
								Duis aute irure dolor inreprehenderit in voluptate velit 
								esse cillum dolore eu fugiat nullapariatur. Excepteur 
								sint occaecat cupidatat non proident, sunt inculpa qui 
								officia deserunt.</p>
								<a class="follow-to-the-next-page" href="bauland.php" title=""> Weiter zum Bauland...</a></p>
							</article>
						</div>

				</div>

			</div>

				<div class="clearfix"></div>

			<?php include 'inc.footer.html';?>
		<?php include 'inc.menuitemsM.html';?>
	</body>
</html>
